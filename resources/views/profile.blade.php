@extends('layouts.main')

@section('content')
    <main class="left-sidebar-before">
        <div class="container-fluid">
            <div class="row order-row">
                <!-- Left block -->
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2 col-lg-2x  left-sidebar-out">
                    <div class="left-sidebar">
                        @include('partials.topwinnings')
                    </div>
                </div>
        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-lg-10x main-content-out">
            <div class="main-content">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="user-profile-info clearfix">
                            <div class="user-profile-info-left">
                                <div class="media">
                                    <div class="media-left media-middle">
                                        <a href="javascript:void(0);">
                                            <img class="img-circle" src="images/user-1.jpg" alt="product">
                                        </a>
                                    </div>
                                    <div class="media-body media-middle">
                                        <h4 class="media-heading">Glane Mexwell</h4>
                                        <p>Steam Profile</p>
                                    </div>
                                </div>
                            </div>
                            <div class="user-profile-info-right">
                                <table class="table">
                                    <tr>
                                        <th>Cases Opened</th>
                                        <td>150</td>
                                    </tr>
                                    <tr>
                                        <th>Contract Signed</th>
                                        <td>1</td>
                                    </tr>
                                    <tr>
                                        <th>Items Shared</th>
                                        <td>5</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <ul class="spin-slider-btn clearfix">
                            <li><button type="button" class="btn btn-md btn-blue">USER ITEMS</button></li>
                            <li><button type="button" class="btn btn-md btn-dark-blue">USER SHARDS</button></li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <h2 class="title-1 blue-title">Community Created Cases</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="product-1-grid col-xs-4 col-sm-4 col-md-6 col-lg-3">
                        <div class="product-1 product-1-inner product-yellow">
                            <a href="javascript:void(0);">
                                            <span class="product-1-img">
                                                <img class="img-responsive center-block" src="images/product/product-1.png" alt="product">
                                                <span class="sold">$ Sold</span>
                                                <span class="money-2">$00.10</span>
                                            </span>
                                <span class="product-1-detail">
                                                <span class="product-1-name">StatTrak™ M9 Bayonet | Crimson Web (Minimal Wear)</span>
                                            </span>
                            </a>
                        </div>
                    </div>
                    <div class="product-1-grid col-xs-4 col-sm-4 col-md-6 col-lg-3">
                        <div class="product-1 product-1-inner product-yellow">
                            <a href="javascript:void(0);">
                                            <span class="product-1-img">
                                                <img class="img-responsive center-block" src="images/product/product-1.png" alt="product">
                                                <span class="sold">$ Sold</span>
                                                <span class="money-2">$00.10</span>
                                            </span>
                                <span class="product-1-detail">
                                                <span class="product-1-name">StatTrak™ M9 Bayonet | Crimson Web (Minimal Wear)</span>
                                            </span>
                            </a>
                        </div>
                    </div>
                    <div class="product-1-grid col-xs-4 col-sm-4 col-md-6 col-lg-3">
                        <div class="product-1 product-1-inner product-yellow">
                            <a href="javascript:void(0);">
                                            <span class="product-1-img">
                                                <img class="img-responsive center-block" src="images/product/product-1.png" alt="product">
                                                <span class="sold">$ Sold</span>
                                                <span class="money-2">$00.10</span>
                                            </span>
                                <span class="product-1-detail">
                                                <span class="product-1-name">StatTrak™ M9 Bayonet | Crimson Web (Minimal Wear)</span>
                                            </span>
                            </a>
                        </div>
                    </div>
                    <div class="product-1-grid col-xs-4 col-sm-4 col-md-6 col-lg-3">
                        <div class="product-1 product-1-inner product-yellow">
                            <a href="javascript:void(0);">
                                            <span class="product-1-img">
                                                <img class="img-responsive center-block" src="images/product/product-1.png" alt="product">
                                                <span class="sold">$ Sold</span>
                                                <span class="money-2">$00.10</span>
                                            </span>
                                <span class="product-1-detail">
                                                <span class="product-1-name">StatTrak™ M9 Bayonet | Crimson Web (Minimal Wear)</span>
                                            </span>
                            </a>
                        </div>
                    </div>
                    <div class="product-1-grid col-xs-4 col-sm-4 col-md-6 col-lg-3">
                        <div class="product-1 product-1-inner product-yellow">
                            <a href="javascript:void(0);">
                                            <span class="product-1-img">
                                                <img class="img-responsive center-block" src="images/product/product-1.png" alt="product">
                                                <span class="sold">$ Sold</span>
                                                <span class="money-2">$00.10</span>
                                            </span>
                                <span class="product-1-detail">
                                                <span class="product-1-name">StatTrak™ M9 Bayonet | Crimson Web (Minimal Wear)</span>
                                            </span>
                            </a>
                        </div>
                    </div>
                    <div class="product-1-grid col-xs-4 col-sm-4 col-md-6 col-lg-3">
                        <div class="product-1 product-1-inner product-yellow">
                            <a href="javascript:void(0);">
                                            <span class="product-1-img">
                                                <img class="img-responsive center-block" src="images/product/product-1.png" alt="product">
                                                <span class="sold">$ Sold</span>
                                                <span class="money-2">$00.10</span>
                                            </span>
                                <span class="product-1-detail">
                                                <span class="product-1-name">StatTrak™ M9 Bayonet | Crimson Web (Minimal Wear)</span>
                                            </span>
                            </a>
                        </div>
                    </div>
                    <div class="product-1-grid col-xs-4 col-sm-4 col-md-6 col-lg-3">
                        <div class="product-1 product-1-inner product-pink">
                            <a href="javascript:void(0);">
                                            <span class="product-1-img">
                                                <img class="img-responsive center-block" src="images/product/product-1.png" alt="product">
                                                <span class="sold">$ Sold</span>
                                                <span class="money-2">$00.10</span>
                                            </span>
                                <span class="product-1-detail">
                                                <span class="product-1-name">StatTrak™ M9 Bayonet | Crimson Web (Minimal Wear)</span>
                                            </span>
                            </a>
                        </div>
                    </div>
                    <div class="product-1-grid col-xs-4 col-sm-4 col-md-6 col-lg-3">
                        <div class="product-1 product-1-inner product-pink">
                            <a href="javascript:void(0);">
                                            <span class="product-1-img">
                                                <img class="img-responsive center-block" src="images/product/product-1.png" alt="product">
                                                <span class="sold">$ Sold</span>
                                                <span class="money-2">$00.10</span>
                                            </span>
                                <span class="product-1-detail">
                                                <span class="product-1-name">StatTrak™ M9 Bayonet | Crimson Web (Minimal Wear)</span>
                                            </span>
                            </a>
                        </div>
                    </div>
                    <div class="product-1-grid col-xs-4 col-sm-4 col-md-6 col-lg-3">
                        <div class="product-1 product-1-inner product-purple">
                            <a href="javascript:void(0);">
                                            <span class="product-1-img">
                                                <img class="img-responsive center-block" src="images/product/product-1.png" alt="product">
                                                <span class="sold">$ Sold</span>
                                                <span class="money-2">$00.10</span>
                                            </span>
                                <span class="product-1-detail">
                                                <span class="product-1-name">StatTrak™ M9 Bayonet | Crimson Web (Minimal Wear)</span>
                                            </span>
                            </a>
                        </div>
                    </div>
                    <div class="product-1-grid col-xs-4 col-sm-4 col-md-6 col-lg-3">
                        <div class="product-1 product-1-inner product-purple">
                            <a href="javascript:void(0);">
                                            <span class="product-1-img">
                                                <img class="img-responsive center-block" src="images/product/product-1.png" alt="product">
                                                <span class="sold">$ Sold</span>
                                                <span class="money-2">$00.10</span>
                                            </span>
                                <span class="product-1-detail">
                                                <span class="product-1-name">StatTrak™ M9 Bayonet | Crimson Web (Minimal Wear)</span>
                                            </span>
                            </a>
                        </div>
                    </div>
                    <div class="product-1-grid col-xs-4 col-sm-4 col-md-6 col-lg-3">
                        <div class="product-1 product-1-inner product-purple">
                            <a href="javascript:void(0);">
                                            <span class="product-1-img">
                                                <img class="img-responsive center-block" src="images/product/product-1.png" alt="product">
                                                <span class="sold">$ Sold</span>
                                                <span class="money-2">$00.10</span>
                                            </span>
                                <span class="product-1-detail">
                                                <span class="product-1-name">StatTrak™ M9 Bayonet | Crimson Web (Minimal Wear)</span>
                                            </span>
                            </a>
                        </div>
                    </div>
                    <div class="product-1-grid col-xs-4 col-sm-4 col-md-6 col-lg-3">
                        <div class="product-1 product-1-inner product-purple">
                            <a href="javascript:void(0);">
                                            <span class="product-1-img">
                                                <img class="img-responsive center-block" src="images/product/product-1.png" alt="product">
                                                <span class="sold">$ Sold</span>
                                                <span class="money-2">$00.10</span>
                                            </span>
                                <span class="product-1-detail">
                                                <span class="product-1-name">StatTrak™ M9 Bayonet | Crimson Web (Minimal Wear)</span>
                                            </span>
                            </a>
                        </div>
                    </div>
                    <div class="product-1-grid col-xs-4 col-sm-4 col-md-6 col-lg-3">
                        <div class="product-1 product-1-inner product-purple">
                            <a href="javascript:void(0);">
                                            <span class="product-1-img">
                                                <img class="img-responsive center-block" src="images/product/product-1.png" alt="product">
                                                <span class="sold">$ Sold</span>
                                                <span class="money-2">$00.10</span>
                                            </span>
                                <span class="product-1-detail">
                                                <span class="product-1-name">StatTrak™ M9 Bayonet | Crimson Web (Minimal Wear)</span>
                                            </span>
                            </a>
                        </div>
                    </div>
                    <div class="product-1-grid col-xs-4 col-sm-4 col-md-6 col-lg-3">
                        <div class="product-1 product-1-inner product-sky-blue">
                            <a href="javascript:void(0);">
                                            <span class="product-1-img">
                                                <img class="img-responsive center-block" src="images/product/product-1.png" alt="product">
                                                <span class="sold">$ Sold</span>
                                                <span class="money-2">$00.10</span>
                                            </span>
                                <span class="product-1-detail">
                                                <span class="product-1-name">StatTrak™ M9 Bayonet | Crimson Web (Minimal Wear)</span>
                                            </span>
                            </a>
                        </div>
                    </div>
                    <div class="product-1-grid col-xs-4 col-sm-4 col-md-6 col-lg-3">
                        <div class="product-1 product-1-inner product-sky-blue">
                            <a href="javascript:void(0);">
                                            <span class="product-1-img">
                                                <img class="img-responsive center-block" src="images/product/product-1.png" alt="product">
                                                <span class="sold">$ Sold</span>
                                                <span class="money-2">$00.10</span>
                                            </span>
                                <span class="product-1-detail">
                                                <span class="product-1-name">StatTrak™ M9 Bayonet | Crimson Web (Minimal Wear)</span>
                                            </span>
                            </a>
                        </div>
                    </div>
                    <div class="product-1-grid col-xs-4 col-sm-4 col-md-6 col-lg-3">
                        <div class="product-1 product-1-inner product-gray">
                            <a href="javascript:void(0);">
                                            <span class="product-1-img">
                                                <img class="img-responsive center-block" src="images/product/product-1.png" alt="product">
                                                <span class="sold">$ Sold</span>
                                                <span class="money-2">$00.10</span>
                                            </span>
                                <span class="product-1-detail">
                                                <span class="product-1-name">StatTrak™ M9 Bayonet | Crimson Web (Minimal Wear)</span>
                                            </span>
                            </a>
                        </div>
                    </div>
                    <div class="product-1-grid col-xs-4 col-sm-4 col-md-6 col-lg-3">
                        <div class="product-1 product-1-inner product-gray">
                            <a href="javascript:void(0);">
                                            <span class="product-1-img">
                                                <img class="img-responsive center-block" src="images/product/product-1.png" alt="product">
                                                <span class="sold">$ Sold</span>
                                                <span class="money-2">$00.10</span>
                                            </span>
                                <span class="product-1-detail">
                                                <span class="product-1-name">StatTrak™ M9 Bayonet | Crimson Web (Minimal Wear)</span>
                                            </span>
                            </a>
                        </div>
                    </div>
                    <div class="product-1-grid col-xs-4 col-sm-4 col-md-6 col-lg-3">
                        <div class="product-1 product-1-inner product-dark-blue">
                            <a href="javascript:void(0);">
                                            <span class="product-1-img">
                                                <img class="img-responsive center-block" src="images/product/product-1.png" alt="product">
                                                <span class="sold">$ Sold</span>
                                                <span class="money-2">$00.10</span>
                                            </span>
                                <span class="product-1-detail">
                                                <span class="product-1-name">StatTrak™ M9 Bayonet | Crimson Web (Minimal Wear)</span>
                                            </span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <nav aria-label="Page navigation">
                            <ul class="pagination">
                                <li class="previous"><a href="javascript:void(0);" aria-label="Previous">&#60;</a></li>
                                <li><a href="javascript:void(0);">1</a></li>
                                <li class="active"><a href="javascript:void(0);">2</a></li>
                                <li><a href="javascript:void(0);">3</a></li>
                                <li><a href="javascript:void(0);">...</a></li>
                                <li><a href="javascript:void(0);">15</a></li>
                                <li><a href="javascript:void(0);">16</a></li>
                                <li class="next"><a href="javascript:void(0);" aria-label="Next">></a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
            </div>
        </div>
    </main>
@endsection