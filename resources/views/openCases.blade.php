@extends('layouts.main')

@section('content')
    <main class="left-sidebar-before">
        <div class="container-fluid">
            <div class="row order-row">
                <!-- Left block -->
                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 col-lg-2x left-sidebar-out">
                    <div class="left-sidebar">
                        @include('partials.latestwinners')
                        @include('partials.topwinnings')
                    </div>
                </div>

                <!-- Center block -->
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-lg-10x main-content-out">
                    <div class="main-content">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h2 class="title-1 pink-title clearfix">{{$case->name}}<span class="title-1-text">Cases Opened: {{$opened}}</span>
                                </h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="spin-slider-out">
                                    <div class="spin-slider-arrow">
                                        <div class="top"></div>
                                        <div class="bottom"></div>
                                    </div>
                                    <div class="owl-carousel owl-theme spin-slider-1">
                                        @foreach($items as $item)
                                            <div class="item">
                                                <div class="product-spin-1">
                                                    <a href="javascript:void(0);"><img width="219"
                                                                class="owl-item img-responsive center-block"
                                                                src="https://steamcommunity-a.akamaihd.net/economy/image/{{$item->skin->img}}"
                                                                alt="product"></a>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <form action="/winner/post" method="POST">
                                    {{csrf_field()}}
                                    <input type="hidden" name="case" value="{{$case->id}}">
                                    <div class="customNavigation text-center">
                                        <ul class="spin-slider-btn clearfix">
                                            <li>
                                                <button type="button" id="open_case_button" class="btn btn-md btn-blue start">Open Case</button>
                                            </li>
                                            <li>
                                                <button type="button" id="test_spin_button"
                                                        class="btn btn-md btn-pink start">Test Spin
                                                </button>
                                            </li>
                                            <li>
                                                <div class="select-wrapper triangle-border">
                                                    <select id="roulette_counter" class="form-control select-1">
                                                        <option>1</option>
                                                        <option>2</option>
                                                        <option>3</option>
                                                        <option>4</option>
                                                        <option>5</option>
                                                        <option>6</option>
                                                        <option>7</option>
                                                    </select>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="spin-currency">$<span>{{$case->price}}</span></div>
                                            </li>
                                        </ul>
                                        <!-- <a class="btn prev" href="javascript:void(0);">Previous</a>
                                        <a class="btn next" href="javascript:void(0);">Next</a>
                                        <a class="btn play" href="javascript:void(0);">Autoplay</a>
                                        <a class="btn stop" href="javascript:void(0);">Stop</a> -->
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h2 class="title-1 blue-title">Community Created Cases</h2>
                            </div>
                        </div>
                        <div class="row">
                            @foreach($cases as $case1)
                                <div class="product-1-grid col-xs-4 col-sm-4 col-md-6 col-lg-3">
                                    <div class="product-1 product-1-inner product-dark-blue">
                                        <a href="/opencases/{{$case1->id}}">
                                        <span class="product-1-img">
                                            <img class="img-responsive center-block" id="image{{$case1->id}}"
                                                 src="/case_images/{{$case1->image->image_closed}}" alt="product">
                                            <span class="money">${{$case1->price}}</span>
                                        </span>
                                            <span class="product-1-detail">
                                            <span class="product-1-name">{{$case1->name}}</span>
                                        </span>
                                        </a>
                                    </div>
                                </div>
                                <script>
                                    $("#image{{$case1->id}}").on({
                                        "mouseover": function () {
                                            this.src = '/case_images/{{$case1->image->image_opened}}';
                                        },
                                        "mouseout": function () {
                                            this.src = '/case_images/{{$case1->image->image_closed}}';
                                        }
                                    });
                                </script>
                            @endforeach
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h2 class="title-1 blue-title">Recent Winnings from this case</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="product-4-row clearfix">
                                    @foreach($winners as $winner)
                                        <div class="product-4-col">
                                            <div class="product-4 tooltipster-1" title='
                                        <ul class="product-4-list">
                                        <li>Winner: <a class="clr-26bebd" href="javascript:void(0);">TheRodZombie</a></li>
                                        <li>Case: <a class="clr-26bebd" href="javascript:void(0);">StatTrak Case</a></li>
                                        <li>Case Price: <strong class="clr-ec407a">$15.00</strong></li>
                                        <li>Winnings: <strong class="clr-ec407a">$22.78</strong></li>
                                        <li>Content: StatTrak™ P2000 | Fire Elemental (Field-Tested)</li>
                                        </ul>
                                        '
                                                 data-html="true" rel="tooltip">
                                                <a href="javascript:void(0);">
                                                    <img class="img-responsive center-block"
                                                         src="http://steamcommunity.com/economy/image/{{$winner->item->skin->img}}"
                                                         alt="product">
                                                </a>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection