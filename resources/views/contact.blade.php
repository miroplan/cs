@extends('layouts.main')

@section('content')
    <main class="left-sidebar-before">
        <div class="container-fluid">
            <div class="row order-row">
                <!-- Left block -->
                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 col-lg-2x left-sidebar-out">
                    <div class="left-sidebar">
                        @include('partials.topwinnings')
                    </div>
                </div>

                <!-- Center block -->
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-lg-10x main-content-out">
                    <div class="main-content">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h2 class="title-1 pink-title">Contact Us</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="main-content-main clearfix">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                                            <form class="form-2">
                                                <div class="form-group">
                                                    <label class="form-2-label">Name</label>
                                                    <input class="form-control" type="text">
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-2-label">Email Address</label>
                                                    <input class="form-control" type="email">
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-2-label">Phone Number</label>
                                                    <input class="form-control" type="text">
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-2-label">Subject</label>
                                                    <input class="form-control" type="text">
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-2-label">Message</label>
                                                    <textarea class="form-control" rows="5"></textarea>
                                                </div>
                                                <button class="btn btn-lg btn-blue pull-right" type="submit">SUBMIT</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection