@extends('layouts.main')

@section('content')
    <main class="left-sidebar-before">
        <div class="container-fluid">
            <div class="row order-row">
                <!-- Left block -->
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2 col-lg-2x  left-sidebar-out">
                    <div class="left-sidebar">
                        @include('partials.latestwinners')
                        @include('partials.topwinnings')
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-lg-10x main-content-out">
                    <div class="main-content">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <ul class="case-browser-tab clearfix">
                                    @if($filters)
                                        @foreach($filters as $filterKey=>$filter)
                                            <li>
                                                <a @if($filterKey == $type) class="selected" @endif href="{{ route('cases', ['type' => $filterKey]) }}">{{ $filter['label'] }}</a>
                                            </li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                        @if($type == "custom")
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="case-browser-filter clearfix">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-1">
                                            <div class="filter-main clearfix">
                                                <h5>FILTER :</h5>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-11">
                                            <div class="row">
                                                {{ Form::open(['url' => route('cases', $type), 'method' => 'get']) }}
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                            <div class="filter-main">
                                                                {{ Form::number('min_opened', old('min_opened'), ['class' => 'form-control select-1', 'placeholder' => 'Min Opened'])}}
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                            <div class="filter-main">
                                                                {{ Form::number('min_price', old('min_price'), ['class' => 'form-control select-1', 'placeholder' => 'Min Price', 'step' => "0.01"])}}
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                            <div class="filter-main">
                                                                {{ Form::number('max_price', old('max_price'), ['class' => 'form-control select-1', 'placeholder' => 'Max Price', 'step' => "0.01"])}}
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                            <div class="filter-main">
                                                                {{ Form::text('search', old('search'), ['class' => 'form-control select-1', 'placeholder' => 'Search'])}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2">
                                                    <div class="filter-main">
                                                        {{ Form::submit('Search', ['class' => 'btn btn-md btn-blue btn-block'])}}
                                                    </div>
                                                </div>
                                                {{ Form::close() }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="row">
                                @if(count($cases)>0)
                                    @foreach($cases as $case)
                                    <div class="product-1-grid col-xs-4 col-sm-4 col-md-4 col-lg-3">
                                        <div class="product-1">
                                        <a href="/opencases/{{$case->id}}">
                                            <span class="product-1-img">
                                                <img class="img-responsive center-block" id="cases" src="/case_images/{{$case->image->image_closed}}" alt="product">
                                                <span class="money-3">${{$case->price}}</span>
                                            </span>
                                            <span class="product-1-detail">
                                                <span class="product-1-name">{{$case->name}}</span>
                                            </span>
                                        </a>
                                        <script>
                                            $('img[id="cases"]').on({
                                                "mouseover" : function() {
                                                    this.src = '/case_images/{{$case->image->image_opened}}';
                                                },
                                                "mouseout" : function() {
                                                    this.src='/case_images/{{$case->image->image_closed}}';
                                                }
                                            });
                                        </script>
                                    </div>
                                    </div>
                                @endforeach

                            @else
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p>No Cases were found.</p>
                                </div>
                            @endif
                        @if($cases->links())
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <nav aria-label="Page navigation">
                                        {{ $cases->links() }}
                                    </nav>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </main>
@stop

@section('scripts')
    <script>

    </script>
@stop