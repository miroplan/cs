@extends('layouts.main')

@section('content')
    <main class="left-sidebar-before">
        <div class="container-fluid">
            <div class="row order-row">
                <!-- Left block -->
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2 col-lg-2x  left-sidebar-out">
                    <div class="left-sidebar">
                        @include('partials.latestwinners')
                        @include('partials.topwinnings')
                    </div>
                </div>

                <!-- Center block -->
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8 	col-lg-8x main-content-out">
                    <div class="main-content">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h2 class="title-1 pink-title">Official CS:GO Cases</h2>
                            </div>
                        </div>
                        <div class="row">

                            @foreach($cases_official as $key => $inventory)

                                <div class="product-1-grid col-xs-4 col-sm-4 col-md-6 col-lg-3">
                                    <div class="product-1">
                                        <a href="/opencases/{{$inventory->id}}">
                                        <span class="product-1-img">
                                            <img class="img-responsive center-block" id="image{{$inventory->id}}" src="/case_images/{{$inventory->image->image_closed}}" alt="product">
                                            <span class="money">${{$inventory->price}}</span>
                                        </span>
                                            <span class="product-1-detail">
                                            <span class="product-1-name">{{$inventory->name}}</span>
                                        </span>
                                        </a>
                                    </div>
                                    <script>
                                        $("#image{{$inventory->id}}").on({
                                            "mouseover" : function() {
                                                this.src = '/case_images/{{$inventory->image->image_opened}}';
                                            },
                                            "mouseout" : function() {
                                                this.src='/case_images/{{$inventory->image->image_closed}}';
                                            }
                                        });
                                    </script>
                                </div>
                            @endforeach
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h2 class="title-1 blue-title">Community Created Cases</h2>
                            </div>
                        </div>
                        <div class="row">

                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h2 class="title-1 pink-title">CS:GO Steam Casess</h2>
                            </div>
                        </div>
                        <div class="row">

                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h2 class="title-1 pink-title">CS:GO Stickers</h2>
                            </div>
                        </div>
                        <div class="row">

                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h2 class="title-1 pink-title">CS:GO Collections</h2>
                            </div>
                        </div>
                        <div class="row">

                        </div>
                    </div>
                </div>

                <!-- Right block -->
                @include('partials.rightsidebar')
            </div>
        </div>
    </main>
@stop

@section('scripts')
    <script>

    </script>
@stop