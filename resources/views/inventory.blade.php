@extends('layouts.main')

@section('content')
    <main class="left-sidebar-before">
        <div class="container-fluid">
            <div class="row order-row">
                <!-- Left block -->
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2 col-lg-2x  left-sidebar-out">
                    <div class="left-sidebar">
                        @include('partials.topwinnings')
                    </div>
                </div>

                <!-- Center block -->
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8 	col-lg-8x main-content-out">
                    <div class="main-content">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h2 class="title-1 pink-title">Inventory</h2>
                            </div>
                        </div>
                        <div class="row">
                            @if(Auth::guest())
                                <div class="col-xs-12">
                                    You must be logged in first to withdraw.
                                </div>
                            @elseif(isset($error))
                                <div class="col-xs-12">
                                    {{$error}}
                                </div>
                            @else
                                @foreach($user_skins as $key => $inventory)

                                    <div class="product-1-grid col-xs-4 col-sm-4 col-md-6 col-lg-3">
                                        <div class="product-1">
                                            <a href="javascript:void(0);">
                                        <span class="product-1-img">
                                            <img class="img-responsive center-block"
                                                 src="http://steamcommunity.com/economy/image/{{$inventory->icon_url}}"
                                                 alt="product">
                                            <span class="money">{{$inventory->price}} $</span>
                                        </span>
                                                <span class="product-1-detail">
                                            <span class="product-1-name">{{$inventory->market_hash_name}}</span>
                                        </span>
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                                @foreach($user_custom_skins as $inventory)

                                    <div class="product-1-grid col-xs-4 col-sm-4 col-md-6 col-lg-3">
                                        <div class="product-1">
                                            <a data-id="{{$inventory->skin->id}}" class="inventory_info"
                                               href="javascript:void(0);">
                                        <span class="product-1-img">
                                            <img class="img-responsive center-block"
                                                 src="http://steamcommunity.com/economy/image/{{$inventory->skin->img}}"
                                                 alt="product">
                                            <span class="money">{{$inventory->skin->price}} $</span>
                                        </span>
                                                <span class="product-1-detail">
                                            <span class="product-1-name">{{$inventory->skin->market_name}}</span>
                                        </span>
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>

                <!-- Right block -->
                @include('partials.rightsidebar')
            </div>
        </div>


        <!-- Modal -->
        <div id="item_modal" class="modal fade " role="dialog">
            <div class="modal-dialog">
                <div class="winning-model">
                    <span class="textonly item_name"></span>
                    <div class="winning-body">
                        <span class="product-1-img">
                        <img class="img-responsive center-block item_img" src="" alt="product">
                    </span>
                    </div>
                    <span class="model-button">
                        <a href="#" id="sale_item_button" data-id="" class="model-cart-btn" data-dismiss="modal"><i
                                    class="fa fa-shopping-cart" aria-hidden="true"></i> Sell for </a>
                        <a href="#" class="model-withdraw"><i class="fa fa-reply-all" aria-hidden="true"
                                                              data-dismiss="modal"></i> Withdraw Item</a>
                        <a href="#" class="model-marketplace-btn"><i class="fa fa-plus-square" aria-hidden="true"
                                                                     data-dismiss="modal"></i> Add to marketplace</a>
                        <a href="#" class="model-trade-btn"><i class="fa fa-pencil-square-o" aria-hidden="true"
                                                               data-dismiss="modal"></i> Trade up</a>
                    </span>
                    <span class="textonly">
                        <small> Item Must be withdraw within <strong>24 hours</strong> </small>
                    </span>
                </div>
            </div>
        </div>

        <div id="status_modal" class="modal fade " role="dialog">
            <div class="modal-dialog">
                <div class="modal-content" style="background-color: #36414b;">
                    <div class="modal-header ">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Status</h4>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-md btn-blue btn-block " data-dismiss="modal">Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </main>
@stop


@section('scripts')
    @if(isset($winners) && $winners)
        <script>
            $(document).ready(function () {
                getInfo();
            });
        </script>
    @endif
@stop
