@extends('layouts.main')

@section('content')
    <main>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h2 class="title-1 pink-title">TERMS OF SERVICE</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="main-content-main">
                        <div class="main-content-main-row">
                            <h2 class="title-4 mt--50">Basic usage</h2>
                        </div>
                        <div class="terms-section">
                            <p class="clr-26bebd"><strong>No individual under the age of eighteen (18) may use the Service, regardless of any consent from your parent or guardian to use the Service.</strong></p>
                            <ul class="ul-disc">
                                <li>You need a supported Web browser to access the Service.</li>
                                <li>You acknowledge and agree that GOD OF SKINS may cease to support a given Web browser and that your continuous use of the Service will require you to download a supported Web browser.</li>
                                <li>You also acknowledge and agree that the performance of the Service is incumbent on the performance of your computer equipment and your Internet connection.</li>
                                <li>You agree to sign on and register for the Services through your Steam account provided by the Valve Corporation.</li>
                                <li>You are solely responsible for managing your account and password and for keeping your password confidential.</li>
                                <li>You are also solely responsible for restricting access to your account.</li>
                                <li>You agree that you are responsible for all activities that occur on your account or through the use of your password by yourself or by other persons.</li>
                                <li>If you believe that a third party has access your password, use the password regeneration feature of the Service as soon as possible to obtain a new password.</li>
                                <li>In all circumstances, you agree not to permit any third party to use or access the Service.</li>
                                <li>As a condition to your use of the Service, you agree not to:</li>
                                <li>Impersonate or misrepresent your affiliation with any person or entity.</li>
                                <li>Access, tamper with, or use any non-public areas of the Service or GOD OF SKINS’s computer systems.</li>
                                <li>Attempt to probe, scan, or test the vulnerability of the Service or any related system or network or breach any security or authentication measures used in connection with the Service and such systems and networks.</li>
                                <li>Attempt to decipher, decompile, disassemble, reverse engineer or otherwise investigate any of the software or components used to provide the Service.</li>
                                <li>Harm or threaten to harm other users in any way or interfere with, or attempt to interfere with, the access of any user, host or network, including without limitation, by sending a virus, overloading, flooding, spamming, or mail-bombing the Service.</li>
                                <li>Provide payment information belonging to a third party; use the Service in an abusive way contrary to its intended use, to GOD OF SKINS policies and instructions and to any applicable law.</li>
                                <li>Systematically retrieve data or other content from the Service to create or compile, directly or indirectly, in single or multiple downloads, a collection, compilation, database, directory or the like, whether by manual methods, through the use of bots, crawlers, or spiders, or otherwise.</li>
                                <li>Make use of the Service in a manner contrary to the terms and conditions under which third parties provide facilities and technology necessary for the operation of the Service, such as G2A or Valve.</li>
                                <li>Infringe third party intellectual property rights when using or accessing the Service, including but not limited to in making available virtual items by using the Service; make use of, promote, link to or provide access to materials deemed by GOD OF SKINS at its sole discretion to be offensive or cause harm to csgosace reputation, including, but not limited to, illegal content and pornographic content and content deemed offensive or injurious to GOD OF SKINS and/or the Service (such as Warez sites, IRC bots and bittorent sites).</li>
                            </ul>
                        </div>
                        <div class="main-content-main-row">
                            <h2 class="title-4">LIVE CHAT USAGE</h2>
                        </div>
                        <div class="terms-section">
                            <p>You must not post any phishing links, advertisements, pornography or other suspicious links in the chat. Nor should you post your own or other people’s personal information. Spamming or any other type of disruptive and annoying chat behaviour is strictly forbidden. Excessive flaming, racism, sexism and similar offensive behaviour is also not permitted. Failure to abide by any of these usage rules may result in a temporary or permanent ban in our chat rooms. GOD OF SKINS staff reserve the right to ban any user at their own discretion and these decisions may or may not be reverted on special request.</p>
                        </div>
                        <div class="main-content-main-row">
                            <h2 class="title-4">WITHDRAW OF SKINS</h2>
                        </div>
                        <div class="terms-section">
                            <p>You can withdraw your skins by selecting the skins you wish to withdraw and hitting “Withdraw”. A Trade Offer from one or more from our GOD OF SKINS bots with your selected Skins will be sent to your Steam account. Open trade offers will be automatically cancelled by GOD OF SKINS if they are not accepted thirty minutes after opening by a GOD OF SKINS bot. If a trade offer is canceled by a GOD OF SKINS bot, we will return Skins into your GOD OF SKINS account. We are not responsible for the name tags or stickers attached to your Skins. GOD OF SKINS is not responsible for the loss of your Skins as a result of any third-parties including Steam. Using GOD OF SKINS as a trading platform is stricktly forbidden. Users found doing this will be permanently banned from GOD OF SKINS.</p>
                        </div>
                        <div class="main-content-main-row">
                            <h2 class="title-4">REFUNDS</h2>
                        </div>
                        <div class="terms-section">
                            <p>GOD OF SKINS does not offer refunds. By playing on this website you agree that you are always playing at your own risk.</p>
                        </div>
                        <div class="main-content-main-row">
                            <h2 class="title-4">TERMINATION</h2>
                        </div>
                        <div class="terms-section">
                            <p>We may terminate or suspend access to our Service immediately, without prior notice or liability, for any reason whatsoever, including without limitation if you breach the Terms.</p>
                            <p>All provisions of the Terms which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability.</p>
                            <p>We may terminate or suspend your account immediately, without prior notice or liability, for any reason whatsoever, including without limitation if you breach the Terms. Upon termination, your right to use the Service will immediately cease. If you wish to terminate your account, you may simply discontinue using the Service.</p>
                            <p>All provisions of the Terms which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability.</p>
                        </div>
                        <div class="main-content-main-row">
                            <h2 class="title-4">LINKS TO OTHER WEB SITES</h2>
                        </div>
                        <div class="terms-section">
                            <p>Our Service may contain links to third-party web sites or services that are not owned or controlled by GOD OF SKINS.</p>
                            <p>GOD OF SKINS has no control over, and assumes no responsibility for, the content, privacy policies, or practices of any third party web sites or services.</p>
                            <p>You further acknowledge and agree that GOD OF SKINS shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on or through any such web sites or services.</p>
                            <p>We strongly advise you to read the terms and conditions and privacy policies of any third-party web sites or services that you visit.</p>
                        </div>
                        <div class="main-content-main-row">
                            <h2 class="title-4">NO WARRANTIES</h2>
                        </div>
                        <div class="terms-section">
                            <p>This website is provided “as is” without any representations or warranties, express or implied. www.GOD OF SKINS makes no representations or warranties in relation to this website or the information and materials provided on this website.</p>
                            <p>Without prejudice to the generality of the foregoing paragraph, www.GOD OF SKINS does not warrant that: this website will be constantly available, or available at all; or the information on this website is complete, true, accurate or non-misleading.</p>
                            <p>Nothing on this website constitutes, or is meant to constitute, advice of any kind. If you require advice in relation to any legal, financial or medical matter you should consult an appropriate professiona</p>
                        </div>
                        <div class="main-content-main-row">
                            <h2 class="title-4">AFFILIATION</h2>
                        </div>
                        <div class="terms-section">
                            <p> We are in NO WAY affiliated with or endorsed by the Valve corporation, Counter Strike: Global Offensive, Steam or any other trademarks of the Valve corporation. GOD OF SKINS Wallet By topping up an amount to your GOD OF SKINS wallet, you accept that the credits received (labeled by the $-sign) are not worth real money and not subject to any refunds.
                        </div>
                        <div class="main-content-main-row">
                            <h2 class="title-4">PRIVACY</h2>
                        </div>
                        <div class="terms-section">
                            <p>All user data is saved on our servers and prevents you from losing information. In accordance with the Data Protection Act of 6 January 1978, you also have the right to access, rectify, modify and delete data concerning you.</p>
                        </div>
                        <div class="main-content-main-row">
                            <h2 class="title-4">ADDITIONAL TERMS AND CONDITIONS; EULAS</h2>
                        </div>
                        <div class="terms-section">
                            <p>When you use G2A Pay services provided by G2A.COM Limited (hereinafter referred to as the “G2A Pay services provider”) to make a purchase on our website, responsibility over your purchase will first be transferred to G2A.COM Limited before it is delivered to you. G2A Pay services provider assumes primary responsibility, with our assistance, for payment and payment related customer support. The terms between G2A Pay services provider and customers who utilize services of G2A Pay are governed by separate agreements and are not subject to the Terms on this website.</p>
                            <p>With respect to customers making purchases through G2A Pay services provider checkout, (i) the Privacy Policy of G2A Pay services provider shall apply to all payments and should be reviewed before making any purchase, and (ii) the G2A Pay services provider Refund Policy shall apply to all payments unless notice is expressly provided by the relevant supplier to buyers in advance. In addition the purchase of certain products may also require shoppers to agree to one or more End-User License Agreements (or “EULAs”) that may include additional terms set by the product supplier rather than by Us or G2A Pay services provider. You will be bound by any EULA that you agree to.</p>
                            <p>We and/or entities that sell products on our website by using G2A Pay services are primarily responsible for warranty, maintenance, technical or product support services for those Products. We and/or entities that sell products on our website are primarily responsible to users for any liabilities related to fulfillment of orders, and EULAs entered into by the End-User Customer. G2A Pay services provider is primarily responsible for facilitating your payment.</p>
                            <p>You are responsible for any fees, taxes or other costs associated with the purchase and delivery of your items resulting from charges imposed by your relationship with payment services providers or the duties and taxes imposed by your local customs officials or other regulatory body.</p>
                            <p>For customer service inquiries or disputes, You may contact us by email at info.GOD OF SKINS@gmail.com</p>
                            <p>Questions related to payments made through G2A Pay services provider payment should be addressed to support@g2a.com.</p>
                            <p>Where possible, we will work with You and/or any user selling on our website, to resolve any disputes arising from your purchase.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection