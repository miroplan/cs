<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ asset('images/favicon.ico') }}">

    <title>God of Skins</title>

    <!-- Google Font CSS -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i">

    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">

    <!-- Owl Carousel CSS -->
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">

    <!-- Range Slider CSS -->
    <link rel="stylesheet" href="{{ asset('css/range-slider.css') }}">
    <link rel="stylesheet" href="{{ asset('css/range-slider-skin-flat.css') }}">

    <!-- Range Slider CSS -->
    <link rel="stylesheet" href="{{ asset('css/range-slider.css') }}">

    <!-- Scrollbar core CSS -->
    <link rel="stylesheet" href="{{ asset('css/scrollbar.css') }}">

    <!-- Menu CSS -->
    <link rel="stylesheet" href="{{ asset('css/smartmenu-core.css') }}">
    <link rel="stylesheet" href="{{ asset('css/smartmenu.css') }}">

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="{{ asset('css/theme.css') }}">
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="{{ asset('js/html5shiv.min.js') }}"></script>
    <script src="{{ asset('js/respond.min.js') }}"></script>
    <![endif]-->

    <script type="text/javascript" src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
</head>

<body>
<div class="wrapper">

    <header id="main-header">
        <div class="header-top">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="header-logo">
                            <div class="logo">
                                <a href="javascript:void(0);"><img class="img-responsive center-block" src="/images/logo.png" alt="Logo"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-bottom">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="header-social-out">
                            <button class="btn" type="button" data-toggle="collapse" data-target="#social-collapse" aria-expanded="false" aria-controls="social-collapse"><i class="fa fa-share-alt" aria-hidden="true"></i></button>
                            <div class="collapse" id="social-collapse">
                                <ul class="social-links clearfix">
                                    <li><a href="javascript:void(0);"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                    <li><a href="javascript:void(0);"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="javascript:void(0);"><i class="fa fa-steam" aria-hidden="true"></i></a></li>
                                    <li><a href="javascript:void(0);"><i class="fa fa-snapchat-ghost" aria-hidden="true"></i></a></li>
                                    <li><a href="javascript:void(0);"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="nav-out">
                            <nav class="navbar navbar-default" role="navigation">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>
                                <div class="collapse navbar-collapse navbar-ex1-collapse">
                                    <ul id="main-menu" class="nav navbar-nav sm sm-blue">
                                        <li class="{{ Request::segment(1) == '' ? 'active' : '' }}"><a href="/">Home</a></li>
                                        <li class="{{ Request::segment(1) == 'cases' ? 'active' : '' }}" ><a href="/cases">Case Browser</a></li>
                                        <li class="{{ Request::segment(2) == 'case_create' ? 'active' : '' }}" ><a href="/case_create">Case Creator</a></li>
                                        <li class="{{ Request::segment(1) == 'tradeup' ? 'active' : '' }}" ><a href="/tradeup">Trade Up</a></li>
                                        <li><a href="javascript:void(0);">Promotions</a></li>
                                        <li><a href="javascript:void(0);">Partner</a></li>
                                        <li>
                                            <a href="javascript:void(0);">Other</a>
                                            <ul>
                                                <li class="{{ Request::segment(1) == 'inventory' ? 'active' : '' }}"><a href="/inventory">Inventory</a></li>
                                                <li class="{{ Request::segment(1) == 'giftcards' ? 'active' : '' }}"><a href="/giftcards">Gift Cards</a></li>
                                                <li><a href="javascript:void(0);">Affiliates</a></li>
                                                <li class="{{ Request::segment(1) == 'faq' ? 'active' : '' }}"><a href="/faq">FAQ</a></li>
                                                <li><a href="javascript:void(0);">Search by Item</a></li>
                                                <li><a href="javascript:void(0);">Planned Updates</a></li>
                                                <li><a href="/terms">Terms</a></li>
                                                <li><a href="/support">Support</a></li>
                                                <li><a href="/contact">Contact</a></li>
                                                <li><a href="/privacy_policy">Privacy Policy</a></li>
                                            </ul>
                                        </li>
                                        <li class="{{ Request::segment(1) == 'marketplace' ? 'active' : '' }}"><a class="btn-marketplace" href="/marketplace">Marketplace</a></li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                        <div class="header-right">
                            <ul class="header-dropdown clearfix">
                                <li>
                                    <div class="dropdown">
                                        <button class="our-btn" id="language" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="round">
                                                <img class="img-responsive center-block img-circle" src="/images/languages/icn-english.png" alt="English">
                                            </span>
                                            <span class="btn-text">English</span>
                                            <span class="glyphicon glyphicon-option-vertical" aria-hidden="true"></span>
                                        </button>
                                        <div class="dropdown-menu dropdown-right" aria-labelledby="language">
                                            <ul class="even-odd-color clearfix language-ul">
                                                <li>
                                                    <a href="javascript:void(0);">
                                                        <span class="round">
                                                            <img class="img-responsive center-block img-circle" src="/images/languages/icn-english.png" alt="English">
                                                        </span>
                                                        <span class="language-text">English</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);">
                                                        <span class="round">
                                                            <img class="img-responsive center-block img-circle" src="/images/languages/icn-spanish.png" alt="Spanish">
                                                        </span>
                                                        <span class="language-text">Spanish</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);">
                                                        <span class="round">
                                                            <img class="img-responsive center-block img-circle" src="/images/languages/icn-portuguese.png" alt="Portuguese">
                                                        </span>
                                                        <span class="language-text">Portuguese</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);">
                                                        <span class="round">
                                                            <img class="img-responsive center-block img-circle" src="/images/languages/icn-dutch.png" alt="Dutch">
                                                        </span>
                                                        <span class="language-text">Dutch</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);">
                                                        <span class="round">
                                                            <img class="img-responsive center-block img-circle" src="/images/languages/icn-japanese.png" alt="Japanese">
                                                        </span>
                                                        <span class="language-text">Japanese</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);">
                                                        <span class="round">
                                                            <img class="img-responsive center-block img-circle" src="/images/languages/icn-chinese.png" alt="Chinese">
                                                        </span>
                                                        <span class="language-text">Chinese</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);">
                                                        <span class="round">
                                                            <img class="img-responsive center-block img-circle" src="/images/languages/icn-french.png" alt="French">
                                                        </span>
                                                        <span class="language-text">French</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);">
                                                        <span class="round">
                                                            <img class="img-responsive center-block img-circle" src="/images/languages/icn-polish.png" alt="Polish">
                                                        </span>
                                                        <span class="language-text">Polish</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);">
                                                        <span class="round">
                                                            <img class="img-responsive center-block img-circle" src="/images/languages/icn-turkish.png" alt="Turkish">
                                                        </span>
                                                        <span class="language-text">Turkish</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>


    @yield('content')

    <footer id="main-footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2 col-lg-2x">
                    <div class="footer-table">
                        <table class="table">
                            <tbody>
                            <tr>
                                <th>{{ $statOpened }}</th>
                                <td>Cases Opened</td>
                            </tr>
                            <tr>
                                <th>{{ $statPlayer }}</th>
                                <td>Players</td>
                            </tr>
                            <tr>
                                <th>{{ $statOnline }}</th>
                                <td>Online</td>
                            </tr>
                            <tr>
                                <th>0</th>
                                <td>Trade Ups</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-8 col-md-9 col-lg-10">
                    <div class="footer-container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2">
                                <div class="footer-main-out">
                                    <img class="img-responsive center-block" src="images/logo-footer.png" alt="Logo">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-5">
                                <div class="footer-main-out">
                                    <h3>GODOFSKINS.COM © 2017 Terms and Conditions - Privacy Policy – FAQ</h3>
                                    <p>Open CS:GO skin cases for the best prices and best odds All trades work automatically via Steam bots.</p>
                                    <div class="clearfix"></div>
                                    <ul class="footer-logo clearfix">
                                        <li><a class="clr-ffffff" href="mailto:support@godofskins.com" target="_top">support@godofskins.com</a></li>
                                        <li><img class="img-responsive center-block" src="images/g2apay.png" alt="g2apay"></li>
                                        <li><img class="img-responsive center-block" src="images/comodo-secure.png" alt="comodo-secure"></li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                                <div class="footer-main-out">
                                    <ul class="social-links clearfix">
                                        <li><a href="javascript:void(0);"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="javascript:void(0);"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                        <li><a href="javascript:void(0);"><i class="fa fa-steam" aria-hidden="true"></i></a></li>
                                        <li><a href="javascript:void(0);"><i class="fa fa-snapchat-ghost" aria-hidden="true"></i></a></li>
                                        <li><a href="javascript:void(0);"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
                                <div class="footer-main-out">
                                    <img class="img-responsive center-block" src="images/customer-love-this.jpg" alt="customer-love-this">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<script type="text/javascript" src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/scrollbar.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/owl.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/tooltipster.js')}}"></script>
<script type="text/javascript" src="{{asset('js/range-slider.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/smartmenu.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/roulette.js')}}"></script>
<script type="text/javascript" src="{{asset('js/tweenmax/TweenMax.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/tweenmax/TimelineMax.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/custom.js')}}"></script>
<script type="text/javascript" src="{{asset('js/handlebars.js')}}"></script>

<!-- Broadcasting Event -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.1/socket.io.js"></script>
<script type="text/javascript" src="{{asset('js/server.js')}}"></script>
<script type="text/javascript" src="{{asset('js/latestWinners.js')}}"></script>

@yield('scripts')
</body>
</html>
