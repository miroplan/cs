@extends('layouts.main')

@section('content')
<main class="left-sidebar-before">
	<div class="container-fluid">
		<div class="row order-row">
			<!-- Left block -->
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 col-lg-2x left-sidebar-out">
				<div class="left-sidebar">
					@include('partials.topwinnings')
				</div>
			</div>

			<!-- Center block -->
			<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-lg-10x main-content-out">
				<div class="main-content">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<h2 class="title-1 pink-title">
								FAQ : The Smart Way To Answer Questions
							</h2>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="main-content-main main-content-main-faq clearfix">

								<div class="main-content-main-row">

									@if (count($faqs) > 0)

									<div class="panel-group" id="accordion-1" role="tablist" aria-multiselectable="true">

										@for($i = 0; $i < count($faqs); $i++)

										<h2 class="title-5 <? if(!$i) : ?>mt--50<? endif; ?>">
											{{$faqs[$i]->group_title}}
										</h2>

										@foreach(App\Faq::where('parent_id',$faqs[$i]->group_id)->get() as $out)



										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-{{$out->faq_id}}" aria-expanded="false" aria-controls="collapse-{{$out->faq_id}}">
														{{$out->question}}
													</a>
												</h4>
											</div>
											<div id="collapse-{{$out->faq_id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false" >
												<div class="panel-body">
													{!!$out->answer!!}
												</div>
											</div>
										</div>




										@endforeach

										@endfor

									</div>

									@else

									<div>
										List empty
									</div>

									@endif

								</div>

								<div class="terms-footer">
									<p>
										Not found right answer Contact us now
									</p>
									<a class="btn btn-md btn-blue" href="javascript:void(0);">
										Click here
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
@endsection