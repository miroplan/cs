@extends('layouts.main')

@section('content')
    <main class="left-sidebar-before">
        <div class="container-fluid">
            <div class="row order-row">
                <!-- Left block -->
                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 col-lg-2x left-sidebar-out">
                    <div class="left-sidebar">
                        @include('partials.shopfilter')
                    </div>
                </div>

                <!-- Center block -->
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-lg-10x main-content-out">
                    <div class="main-content">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h2 class="title-1 pink-title">iTunes e-Gift Card: 7.0% OFF</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="main-content-main">
                                    <div class="main-content-main-row">
                                        <div class="product-1-detail-more mt--50">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                    <img class="img-responsive center-block" src="images/itunes-product/product-1.png" alt="product">
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                                                    <div class="clearfix product-value-out">
                                                        <span class="product-type">Type: Electronic delivery for online use only</span>
                                                        <div class="row">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <span class="product-title">Value:</span>
                                                                <span class="product-value">$100</span>
                                                            </div>
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <span class="product-title">Price:</span>
                                                                <span class="product-value">$93.00</span>
                                                            </div>
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <span class="product-title">You Save:</span>
                                                                <span class="product-value">7.0%</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-5">
                                                    <button type="button" class="btn btn-md btn-blue btn-product-more">BUY NOW</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <h4>About iTunes</h4>
                                    <p>The Apple iTunes Store offers music, movies, audiobooks, music videos, games, iPhone apps, iTunes U and free podcasts. The iTunes store also offers free iTunes software for playing and organizing digital music and video files on Apple and PC computers, iPods, iPhones and iPads. Buy iTunes gift cards to save on the latest music and movie trends. iTunes gift cards are great gifts for anyone who loves music, movies or books.</p>
                                    <h4>About iTunes</h4>
                                    <ul class="ul-disc">
                                        <li>Delivery & Redemption</li>
                                        <li>Most eGift cards are delivered to your Raise account instantly, but please allow up to 24 hours</li>
                                    </ul>
                                    <h4>Card Details</h4>
                                    <ul class="ul-disc">
                                        <li>Free delivery</li>
                                        <li>Gift cards cannot be used to purchase other gift cards</li>
                                    </ul>
                                    <h4>Terms of Use</h4>
                                    <p>All trademarks not owned by Raise that appear on this site are the property of their respective owners. Raise is not the issuer of any of the gift cards or other closed-loop products on Raise and is not related to any merchant whose trademark and gift cards appear on Raise for sale. All gift cards sold on our marketplace are backed by our 1 Year Money-Back Guarantee. View full Raise Terms of Use.</p>
                                    <h4>iTunes</h4>
                                    <p>Valid only on iTunes Store for U.S. Requires iTunes account and prior acceptance of license and usage terms. To open an account you must be 13+ and in the U.S. Compatible software, hardware, and Internet access required. Not redeemable for cash, no refunds or exchanges (except as required by law). Card may not be used to purchase any other merchandise, allowances or iTunes gifting. Data collection and use subject to Apple Customer Privacy Policy, see www.apple.com/privacy, unless stated otherwise. Risk of loss and title for card passes to purchaser on transfer. Cards are issued and managed by Apple Value Services, LLC (Issuer). Neither Apple nor Issuer is responsible for any loss or damage resulting from lost or stolen cards or use without permission. Apple and its licensees, affiliates, and licensors make no warranties, express or implied, with respect to card or the iTunes Store and disclaim any warranty to the fullest extent available. These limitations may not apply to you. Void where prohibited. Not for resale. Subject to full terms and conditions, see www.apple.com/legal/itunes/us/gifts.html. Content and pricing subject to availability at the time of actual download. Content purchased from the iTunes Store is for personal lawful use only. Don’t steal music. ©2014 Apple Inc. All rights reserved.</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h2 class="title-1 pink-title">Similar e-Gift Card</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="product-1-grid col-xs-4 col-sm-4 col-md-6 col-lg-3">
                                <div class="product-1">
                                    <a href="javascript:void(0);">
                                        <span class="product-1-img">
                                            <img class="img-responsive center-block" src="images/itunes-product/product-1.png" alt="product">
                                            <span class="money">$10.00</span>
                                        </span>
                                        <span class="product-1-detail">
                                            <span class="product-1-name">iTunes Gift Cards</span>
                                            <span class="product-1-price">Save Up to 9.4%</span>
                                        </span>
                                    </a>
                                </div>
                            </div>
                            <div class="product-1-grid col-xs-4 col-sm-4 col-md-6 col-lg-3">
                                <div class="product-1">
                                    <a href="javascript:void(0);">
                                        <span class="product-1-img">
                                            <img class="img-responsive center-block" src="images/itunes-product/product-1.png" alt="product">
                                            <span class="money">$10.00</span>
                                        </span>
                                        <span class="product-1-detail">
                                            <span class="product-1-name">iTunes Gift Cards</span>
                                            <span class="product-1-price">Save Up to 9.4%</span>
                                        </span>
                                    </a>
                                </div>
                            </div>
                            <div class="product-1-grid col-xs-4 col-sm-4 col-md-6 col-lg-3">
                                <div class="product-1">
                                    <a href="javascript:void(0);">
                                        <span class="product-1-img">
                                            <img class="img-responsive center-block" src="images/itunes-product/product-1.png" alt="product">
                                            <span class="money">$10.00</span>
                                        </span>
                                        <span class="product-1-detail">
                                            <span class="product-1-name">iTunes Gift Cards</span>
                                            <span class="product-1-price">Save Up to 9.4%</span>
                                        </span>
                                    </a>
                                </div>
                            </div>
                            <div class="product-1-grid col-xs-4 col-sm-4 col-md-6 col-lg-3">
                                <div class="product-1">
                                    <a href="javascript:void(0);">
                                        <span class="product-1-img">
                                            <img class="img-responsive center-block" src="images/itunes-product/product-1.png" alt="product">
                                            <span class="money">$10.00</span>
                                        </span>
                                        <span class="product-1-detail">
                                            <span class="product-1-name">iTunes Gift Cards</span>
                                            <span class="product-1-price">Save Up to 9.4%</span>
                                        </span>
                                    </a>
                                </div>
                            </div>
                            <div class="product-1-grid col-xs-4 col-sm-4 col-md-6 col-lg-3">
                                <div class="product-1">
                                    <a href="javascript:void(0);">
                                        <span class="product-1-img">
                                            <img class="img-responsive center-block" src="images/itunes-product/product-1.png" alt="product">
                                            <span class="money">$10.00</span>
                                        </span>
                                        <span class="product-1-detail">
                                            <span class="product-1-name">iTunes Gift Cards</span>
                                            <span class="product-1-price">Save Up to 9.4%</span>
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection