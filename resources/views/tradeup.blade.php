@extends('layouts.main')

@section('content')
    <main class="left-sidebar-before">
        <div class="container-fluid">
            <div class="row order-row">
                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 col-lg-2x left-sidebar-out">
                    <div class="left-sidebar">
                    @include('partials.topwinnings')
                        <!-- <div class="abc"></div> -->
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-lg-10x main-content-out">
                    <div class="main-content">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h2 class="title-1 pink-title">TRADE UP</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                <div class="trade-up clearfix"> <h2> GOT SOME UNWANTED SKINS BY GOD OF SKINS CASES ON OUR SITE?
                                        <span>YOU CAN TRADE THESE TO RECEIVE THE BETTER ONE!</span></h2>



                                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 trade-up-left">

                                        <div class="top-button">
                                            <a href="#"><i class="fa fa-youtube" aria-hidden="true"></i> Refresh </a> <a href="#"><i class="fa fa-check-square" aria-hidden="true"></i> Add All to List </a><div class="clearfix"></div>
                                        </div>
                                        <div class="datasection">
                                            <ul><li><span>1/10</span>
                                                    2 items remaining </li><li class="li2x"><span>$9.00</span>
                                                    Items sum </li></ul>

                                        </div>
                                        <div class="trade-up-button">
                                            <a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> trade up</a>
                                        </div>
                                        <h4>You will receive item <br>

                                            worth <span>$9.00</span> to <span class="color2">$9.00</span></h4>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 trade-up-right">
                                        <ul>
                                            <li><a href="#">
                                                    <img src="images/product/product-2.png" class="img-responsive" > <span>AWP | Worm God </span> </a>

                                            </li><li><a href="#" class="selected">
                                                    <img src="images/product/product-2.png" class="img-responsive" > <span>AWP | Worm God </span> </a>

                                            </li><li><a href="#">
                                                    <img src="images/product/product-2.png" class="img-responsive" > <span>AWP | Worm God </span> </a>

                                            </li><li><a href="#">
                                                    <img src="images/product/product-2.png" class="img-responsive" > <span>AWP | Worm God </span> </a>

                                            </li><li><a href="#">
                                                    <img src="images/product/product-2.png" class="img-responsive" > <span>AWP | Worm God </span> </a>

                                            </li>
                                            <li><a href="#">
                                                    <img src="images/product/product-2.png" class="img-responsive" > <span>AWP | Worm God </span> </a>

                                            </li><li><a href="#">
                                                    <img src="images/product/product-2.png" class="img-responsive" > <span>AWP | Worm God </span> </a>

                                            </li><li><a href="#">
                                                    <img src="images/product/product-2.png" class="img-responsive" > <span>AWP | Worm God </span> </a>

                                            </li><li><a href="#">
                                                    <img src="images/product/product-2.png" class="img-responsive" > <span>AWP | Worm God </span> </a>

                                            </li><li><a href="#">
                                                    <img src="images/product/product-2.png" class="img-responsive" > <span>AWP | Worm God </span> </a>

                                            </li> <div class="clearfix"></div>
                                        </ul>

                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h2 class="title-1 pink-title">HOW DOES GOD OF SKINS CONTRACT WORK?</h2>
                            </div>
                        </div>
                        <div class="row contractwork">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 contactlist">
                                <span>1</span>
                                You can still utilise unneeded skins you’ve got by opening our cases to receive one way more better item!
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 center-contract contactlist">
                                <span>2</span>
                                Put 3 to 10 skins into the contract, the final item’s price depends on the summary cost of the skins used!

                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 contactlist">
                                <span>3</span>
                                Sign contract and enjoy your new decent skin or use it again to get even more expensive one!


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@stop

@section('scripts')
    <script>

    </script>
@stop