@extends('layouts.main')

@section('content')
    <main class="left-sidebar-before">
        <div class="container-fluid">
            <div class="row order-row">
                <!-- Left block -->
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2 col-lg-2x  left-sidebar-out">
                    <div class="left-sidebar">
                        @include('partials.shopfilter')
                    </div>
                </div>

                <!-- Center block -->
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-lg-10x main-content-out">
                    <div class="main-content">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h2 class="title-1 pink-title">Gift Cards</h2>
                                <div class="select-most-popular">
                                    <div class="select-wrapper triangle-border">
                                        <select class="form-control select-1">
                                            <option>Sort By Most Popular</option>
                                            <option>Sory by Date</option>
                                            <option>Sory by Week</option>
                                            <option>Sory by Month</option>
                                            <option>Sory by Month</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="product-1-grid col-xs-4 col-sm-4 col-md-6 col-lg-3">
                                <!-- NO PRODUCTS -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@stop

@section('scripts')
    <script>

    </script>
@stop