@extends('layouts.admin')
@section('title', 'Cases')
@section('header')
        <a href="/admin/case/create" class="btn btn-primary pull-right">Create case</a>
@endsection

@section('content')
    <hr>
    <section class="content">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 " >
                <div class="box table-responsive">
                    <div class="box-header">
                        <h3 class="box-title">Official cases</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body" style="height: 100%;overflow-y: scroll;">
                        <table id="table" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Preview</th>
                                    <th>Price</th>
                                    <th>Name</th>
                                    <th>Date</th>
                                    <th>Items</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($cases_official as $case)
                                <tr>
                                    <td>
                                        <div style="padding: 5px;">
                                            <img id="image" src="/case_images/{{$case->image->image_closed}}" class="img-responsive">
                                        </div>
                                        <script>
                                            $("img").on({
                                                "mouseover" : function() {
                                                    this.src = '/case_images/{{$case->image->image_opened}}';
                                                },
                                                "mouseout" : function() {
                                                    this.src='/case_images/{{$case->image->image_closed}}';
                                                }
                                            });
                                        </script>

                                    </td>
                                    <td>{{$case->price}}</td>
                                    <td>{{$case->name}}</td>
                                    <td>{{$case->created_at}}</td>
                                    <td>{{count($case->items)}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
                <div class="box table-responsive">
                    <div class="box-header">
                        <h3 class="box-title">Community cases</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body" style="height: 100%;overflow-y: scroll;">
                        <table id="table" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Preview</th>
                                <th>Price</th>
                                <th>Name</th>
                                <th>Date</th>
                                <th>Items</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($cases_community as $case)
                                <tr>
                                    <td>{{$case->case_image_id}}</td>
                                    <td>{{$case->price}}</td>
                                    <td>{{$case->name}}</td>
                                    <td>{{$case->created_at}}</td>
                                    <td>{{count($case->items)}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
@endsection