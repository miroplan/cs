@extends('layouts.admin')
@section('title', 'Add question/answer')
@section('content')

<hr>

<div style="width: 50%; margin: 10px auto; ">

	<div id="info" style="display:none;" class= "alert alert-success" role= "alert" >
		Faq added.
	</div>

	<form id="faq_form" onsubmit="return false;">

		<div class="form-group">

			<label>Select groups</label>

			<select id="group" class="form-control">

				@foreach($groups as $group)

				<option
					@if(session('group_id') && session('group_id') == $group['group_id']) selected @endif value="{{$group['group_id']}}">{{@strip_tags($group['group_title'])}}
				</option>

				@endforeach

			</select>

		</div>

		<div class="form-group">

			<label>Question</label>

			<input type="text" id="question" class="form-control" >

		</div>

		<div class="form-group">

			<label>Answer</label>

			<textarea id="answer" ></textarea>

		</div>

		<button type="button" id="btn_add" class="btn btn-primary">Add</button>

	</form>

</div>

<script type="text/javascript">

	$(function()
		{
			var validate =
			{

				form: function (list)
				{

					var valid = true;

					$(list).each(function(index,val)
						{

							var input = $(val);

							var val = input.val().replace(/<br>/,'');

							var formGrout = input.parents('.form-group');

							var label = formGrout.find('label').text().toLowerCase();

							var textError = 'Enter the ' + label;

							if (val.length == 0)
							{

								formGrout.addClass('has-error').removeClass('has-success');

								input.tooltip(
									{

										trigger: 'manual',

										placement: 'right',

										title: textError

									}).tooltip('show');

								valid = false;

							} else
							{

								input.tooltip('hide');

								formGrout.removeClass('has-error').addClass('has-success');

							}

						});

					return valid;

				},

				removeError: function ()
				{

					$(this).find('input,textarea').tooltip('destroy');

					$(this).removeClass('has-error').addClass('has-success');

				}

			};

			$('#faq_form').on('keydown','.has-error',validate.removeError);

			$('#btn_add').click(function(e)
				{

					if (!validate.form('input[id=question],textarea[id=answer]')) return false;

					$(this).attr('disabled','true');

					$.ajax(
						{

							url: '/admin/faq/create',

							method: 'POST',

							data:
							{

								_token: '{{ csrf_token() }}',

								group: $('#group').select().val(),

								question: $('#question').val(),

								answer: $('#answer').val()


							},

							success: function ()
							{

								$('#faq_form')[0].reset();

								$('#info').css('display','block');

								$('iframe').contents().find('body').html('');

								window.setTimeout(function()
									{
										
										$('#info').css('display','none');

										$('#btn_add').removeAttr('disabled');

									},3000);

							},

							error: function (xhr,status,text)
							{

                                $('#btn_add').removeAttr('disabled');

								alert(text);

							}

						});

				});

			$("#answer").htmlarea({toolbar: ["html", "|","forecolor","|", "bold", "italic", "underline", "|", "h1", "h2", "h3", "|", "link", "unlink"],css: "{{asset('/admin_template/jHtmlArea/css/jHtmlArea.editor.css')}}"});

		});

</script>

@endsection