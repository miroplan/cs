@extends('layouts.admin')
@section('title', 'Create new case')
@section('content')
    <hr>
    <section class="content">
        <form action="/admin/store_case" method="POST">
            {{csrf_field()}}
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="box table-responsive">
                    <div class="box-header">
                        <h3 class="box-title">Select name</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <input type="text" name="name" class="form-control" placeholder="enter name">
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="box table-responsive">
                    <div class="box-header">
                        <h3 class="box-title">Is Featured?</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <input type="checkbox" name="is_featured">
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="box table-responsive">
                    <div class="box-header">
                        <h3 class="box-title">Select Image</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body" style="max-width: 1200px;overflow-x: scroll;">
                        @foreach($images as $image)
                            <label class="col-md-3">
                                <input type="radio" name="image_id" id="image_{{$image->id}}" value="{{$image->id}}" >

                                    <img id="image" src="/case_images/{{$image->image_closed}}" class="img-responsive">
                                <script>
                                    $("img").on({
                                        "mouseover" : function() {
                                            this.src = '/case_images/{{$image->image_opened}}';
                                        },
                                        "mouseout" : function() {
                                            this.src='/case_images/{{$image->image_closed}}';
                                        }
                                    });
                                </script>
                            </label>
                            @endforeach
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="box table-responsive">
                    <div class="box-header">
                        <h3 class="box-title">Select Affiliate Cut</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body" style="width: 100%;overflow-x: scroll;">
                        <input type="number" name="affiliate" step="0.01" min="0" max="20">
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="box table-responsive">
                    <div class="box-header">
                        <h3 class="box-title">Select Skin</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body" style="height: 700px;overflow-y: scroll;">
                        @foreach($bot_skins as $skin)
                            <label class="col-md-3">
                                <input type="checkbox" name="skin_id[]"  value="{{$skin['id']}}" >
                                <img id="image" src="http://steamcommunity.com/economy/image/{{$skin['icon_url']}}" class="img-responsive">
                            </label>
                        @endforeach
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="box table-responsive">
                    <div class="box-header">
                        <h3 class="box-title">Odds</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                Case price <small></small>  Total odds <small></small>  <button type="submit" class="btn btn-success">Create Case</button>
           </div>
        </div>
        </form>
    </section>
@endsection