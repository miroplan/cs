@extends('layouts.admin')
@section('title', 'Create group:')
@section('content')

<form class="form-inline" action="/admin/group/create" method="post">

	<input class="form-control mb-2 mr-sm-2 mb-sm-0" type="text" name="title" maxlength="255" placeholder="Title" required>

	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	&nbsp;
	<button type="submit" id="btn_add" class="btn btn-primary">Add</button>

</form>


@if ($groups)

@foreach ($groups as $group)

<div class="row bp" id="group{{$group->group_id}}" >

	<div class="col-md-10">

		<span id="title{{$group->group_id}}" >{{$group->group_title}}</span>

		<span id="edit_title{{$group->group_id}}" style="display:none;">
		<input class="form-control mb-2 mr-sm-2 mb-sm-0" style="border-style: none none solid none; border-width: 1px;" type="text" name="title" 
		 value="{{$group->group_title}}">
		</span>

	</div>

	<div class="col-md-2">

		<span id="menu_btn{{$group->group_id}}">
			<button type="button" class="btn-primary" data-title="edit" data-id="{{$group->group_id}}">Edit</button>&nbsp;&nbsp;
			<button type="button"  class="btn-danger" data-title="delete" data-id="{{$group->group_id}}" >Delete</button>
		</span>

		<span style="display:none;" id="menu_edit_btn{{$group->group_id}}">
			<button type="button" class="btn-primary" data-title="save" data-id="{{$group->group_id}}">Save</button>&nbsp;&nbsp;
			<button type="button"  class="btn-default" data-title="cancel" data-id="{{$group->group_id}}" >Cancel</button>
		</span>

	</div>

</div>

@endforeach

@else

<div class="empty">List empty</div>

@endif

<script type="text/javascript">

	$(function()
		{

			$('button[data-title=edit]').click(function(e)
				{

					$('#title' + e.target.getAttribute('data-id')).css('display','none');
                             
					$('#edit_title' + e.target.getAttribute('data-id')).css('display','block');

					$('#menu_btn' + e.target.getAttribute('data-id')).css('display','none');

					$('#menu_edit_btn' + e.target.getAttribute('data-id')).css('display','block');

				});


			var cancel = function (e)
			{

				$('#edit_title' + e.target.getAttribute('data-id')).css('display','none');

				$('#title' + e.target.getAttribute('data-id')).css('display','block');

				$('#menu_edit_btn' + e.target.getAttribute('data-id')).css('display','none');

				$('#menu_btn' + e.target.getAttribute('data-id')).css('display','block');

			}

			$('button[data-title=cancel]').click(cancel);


			$('button[data-title=delete]').click(function (e)
				{

					$.ajax(
						{

							url: '/admin/group/remove',

							method: 'POST',

							data:
							{

								_token: '{{ csrf_token() }}',

								id: e.target.getAttribute('data-id')

							},

							success: function ()
							{

								$('#group' + e.target.getAttribute('data-id')).remove();

							}

						});

				});

			$('button[data-title=save]').click(function (e)
				{

					var input = $('#edit_title' + e.target.getAttribute('data-id')).children('input');

					if (input.val().length == 0)
					{

						$('#edit_title' + e.target.getAttribute('data-id')).addClass('has-error');

						input.tooltip(
							{

								trigger: 'manual',

								placement: 'top',

								title: 'Enter the title'

							}).tooltip('show');

						return false;

					} else
					{

						input.tooltip('destroy');

						$('#edit_title' + e.target.getAttribute('data-id')).removeClass('has-error');

					}

					$.ajax(
						{

							url: '/admin/group/update',

							method: 'POST',

							data:
							{

								_token: '{{ csrf_token() }}',

								id: e.target.getAttribute('data-id'),

								title: input.val()

							},

							success: function ()
							{

								$('#title' + e.target.getAttribute('data-id')).text(input.val());

								cancel(e);

							},

							error: function (xhr,status,text)
							{

								alert(text);

							}

						});

				});


		});

</script>

@endsection