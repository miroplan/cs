@if (count($faqs))

@foreach ($faqs as $faq)

<div class="row bp" id="faq{{$faq->faq_id}}" >

	<div class="col-md-10">

		<a id="question{{$faq->faq_id}}">{{$faq->question}}</a>

		<div id="answer{{$faq->faq_id}}" style="word-break: break-all;">{!!$faq->answer!!}</div>

	</div>

	<div class="col-md-2">

		<span id="menu_btn{{$faq->faq_id}}">
			<button type="button" class="btn-primary" data-title="edit" data-id="{{$faq->faq_id}}" onclick="amc.modal(event);">Edit</button>&nbsp;&nbsp;<button type="button" class="btn-danger" data-title="delete" data-id="{{$faq->faq_id}}" onclick="amc.remove(event);" >Delete</button>
		</span>

	</div>

</div>

@endforeach

@else

<div class="empty">List empty</div>

@endif
