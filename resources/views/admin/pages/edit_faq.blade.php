@extends('layouts.admin')
@section('title', 'Edit question/answer')
@section('content')

<form class="form-inline">

	<select id="select_group" class="form-control mb-2 mr-sm-2 mb-sm-0" style="width: 50%;">

		<option selected value="0">
			Select group
		</option>

		@foreach($groups as $group)

		<option value="{{$group['group_id']}}">{{@strip_tags($group['group_title'])}}</option>

		@endforeach

	</select>

</form>

<div id="faq">

</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					&times;
				</button>
				<h4 class="modal-title" id="myModalLabel">
					Q/A
				</h4>
			</div>
			<div class="modal-body">

				<div class="center-block">

					<form id="form_update" onsubmit="return false;">

						<div class="form-group">

							<label>Question</label>

							<input type="text" id="question" class="form-control" >

						</div>

						<div class="form-group">

							<label>Answer</label>

							<textarea id="answer"></textarea>

						</div>

					</form>

				</div>

			</div>

			<div class="modal-footer">
			
			<button type="button" class="btn btn-primary" id="btn_update">Update</button>
			
			</div>

		</div>
	</div>
</div>

<script>

	$(function()
		{

			$('#select_group').change(function (e)
				{

					if (Number(e.target.options[e.target.selectedIndex].value) > 0)
					{

						$.ajax(
							{

								url: '/admin/faq/get',

								method: 'POST',

								data:
								{

									_token: '{{ csrf_token() }}',

									id: Number(e.target.options[e.target.selectedIndex].value)

								},

								success: function (data)
								{

									$('#faq').html(data);

								}

							});

					}

				});


			var validate =
			{

				form: function (list)
				{

					var valid = true;

					$(list).each(function(index,val)
						{

							var input = $(val);

							var val = input.val().replace(/<br>/,'');

							var formGrout = input.parents('.form-group');

							var label = formGrout.find('label').text().toLowerCase();

							var textError = 'Enter the ' + label;

							if (val.length == 0)
							{

								formGrout.addClass('has-error').removeClass('has-success');

								input.tooltip(
									{

										trigger: 'manual',

										placement: 'top',

										title: textError

									}).tooltip('show');

								valid = false;

							} else
							{

								input.tooltip('hide');

								formGrout.removeClass('has-error').addClass('has-success');

							}

						});

					return valid;

				},

				removeError: function ()
				{

					$(this).find('input,textarea').tooltip('destroy');

					$(this).removeClass('has-error').addClass('has-success');

				}

			};



			$('#form_update').on('keydown','.has-error',validate.removeError);

			$('#btn_update').click(function(e)
				{

					if (!validate.form('input[id=question],textarea[id=answer]')) return false;

					$(this).attr('disabled','true');

					$.ajax(
						{

							url: '/admin/faq/update',

							method: 'POST',

							data:
							{

								_token: '{{ csrf_token() }}',

								id: amc.faq_id,

								question: $('#question').val(),

								answer: $('#answer').val()

							},

							success: function (data)
							{

								$('#question' + amc.faq_id).text($('#question').val());

								$('#answer' + amc.faq_id).html($('#answer').val());

								$('#btn_update').removeAttr('disabled');

								$('#myModal').modal('hide');

							},

							error: function (xhr,status,text)
							{

								alert(text);

							}

						});


				});


			$("#answer").htmlarea({toolbar: ["html", "|","forecolor","|", "bold", "italic", "underline", "|", "h1", "h2", "h3", "|", "link", "unlink"],css: "{{asset('/admin_template/jHtmlArea/css/jHtmlArea.editor.css')}}"});


		});

	var amc =
	{

		modal: function (e)
		{

			this.faq_id = e.target.getAttribute('data-id');

			$('#question').val($('#question' + e.target.getAttribute('data-id')).text());

			$('iframe').contents().find('body').html($('#answer' + e.target.getAttribute('data-id')).html());

			$('#myModal').modal();

		},

		remove: function (e)
		{

			$.ajax(
				{

					url: '/admin/faq/remove',

					method: 'POST',

					data:
					{

						_token: '{{ csrf_token() }}',

						id: e.target.getAttribute('data-id')

					},

					success: function ()
					{

						$('#faq' + e.target.getAttribute('data-id')).remove();

					}

				});

		}


	};

</script>

@endsection