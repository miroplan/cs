@extends('layouts.admin')
@section('title', 'Case Images')
@section('header')
    <a href="/admin/cases/images/create" class="btn btn-primary">Add image</a>
@endsection
@section('content')
    <hr/>
    <section class="content">
        <div class="row">
            @foreach($images as $image)
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 " >
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                    <div style="padding: 5px;">
                        <img id="image" src="/case_images/{{$image->image_closed}}" class="img-responsive">
                    </div>
                        <script>
                            $("img").on({
                                "mouseover" : function() {
                                    this.src = '/case_images/{{$image->image_opened}}';
                                },
                                "mouseout" : function() {
                                    this.src='/case_images/{{$image->image_closed}}';
                                }
                            });
                        </script>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
                @endforeach

        </div>
    </section>

@endsection