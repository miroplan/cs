@extends('layouts.admin')
@section('title', 'Create Images')
@section('content')
    <hr>
    <section class="content">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="box table-responsive">
                    <div class="box-header">
                        <h3 class="box-title">Create image</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form role="form" action="/admin/store_image" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Closed case Image</label>
                                    <input type="file" name="image_closed" accept="image/*" required>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Opened case Image</label>
                                    <input type="file" name="image_opened" accept="image/*" required>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Background Color</label>
                                    <input type="text" name="background" placeholder="enter CODE">
                                </div>
                            </div>

                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Create image</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
@endsection