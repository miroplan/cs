<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu tree" data-widget="tree">
            <li class="header"><a href="/admin">Dashboard</a></li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-bomb"></i> <span>Cases</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu" style="display: none;">
                    <li><a href="/admin/cases"><i class="fa fa-circle-o"></i> Cases</a></li>
                    <li><a href="/admin/cases/images"><i class="fa fa-circle-o"></i> Case Images</a></li>
                </ul>
            </li>



<li class="treeview">
                <a href="#">
                    <i class="fa fa-bomb"></i> <span>FAQ</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu" style="display: none;">
                    <li><a href="/admin/group"><i class="fa fa-circle-o"></i>Create group</a></li>
                    <li><a href="/admin/faq"><i class="fa fa-circle-o"></i>Create questions and answers</a></li>

<li><a href="/admin/faq/edit"><i class="fa fa-circle-o"></i>Edit questions and answers</a></li>


                </ul>
            </li>

            
        </ul>
    </section>
</aside>