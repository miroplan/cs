@extends('layouts.main')

@section('content')
    <main class="left-sidebar-before">
        <div class="container-fluid">
            <div class="row order-row">
                <!-- Left block -->
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2 col-lg-2x  left-sidebar-out">
                    <div class="left-sidebar">
                        @include('partials.topwinnings')
                    </div>
                </div>

                <!-- Center block -->
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-8 main-content-out">
                    <div class="main-content">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h2 class="title-1 pink-title">Official CS:GO Cases</h2>
                            </div>
                        </div>
                        <div class="row">

                            @foreach($bots_inventory as $key => $inventory)

                            <div class="product-1-grid col-xs-4 col-sm-4 col-md-6 col-lg-3">
                                <div class="product-1">
                                    <a href="javascript:void(0);">
                                        <span class="product-1-img">
                                            <img class="img-responsive center-block" src="http://steamcommunity.com/economy/image/{{$inventory['icon_url']}}" alt="product">
                                            <span class="money">${{$inventory['price']}}</span>
                                        </span>
                                        <span class="product-1-detail">
                                            <span class="product-1-name">{{$inventory['market_hash_name']}}</span>
                                        </span>
                                    </a>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <nav aria-label="Page navigation">
                                    <ul class="pagination">
                                        <li class="previous"><a href="javascript:void(0);" aria-label="Previous">&#60;</a></li>
                                        <li><a href="javascript:void(0);">1</a></li>
                                        <li class="active"><a href="javascript:void(0);">2</a></li>
                                        <li><a href="javascript:void(0);">3</a></li>
                                        <li><a href="javascript:void(0);">...</a></li>
                                        <li><a href="javascript:void(0);">15</a></li>
                                        <li><a href="javascript:void(0);">16</a></li>
                                        <li class="next"><a href="javascript:void(0);" aria-label="Next">></a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Right block -->
                @include('partials.rightsidebar')
            </div>
        </div>
    </main>
@stop

@section('scripts')
    <script>

    </script>
@stop