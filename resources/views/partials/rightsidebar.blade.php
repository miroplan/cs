<div class="col-xs-12 col-sm-6 col-md-3 col-lg-2 right-sidebar-out">
    <div class="right-sidebar">
        <div class="right-box">
            <div class="bg-28bebd login-box">
                @if (Auth::guest())
                    <a class="btn-login clearfix" href="{{ route('login') }}">
                        <span class="btn-login-left"><i class="fa fa-steam" aria-hidden="true"></i></span>
                        <span class="btn-login-right">
                            <span>LOG IN THROUGH</span>
                            <span class="bottom">STEAM</span>
                        </span>
                    </a>
                @else
                    <?php
                        $profile = App\UserProfile::where('user_id',Auth::user()->id)->first();
                        $avatar =$profile->avatar;
                     ?>

                    <div class="title-2 dropdown">
                        <a class="user-info clearfix"  aria-haspopup="true" aria-expanded="false" href="/profile">
                            <span class="user-info-left">
                                <img class="img-responsive center-block img-circle" src="{{$avatar}}" alt="product">
                            </span>
                            <span class="user-info-right">
                                <span>Welcome {{ Auth::user()->name }}</span>
                                <span class="bottom">Your balance : $0.00</span>
                            </span>
                        </a>
                    </div>

                    <a class="btn btn-block btn-deposit" href="javascript:void(0);">DEPOSIT MONEY</a>

                    <a class="btn-login clearfix" href="javascript:void(0);" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <span class="btn-login-left"><i class="fa fa-steam" aria-hidden="true"></i></span>
                        <span class="btn-login-right">
                            <span class="logout_button">LOG OUT</span>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                {{ csrf_field() }}
                            </form>
                        </span>
                    </a>
                @endif
            </div>
        </div>

        <div class="right-box">
            <a href="javascript:void(0);"><img class="img-responsive center-block" src="images/banner/hurry-up.jpg" alt="banner"></a>
        </div>

        <div class="right-box">
            <div class="chat-out">
                <h2 class="chat-title pink-title">Live Chat</h2>
                <div class="chat-ul">
                    <div class="chat-ul-padding" id="chat_messages">
                    </div>
                </div>
                <div class="chat-reply">
                    <form id="chat_form">
                        <input id="chat_input" @if (Auth::guest()) disabled @else data-username="{{ Auth::user()->name }}" @endif class="form-control" placeholder="Type your message" type="text">
                        <button @if (Auth::guest()) disabled @endif class="reply-btn" type="submit">
                            <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        var socket = io('http://' + window.location.host + ':3000');
        var currentUsername = $('#chat_input').data('username');

        // Send notifications to all connected users that this user became online
        socket.emit('chat.user.connect', {'username': currentUsername});

        // Send message to all connected users
        $('#chat_form').submit(function(){
            if (currentUsername) {
                socket.emit('chat.message', {
                    'username': currentUsername,
                    'message': $('#chat_input').val()
                });
            }
            $('#chat_input').val('');
            return false;
        });

        // Add received chat message to list of messages
        socket.on('chat.message', function(data){
            var chatMsgLi = $('<div class="chat-li"></div>');

            chatMsgLi.append($('<span class="status bg-28bebd"></span>'));
            chatMsgLi.append($('<h6>').text(data.username));
            chatMsgLi.append($('<p>').text(data.message));

            $('#chat_messages').prepend(chatMsgLi);
        });

        // Set red circle to disconnected user
        socket.on('chat.user.disconnect', function (username) {
            setChatUserStatus(username, 'offline')
        });

        // Set blue circle to connected user
        socket.on('chat.user.connect', function (username) {
            setChatUserStatus(username, 'online')
        })
    });

    // Change user status by changing circle color before user name
    function setChatUserStatus(username, status) {
        var chatLiList = document.getElementsByClassName('chat-li');
        for (var i = 0; i < chatLiList.length; i++) {
            if ($(chatLiList[i]).find('h6').length > 0
                && $(chatLiList[i]).find('h6')[0].innerText == username
                && $(chatLiList[i]).find('span.status').length > 0
            )
            {
                var statusSpan = $(chatLiList[i]).find('span.status')[0];
                if (status == 'online') {
                    $(statusSpan).removeClass('bg-ec407a').addClass('bg-28bebd');
                } else if (status == 'offline') {
                    $(statusSpan).removeClass('bg-28bebd').addClass('bg-ec407a');
                }
            }
        }
    }
</script>