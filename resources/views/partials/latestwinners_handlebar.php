<script id="latest-winners-template" type="text/x-handlebars-template">
    <div class="product-2">
        <div class="product-2-img">
            <img class="img-responsive center-block" src="http://steamcommunity.com/economy/image/{{skin_image}}" alt="product">
        </div>
        <div class="product-2-detail product-2-detai-overlay-in">
            <h3 class="product-2-name">Purchase Price:<strong>{{cart_price}}</strong></h3>
            <p class="product-2-price">Payout Price:<strong>{{item_price}}</strong></p>
        </div>
        <div class="product-2-detail product-2-detai-overlay-out">
            <h3 class="product-2-name"><a href="javascript:void(0);">{{market_name}}</a></h3>
        </div>
    </div>
</script>
