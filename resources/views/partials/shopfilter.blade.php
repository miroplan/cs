<div class="shop-by-title">Shop By</div>
<h2 class="title-3">Specials</h2>
<ul class="list-links">
    <li><a href="javascript:void(0);">On Sale</a></li>
    <li><a href="javascript:void(0);">Weekend Offer</a></li>
</ul>
<h2 class="title-3">By Price</h2>
<div class="range-slider-1">
    <input class="range-slider" type="text">
</div>
<h2 class="title-3">By Categories</h2>
<ul class="list-links">
    <li><a href="javascript:void(0);">Arts and Crafts</a></li>
    <li><a href="javascript:void(0);">Automotive</a></li>
    <li><a href="javascript:void(0);">Baby and Kids</a></li>
    <li><a href="javascript:void(0);">Beauty</a></li>
    <li><a href="javascript:void(0);">Books and Magazines</a></li>
    <li><a href="javascript:void(0);">Computer and Software</a></li>
    <li><a href="javascript:void(0);">Department Stores</a></li>
    <li><a href="javascript:void(0);">Education</a></li>
    <li><a href="javascript:void(0);">Electronics</a></li>
    <li><a href="javascript:void(0);">Entertainment</a></li>
    <li><a href="javascript:void(0);">Finance and Business</a></li>
    <li><a href="javascript:void(0);">Flowers and Gifts</a></li>
    <li><a href="javascript:void(0);">Food and Beverage</a></li>
    <li><a href="javascript:void(0);">Health and Wellness</a></li>
    <li><a href="javascript:void(0);">Home and Garden</a></li>
    <li><a href="javascript:void(0);">Jewelry and Watches</a></li>
    <li><a href="javascript:void(0);">Men’s Apparel</a></li>
    <li><a href="javascript:void(0);">Office Supplies</a></li>
    <li><a href="javascript:void(0);">Pets</a></li>
    <li><a href="javascript:void(0);">Relationship Services</a></li>
    <li><a href="javascript:void(0);">Restaurants</a></li>
    <li><a href="javascript:void(0);">Shoes</a></li>
    <li><a href="javascript:void(0);">Sports and Outdoors</a></li>
    <li><a href="javascript:void(0);">Toys</a></li>
    <li><a href="javascript:void(0);">Travel</a></li>
    <li><a href="javascript:void(0);">Women’s Apparel</a></li>
</ul>
<div class="select-wrapper icn-bars">
    <select class="form-control select-2">
        <option>By Location</option>
        <option>Location 1</option>
        <option>Location 2</option>
        <option>Location 3</option>
        <option>Location 4</option>
        <option>Location 5</option>
        <option>Location 6</option>
    </select>
</div>