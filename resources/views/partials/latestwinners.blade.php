<div class="title-2 dropdown">
    <button class="icn-btn btn-block" id="sort-by" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <h2>Latest Winners</h2>
    </button>
</div>
<div class="left-scroll">

    @include('partials.latestwinners_handlebar')

    <div class="left-product latestWinners">
        @if(count($latestWinners) > 0)
            @foreach($latestWinners as $winner)
            <div class="product-2">
                <div class="product-2-img">
                    <img class="img-responsive center-block" src="http://steamcommunity.com/economy/image/{{$winner->item->skin->img}}" alt="product">
                </div>
                <div class="product-2-detail product-2-detai-overlay-in">
                    <h3 class="product-2-name">Purchase Price:<strong>${{$winner->cart->price}}</strong></h3>
                    <p class="product-2-price">Payout Price:<strong>${{$winner->item->price}}</strong></p>
                </div>
                <div class="product-2-detail product-2-detai-overlay-out">
                    <h3 class="product-2-name"><a href="javascript:void(0);">{{$winner->item->skin->market_hash_name}}</a></h3>
                </div>
            </div>
            @endforeach
        @endif
    </div>
</div>