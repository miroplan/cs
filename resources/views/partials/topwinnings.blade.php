<div class="title-2 dropdown">
    <button class="icn-btn btn-block" id="sort-by" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <h2>Top Winners</h2>
        <span class="icn-main glyphicon glyphicon-option-vertical" aria-hidden="true"></span>
    </button>
    <div class="dropdown-menu dropdown-left" aria-labelledby="sort-by">
        <ul class="even-odd-color">
            <li><a href="javascript:void(0);">Sort by Date</a></li>
            <li><a href="javascript:void(0);">Sort by Week</a></li>
            <li><a href="javascript:void(0);">Sort by Month</a></li>
            <li><a href="javascript:void(0);">Sort by Year</a></li>
        </ul>
    </div>
</div>
<div class="left-scroll">
    <div class="left-product">
        @if(count($winnings) > 0)
        @foreach($winnings as $winner)
        <div class="product-2">
            <div class="product-2-img">
                <a href="javascript:void(0);">
                    <img class="img-responsive center-block" src="http://steamcommunity.com/economy/image/{{$winner->item->skin->img}}" alt="product">
                </a>
            </div>
            <div class="product-2-detail product-2-detai-overlay-in">
                <h3 class="product-2-name">Purchase Price:<strong>${{$winner->cart->price}}</strong></h3>
                <p class="product-2-price">Payout Price:<strong>${{$winner->item->price}}</strong></p>
            </div>
            <div class="product-2-detail product-2-detai-overlay-out">
                <a href="javascript:void(0);" class="btn"><img class="img-responsive center-block img-circle" src="images/icn-right-product-2.png" alt="right arrow"></a>
                <h3 class="product-2-name"><a href="javascript:void(0);">{{$winner->item->skin->market_hash_name}}</a></h3>
                <p class="product-2-price"><strong>54 850 Sales</strong></p>
            </div>
        </div>
        @endforeach
        @endif

    </div>
</div>