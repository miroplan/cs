@extends('layouts.main')

@section('content')
    <main class="left-sidebar-before">
        <div class="container-fluid">
            <div class="row order-row">
                <!-- Left block -->
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2 col-lg-2x  left-sidebar-out">
                    <div class="left-sidebar">
                        @include('partials.topwinnings')
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8 	col-lg-8x main-content-out">
                    <div class="main-content">
                @if(Auth::guest())
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <h2 class="title-1 pink-title">Case creator</h2>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                You must be logged in first to create new case.
                            </div>
                @else

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h2 class="title-1 pink-title">Create your own case</h2>
                                <input type="hidden" name="user" value="{{Auth::user()->id}}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="casecreator-section">
                                    <h3 class="title-7">#1 Enter Case Name</h3>

                                    <textarea class="form-control" id="case_name" placeholder="Enter the case’s name here..."></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="casecreator-section">
                                    <h3 class="title-7">#2 Choose Case Picture</h3>
                                    <div class="spin-slider-2-out" style="min-height: 160px;">
                                        <div class="owl-carousel owl-theme spin-slider-2" >
                                            @foreach($images as $image)
                                           <div class="item spin-slider-2-link">
                                                <div class="product-spin-2">
                                                    <input type="radio" name="image_id" class="hidden" id="image_id" value="{{$image->id}}" >
                                                    <a href="javascript:void(0);"><img class="img-responsive center-block" src="/case_images/{{$image->image_opened}}" alt="product"></a>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                        <div id="ellipsis" class="text-center">
                                            <img src="/images/ellipsis.gif" class="img-responsive " style="display: block; margin: 0 auto;">
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="casecreator-section">
                                    <h3 class="title-7">#3 Set Your Affiliate Cut</h3>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-2">
                                            <input class="form-control affiliate-input-lg" name="affilate"  value="0.00%" type="text">
                                        </div>
                                        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-10">
                                            <div class="range-slide-2-design">
                                                <input class="range-slide-2" type="text">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="casecreator-section">
                                    <div class="row">
                                        <div class="col-xs-12  col-md-6"><h3 class="title-7">#4 Add Your Skins</h3></div>
                                        <div class="col-xs-12  col-md-6">
                                            <input class="form-control search-input" placeholder="Search skins" id="search" type="text">
                                        </div>
                                    </div>

                                    <div class="row" id="skins_tab" style="max-height: 570px;">
                                        @foreach($bot_skins as $bot_skin)
                                        <div class="product-1-grid-2 col-xs-4 col-sm-4 col-md-6 col-lg-3">
                                            <div class="product-1">
                                                <a class="product-1-select-hover" id="{{$bot_skin['id']}}" href="javascript:void(0);">
                                                                <span class="product-1-img">
                                                                    <img class="img-responsive center-block" src="https://steamcommunity-a.akamaihd.net/economy/image/{{$bot_skin['img']}}" alt="product">
                                                                    <span class="money-4">${{$bot_skin['price']}}</span>
                                                                </span>
                                                    <span class="product-1-detail">
                                                                    <span class="product-1-name">{{$bot_skin['market_name']}}</span>
                                                                </span>
                                                </a>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="casecreator-section product-detail-table">
                                    <h3 class="title-7">#5 Choose Odds</h3>
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tbody id="creator_odds">
                                                <!-- odd items -->
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="footer">
                                        <ul class="clearfix">
                                            <li><p class="table-text-btn">Total Odds <span id="odds_span">0.0%</span></p></li>
                                            <li><p class="table-text-btn">Case Price <span id="price_span">N/A</span></p></li>
                                            <li class="pull-right"><button type="button" id="submit_btn" class="btn btn-md btn-blue btn-block disabled">Create Case</button></li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>

                <!-- Right block -->
                @include('partials.rightsidebar')
            </div>
        </div>
        <!-- Modal -->
        <div id="case_modal" class="modal fade " role="dialog" >
            <div class="modal-dialog" >
                <div class="modal-content" style="background-color: #36414b;">
                    <div class="modal-header ">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Case</h4>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-md btn-blue btn-block " data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection