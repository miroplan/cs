@extends('layouts.main')

@section('content')
    <main class="left-sidebar-before">
        <div class="container-fluid">
            <div class="row order-row">
                <!-- Left block -->
                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2  col-lg-2x left-sidebar-out">
                    <div class="left-sidebar">
                        @include('partials.topwinnings')
                    </div>
                </div>

                <!-- Center block -->
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10  col-lg-10x main-content-out">
                    <div class="main-content">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h2 class="title-1 pink-title">Support</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="main-content-main clearfix">
                                    <div class="main-content-main-row">
                                        <h2 class="title-4 mt--50 support-title">If you are are experiencing any issues with our game or transactions, please submit a ticket and we will provide you with help within a 48 hour period.</h2>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                                            <form class="form-2">
                                                <div class="main-content-main-row">
                                                    <h2 class="title-5">1. What is the issue that you are experiencing?</h2>
                                                </div>
                                                <div class="form-group">
                                                    <div class="select-wrapper">
                                                        <select class="form-control">
                                                            <option>Select your issue by clicking here</option>
                                                            <option>Your issue 1</option>
                                                            <option>Your issue 2</option>
                                                            <option>Your issue 3</option>
                                                            <option>Your issue 4</option>
                                                            <option>Your issue 5</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="main-content-main-row">
                                                    <h2 class="title-5">1. Problems with withdraw system</h2>
                                                </div>
                                                <div class="form-group">
                                                    <ul class="support-ul clearfix">
                                                        <li>Is this the first time you try to withdraw? </li>
                                                        <li>
                                                            <input id="radio-1" class="magic-radio" name="test" type="radio">
                                                            <label class="checkbox-radio-label" for="radio-1">Yes</label>
                                                        </li>
                                                        <li>
                                                            <input id="radio-2" class="magic-radio" name="test" type="radio">
                                                            <label class="checkbox-radio-label" for="radio-2">No</label>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-2-label">What message appears when you try to withdraw?</label>
                                                    <textarea class="form-control" rows="5"></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-2-label">Write here additional information about your problem.</label>
                                                    <textarea class="form-control" rows="5"></textarea>
                                                </div>
                                                <button class="btn btn-lg btn-blue pull-right" type="submit">SUBMIT</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection