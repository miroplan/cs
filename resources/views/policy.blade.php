@extends('layouts.main')

@section('content')
    <main class="left-sidebar-before">
        <div class="container-fluid">
            <div class="row order-row">
                <!-- Left block -->
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2 col-lg-2x  left-sidebar-out">
                    <div class="left-sidebar">
                        @include('partials.topwinnings')
                    </div>
                </div>
    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-lg-10x main-content-out">
        <div class="main-content">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h2 class="title-1 pink-title">PRIVACY POLICY AGREEMENT</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="main-content-main clearfix">
                        <div class="mt--50 terms-section">
                            <p>GOD OF SKINS  respects the privacy of its online visitors and users of its products and services and complies with the Privacy Shield Framework as agreed to by the US Department of Commerce and the European Commission. GOD OF SKINS recognizes the importance of protecting information collected from users and has adopted this privacy policy to inform users about how GOD OF SKINS gathers, stores, and uses information derived from their use of GOD OF SKINS products, services and online sites.</p>
                            <p>This policy is current as of its last revision date. However, please note that this policy may be amended from time to time to reflect changes and additions to the privacy policy. Please check back for the most current version before relying on any of the provisions in this privacy policy.</p>
                        </div>
                        <div class="main-content-main-row">
                            <h2 class="title-6">Basic usage</h2>
                        </div>
                        <div class="terms-section">
                            <p>By using GOD OF SKINS’s online sites, products, and services, users agree that GOD OF SKINS collects and processes the personally identifiable information (as defined below) they provide by creating a Steam account, by purchasing or selling subscriptions, or during any exchange with GOD OF SKINS in connection with the use of their Steam account. GOD OF SKINS will not share any personally identifiable information with other parties except as described in this policy. GOD OF SKINS also processes anonymous data, aggregated or not, in order to analyze and produce statistics related to the habits, usage patterns, and demographics of users as a group or as individuals. Such anonymous data does not allow the identification of the users to which it relates. GOD OF SKINS may share anonymous data, aggregated or not, with third parties.</p>
                        </div>
                        <div class="main-content-main-row">
                            <h2 class="title-6">Personally Identifiable Information</h2>
                        </div>
                        <div class="terms-section">
                            <p>“Personally identifiable information” is information that can be used to uniquely identify a user such as name, address or credit card number. If you are an EU user, “personally identifiable information” means “personal data” within the meaning of article 2 a) of the Directive 95/46 of 24 October 1995. You may be asked to provide personally identifiable information during the sign in process, and then in connection with the use of GOD OF SKINS’s products, services and online sites. While GOD OF SKINS collects personally identifiable information on a voluntary basis, for certain products and services GOD OF SKINS’s collection of personally identifiable information may be a requirement for access to the product or service or to process a user’s order.</p>
                        </div>
                        <div class="main-content-main-row">
                            <h2 class="title-6">Use of personally identifiable information</h2>
                        </div>
                        <div class="terms-section">
                            <p>Personally identifiable information is used internally by GOD OF SKINS to deliver, develop and improve products, content and services, to which users have subscribed, and to answer users’ requests. In addition, GOD OF SKINS may allow third parties performing services under contract with GOD OF SKINS, such as order or payment processors or merchandise warehouse and fulfillment services, located in and outside the European Union, to access and use personally identifiable information, but only to the extent necessary to provide those services.</p>
                            <p>GOD OF SKINS may use personally identifiable information provided by users to send them information about GOD OF SKINS, including news about product updates, contests, events, and other promotional materials, but only if the users agree to receive such communications. GOD OF SKINS will not share any personally identifiable information with third parties for marketing purposes without your consent.</p>
                            <p>When you create a Steam account, GOD OF SKINS collects a user’s email address and username, and at the user’s option, first and last name. Depending on their settings, users agree that some of this information may be searchable and available to other users within Steam. GOD OF SKINS has no obligation to keep private personally identifiable information that a user makes available to other users via Steam or other GOD OF SKINS software, such as in multiplayer chat or other public functions. </p>
                            <p>External websites and companies with links to and from GOD OF SKINS’s online sites and products may collect personally identifiable information about users when users visit their websites. Third party publishers may also collect personally identifiable information as a requirement of accessing their games or content. GOD OF SKINS’s privacy policy does not extend to external websites and companies or third party publishers with links to or from GOD OF SKINS’s products and online websites or who collect personally identifiable information through their game available on Steam. Please refer directly to these companies and websites regarding their privacy policies.</p>
                            <p>External websites and companies with links to and from GOD OF SKINS’s online sites and products may collect personally identifiable information about users when users visit their websites. Third party publishers may also collect personally identifiable information as a requirement of accessing their games or content. GOD OF SKINS’s privacy policy does not extend to external websites and companies or third party publishers with links to or from GOD OF SKINS’s products and online websites or who collect personally identifiable information through their game available on Steam. Please refer directly to these companies and websites regarding their privacy policies.</p>
                        </div>
                        <div class="main-content-main-row">
                            <h2 class="title-6">Children Under The Age of 13</h2>
                        </div>
                        <div class="terms-section">
                            <p>GOD OF SKINS will not knowingly collect personally identifiable information from any person under the age of 13. GOD OF SKINS encourages parents to instruct their children to never give out personal information when online. Parents who are concerned about the transfer of personal information from their children may contact </p>
                        </div>
                        <div class="main-content-main-row">
                            <h2 class="title-6">Cookies and Other Information on a User’s Machine</h2>
                        </div>
                        <div class="terms-section">
                            <p>GOD OF SKINS’s products, services and sites may employ cookies and other technologies such as web beacons and pixel tags. Cookies are bits of electronic information that can be transferred to a user’s hard drive to customize a person’s use of a product or online site, keep records of a user’s access to an online site or product, or store information needed by the user on a regular basis. Use of cookies is typically associated with websites. For example, when you sign in to Steam, GOD OF SKINS stores your user id and a combination of your IP address and time of login as a cookie on your hard drive. This cookie allows you to move from page to page without having to sign in again on each page. Similarly, if you enter information during your session, such as age verification data, this will be stored as a cookie and you will not have to re-enter such information during that session. We may also use third party web site analytic tools that employ cookies to collect certain non-personally identifiable information concerning use of our websites, such as referring web site addresses, page views and browser types.</p>
                            <p>However, users can disable cookies by changing their browser settings. <br> GOD OF SKINS also stores on a user’s hard drive a unique authorization key or CD-Key that is either entered by the user or downloaded automatically during product registration. This authorization key is used to identify a user as valid and allow access to GOD OF SKINS’s products, services and third party games. This key is necessary for GOD OF SKINS to supply any products and services subscribed by users.</p>
                            <p>Information regarding Steam billing, a user’s Steam account, a user’s Internet connection and the GOD OF SKINS software installed on a user’s computer is uploaded to the server in connection with the user’s use of Steam and GOD OF SKINS software. GOD OF SKINS software automatically generates and submits to GOD OF SKINS bug reports upon a crash or other fault in the GOD OF SKINS software, services or Steam games. This automatically generated bug report information may include information about other software or hardware on a user‘s system. GOD OF SKINS does not associate and store the automatically generated bug report information with users’ personally identifiable information. Any bug reporting information manually submitted by users (e.g. through GOD OF SKINS’s Bug Reporter form) is associated and stored with personally identifiable information sent voluntarily by the user through a bug report.</p>
                        </div>
                        <div class="main-content-main-row">
                            <h2 class="title-6">Chat Forums, Etc.</h2>
                        </div>
                        <div class="terms-section">
                            <p>GOD OF SKINS’s products or services may provide chat, forums, bulletin boards, or instant messaging tools to users. Any information that is disclosed in chat, forums or bulletin boards should be considered public information, and users who message one another may not know each other personally. GOD OF SKINS has no obligation to keep private personally identifiable information that a user makes available to other users or the public using these functions. Users should exercise caution when deciding to disclose personal information in public forums. Users should also be aware that game play information disclosed during multiplayer game play (e.g. player name and current player statistics) is public information and may be displayed to other users.</p>
                        </div>
                        <div class="main-content-main-row">
                            <h2 class="title-6">Choice/Opt-out</h2>
                        </div>
                        <div class="terms-section">
                            <p>GOD OF SKINS gives users the option to receive promotional email communications from GOD OF SKINS. Users may opt out of these communications. Instructions for discontinuing receipt of promotional email communications from GOD OF SKINS will be provided in email communications to users.</p>
                        </div>
                        <div class="main-content-main-row">
                            <h2 class="title-6">Storage and Security of Personally Identifiable Information</h2>
                        </div>
                        <div class="terms-section">
                            <p>Personally identifiable information provided to GOD OF SKINS will be collected, processed and stored by GOD OF SKINS Corporation in the United States. GOD OF SKINS has taken reasonable steps to protect the information users share with us, including, but not limited to, setup of processes, equipment and software to avoid unauthorized access or disclosure of this information.</p>
                        </div>
                        <div class="main-content-main-row">
                            <h2 class="title-6">Payment Data</h2>
                        </div>
                        <div class="terms-section">
                            <p>Payment processing related to Content and Services and/or physical goods purchased on Steam is performed by either GOD OF SKINS Corporation directly or by GOD OF SKINS’s fully owned subsidiary GOD OF SKINS GmbH on behalf of GOD OF SKINS Corporation depending on the type of payment method used. If your card was issued outside the United States, your payment may be processed via a European acquirer by GOD OF SKINS GmbH on behalf of GOD OF SKINS Corporation. For any other type of purchases, payment will be collected by GOD OF SKINS Corporation directly. In any case, delivery of Content and Services as well as physical goods is performed by GOD OF SKINS Corporation.</p>
                        </div>
                        <div class="main-content-main-row">
                            <h2 class="title-6">Corrections, Updates and Removal of Personally Identifiable Information</h2>
                        </div>
                        <div class="terms-section">
                            <p>If you are concerned about the accuracy of personally identifiable information maintained by GOD OF SKINS or wish to review, access, amend or correct it, or would like your personally identifiable information removed from GOD OF SKINS’s records or otherwise deactivated, please contact us here. We may decline requests that are unreasonably repetitive, require disproportionate technical effort, jeopardize the privacy of others, or are extremely impractical.</p>
                        </div>
                        <div class="main-content-main-row">
                            <h2 class="title-6">Contacting GOD OF SKINS</h2>
                        </div>
                        <div class="terms-section">
                            <p>If you have any questions or complaints about this privacy policy, or the privacy practices of GOD OF SKINS, please contact us here. You may also contact GOD OF SKINS Corp. at the address below.</p>
                            <p><strong>GOD OF SKINS Corporation <br> P.O. Box 1688 <br> Bellevue, WA 98004</strong></p>
                        </div>
                        <div class="main-content-main-row">
                            <h2 class="title-6">Privacy Shield</h2>
                        </div>
                        <div class="terms-section">
                            <p>GOD OF SKINS with the EU-U.S. Privacy Shield Framework as set forth by the U.S. Department of Commerce regarding the collection, use, and retention of personal information transferred from the European Union to the United States. GOD OF SKINS has certified to the Department of Commerce that it adheres to the Privacy Shield Principles. If there is any conflict between the terms in this privacy policy and the Privacy Shield Principles, the Privacy Shield Principles shall govern. To learn more about the Privacy Shield program, and to view our certification, please visit http://www.privacyshield.gov.</p>
                            <p>In compliance with the Privacy Shield Principles, GOD OF SKINS commits to resolve complaints about our collection or use of your personal information. European Union individuals with inquiries or complaints regarding our Privacy Shield policy should first contact GOD OF SKINS here. If you have an unresolved privacy or data use concern that we have not addressed satisfactorily, please contact our U.S.-based third party dispute resolution provider (free of charge) at https://feedback-form.truste.com/watchdog/request. As explained in the Privacy Shield documentation (https://www.privacyshield.gov/article?id=ANNEX-I-introduction) certain residual claims not resolved by other means may be subject to binding arbitration. In such event, an arbitration option will be made available to you.</p>
                            <p>The Privacy Shield Principles describe GOD OF SKINS’s accountability for personal data that it subsequently transfers to a third party agent. Under the Principles, GOD OF SKINS shall remain liable if third party agents process the personal information in a manner inconsistent with the Principles, unless GOD OF SKINS proves it is not responsible for the event giving rise to the damage.</p>
                            <p>The Federal Trade Commission has jurisdiction over GOD OF SKINS’s compliance with the Privacy Shield.</p>
                            <p>Revision Date: January 1st, 2017</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </main>
@endsection