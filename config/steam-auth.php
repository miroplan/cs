<?php

return [

    /*
     * Redirect URL after login
     */
    'redirect_url' => '/login',
    /*
     * API Key (http://steamcommunity.com/dev/apikey)
     */
    'api_key' => '3A8F5F3CE22D285BBE8B2154358C73E9',
    /*
     * Is using https ?
     */
    'https' => false

];
