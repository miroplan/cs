<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use GuzzleHttp\Client;

class GuzzleProvider extends ServiceProvider {

    const GET = 'GET';
    const POST = 'POST';
    const TIMEOUT = 30;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind('GuzzleHttp\Client', function () {
            return new Client([
                'timeout' => static::TIMEOUT,
            ]);
        });
    }

}