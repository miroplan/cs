<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\SteamTradeService;

class SteamTradeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Services\SteamTradeService', function ($app) {
            return new SteamTradeService(
                $app['App\Services\SteamAPIConnectorService'],
                $app['App\Services\SteamBotAuthenticatorContainerService'],
                $app['App\Services\SteamInventoryService']
            );
        });
    }
}
