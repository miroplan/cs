<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\OPSkinsAPIConnectorService;

class OPSkinsAPIConnectorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Services\OPSkinsAPIConnectorService', function ($app) {
            return new OPSkinsAPIConnectorService(
                $app['GuzzleHttp\Client'],
                env('OPSKINS_API_KEY')
            );
        });
    }
}
