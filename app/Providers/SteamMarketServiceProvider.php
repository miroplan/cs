<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\SteamMarketService;

class SteamMarketServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Services\SteamMarketService', function ($app) {
            return new SteamMarketService(
                $app['App\Services\SteamAPIConnectorService'],
                $app['App\Services\SteamTransactionService']
            );
        });
    }
}
