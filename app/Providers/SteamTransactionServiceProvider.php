<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\SteamTransactionService;
use App\Traits\BotConfigsTrait;

class SteamTransactionServiceProvider extends ServiceProvider
{
    use BotConfigsTrait;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Services\SteamTransactionService', function ($app) {
            $botConfig = iterator_to_array($this->getBotConfigs(1))[0];

            return new SteamTransactionService($botConfig['STEAM_64_ID']);
        });
    }
}
