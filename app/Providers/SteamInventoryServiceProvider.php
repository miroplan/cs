<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\SteamInventoryService;
use App\Services\SteamAPIConnectorService;

class SteamInventoryServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Services\SteamInventoryService', function ($app) {
            return new SteamInventoryService(
                $app['App\Services\SteamAPIConnectorService'],
                $app['App\Services\SteamTransactionService']
            );
        });
    }
}
