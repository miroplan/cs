<?php

namespace App\Providers;

use App\CaseCart;
use App\User;
use App\Winner;
use Illuminate\Support\ServiceProvider;
use Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
   


            Schema::defaultStringLength(191);

            $winnings = Winner::with('item','cart','user')->get();
            if(!$winnings){
                $winnings=[];
            }
            $opened = CaseCart::opened();
            view()->share('winnings',$winnings);
            view()->share('opened',$opened);

            /* Latest Winners */

            $latestWinners = Winner::with('item','cart','user')->orderBy('created_at', 'DESC')->limit(3)->get();

            view()->share('latestWinners', $latestWinners ? $latestWinners : []);

            /* END Latest Winners */

            /* Statistics */

            $user = new User();

            view()->share('statOnline', count($user->allOnline()));
            view()->share('statPlayer', User::count());
            view()->share('statOpened', CaseCart::sum('open_count'));

            /* END Statistics */
  
  

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
