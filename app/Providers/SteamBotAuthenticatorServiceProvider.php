<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\SteamBotAuthenticatorContainerService;
use App\Traits\BotConfigsTrait;

class SteamBotAuthenticatorServiceProvider extends ServiceProvider {

    use BotConfigsTrait;

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Services\SteamBotAuthenticatorContainerService', function ($app) {
            $containerService = new SteamBotAuthenticatorContainerService();

            foreach ($this->getBotConfigs() as $config) {
                $containerService->addBot($config);
            }

            return $containerService;
        });
    }

}