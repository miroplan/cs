<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\CaseHelperService;

class CaseServiceProvider extends ServiceProvider
{


	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
	    $this->app->bind('App\Services\CaseHelperService', function ($app) {
		    return new CaseHelperService();
	    });
    }
}
