<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\SteamAPIConnectorService;
use App\Traits\BotConfigsTrait;

class SteamAPIConnectorServiceProvider extends ServiceProvider {

    use BotConfigsTrait;

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Services\SteamAPIConnectorService', function ($app) {
            return new SteamAPIConnectorService(
                $app['GuzzleHttp\Client'],
                env('STEAM_API_KEY')
            );
        });
    }

}