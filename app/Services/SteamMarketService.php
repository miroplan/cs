<?php

namespace App\Services;

use App\Skin;
use App\SkinMarketPrice;
use Faker\Provider\zh_TW\DateTime;


/**
 * Class SteamMarketService
 * @package App\Services
 */
class SteamMarketService
{
    /**
     * @var SteamAPIConnectorService
     */
    protected $steamAPI;

    /**
     * @var SteamTransactionService
     */
    protected $transactor;

    /**
     * SteamMarketService constructor.
     * @param SteamAPIConnectorService $steamAPI
     * @param SteamTransactionService $transactor
     */
    public function __construct(SteamAPIConnectorService $steamAPI, SteamTransactionService $transactor)
    {
        $this->steamAPI = $steamAPI;
        $this->transactor = $transactor;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    protected function getAllCommoditySkins()
    {
        return Skin::query()
            ->where('virtual', 0)
            ->where('commodity', 1)
            ->get(['id', 'market_hash_name']);
    }

    public function savePricesToDB(array $marketResults, array $notFound = [])
    {
        $marketResultMap = function ($result, $key) {
            unset($result['success']);
            $result['lowest_price'] = str_replace('$', null, $result['lowest_price']);
            $result['median_price'] = str_replace('$', null, $result['median_price']);
            $result['skin_id'] = $key;
            $result['price_not_found'] = 0;
            $result['updated_at'] = new \DateTime();
            $result['created_at'] = $result['updated_at'];
            return $result;
        };

        $notFoundMap = function($result, $key) {
            $result['skin_id'] = $key;
            $result['price_not_found'] = 1;
            $result['lowest_price'] = 0;
            $result['median_price'] = 0;
            $result['volume'] = 0;
            $result['updated_at'] = new \DateTime();
            $result['created_at'] = $result['updated_at'];
            return $result;
        };

        SkinMarketPrice::insert(collect($marketResults)->map($marketResultMap)
            ->merge(collect($notFound)->map($notFoundMap))->all());
    }

    public function updateSkinPrices()
    {
        $skins = $this->getAllCommoditySkins();
        $notFound = [];
        $marketResults = [];

        foreach ($skins as $skin) {

            $result = $this->steamAPI->officialMarketSearch($skin->market_hash_name);

            if (is_null($result)) {
                $notFound[$skin->id] = $skin;
                continue;
            }

            $marketResults[$skin->id] = $result;

        }
        
        if (empty($notFound)) {
            goto RET;
        }
        
        foreach ($notFound as $skin)
        {
            $result = $this->steamAPI->nonOfficialMarketSearch($skin->market_hash_name);

            if (is_null($result)) {
                $notFound[$skin->id] = [];
                continue;
            }

            $marketResults[$skin->id] = $result;

            unset($notFound[$skin->id]);
        }

        RET :

        $this->savePricesToDB($marketResults, $notFound);

        return [
            'success' => $marketResults,
            'error' => $notFound,
        ];
    }
}