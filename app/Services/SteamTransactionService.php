<?php

namespace App\Services;

use App\Models\GameBet;
use App\User;
use App\Models\UserSkin;
use App\Models\GameRoom;
use App\Models\Skin;
use DB;

class SteamTransactionService
{
    protected $accountId;

    /**
     * SteamTransactionService constructor.
     * @param $accountId
     */
    public function __construct($accountId)
    {
        $this->accountId = $accountId;
    }

    public function betCoin(User $user, $amount, GameRoom $gameRoom)
    {
        $result = DB::transaction(function() use ($user, $gameRoom, $amount)
        {
            $userSkin = new UserSkin();
            $skin = $this->getCoinSkin();
            $userSkin->skin()->associate($skin);
            $userSkin->user()->associate($user);
            $userSkin->coin = 1;
            $userSkin->save();
            $balance = $this->draftUserBalance($user, $amount);
            $gameBet = new GameBet();
            $gameBet->userSkin()->associate($userSkin);
            $gameBet->gameRoom()->associate($gameRoom);
            $gameBet->deposited_from = $amount;
            $gameBet->save();

            return ['balance' => $balance, 'gameBet' => $gameBet, 'skin' => $skin];
        });

        return $result;
    }

    protected function getCoinSkin()
    {
        $skin = Skin::where('virtual', 1)->first();

        if (!$skin instanceof Skin) {
            $skin = new Skin();
            $skin->virtual = 1;
            $skin->icon_url = '/images/coin64.png';
            $skin->app_id = 0;
            $skin->name = 'Coin';
            $skin->type = 'Coin';
            $skin->save();
        }

        return $skin;
    }

    public function setUserBalance($user, $balance)
    {
        $user = $this->getUserForBalance($user);
        $user->account_balance = $balance;
        $user->save();

        return true;
    }

    public function draftUserBalance($user, $amount)
    {
        $user = $this->getUserForBalance($user);
        $user->account_balance = $user->account_balance - $amount;
        $user->save();

        return $user->account_balance;
    }

    public function addUserBalance($user, $amount)
    {
        $user = $this->getUserForBalance($user);
        $user->account_balance += $amount;
        $user->save();
    }

    protected function getUserForBalance($user)
    {
        if (!$user instanceof User) {
            $user = User::where('id', $user)
                ->orWhere('steam_id_64', $user)
                ->orWhere('username', $user)
                ->first();
            if (!$user instanceof User) {
                throw new \Exception('There is no qualified user object.');
            }
        }

        return $user;
    }
}