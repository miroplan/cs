<?php


namespace App\Services;

use App\ItemCase;
use App\Skin;
use App\OPSkinsModels\OPSkin;
use App\OPSkinsModels\OPSkinUserSkin;
use App\UserSkin;
use Illuminate\Support\Facades\Auth;

class CaseHelperService
{


    public function addCase($request, $case)
    {


        $items = array();

        foreach ($request as $item) {
            if(isset($item['id'])) {
                $skin = OPSkin::where('id', $item['id'])->first();
                $user_skin = OPSkinUserSkin::where('skin_id', $item['id'])
                    ->where('user_id', null)->first();

                $case_item = new ItemCase();
                $case_item->casecart_id = $case->id;
                $case_item->skin_id = $skin->id;
                $case_item->user_skin_id = !empty($user_skin) ? $user_skin->id : null;
                $case_item->price = $skin->price;
                $case_item->odds = $item['odd'];
                $case_item->save();
                array_push($items, $case_item);
            }
        }

        return $items;
    }

    public function editItem($request, ItemCase $itemCase)
    {

        $itemCase->price = $request->price;
        $itemCase->save();

        return $itemCase;
    }


}