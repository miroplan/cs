<?php

namespace App\Services;

use App\Policies\ConstantInterfaces\CacheKeysConstantInterface;
use App\Services\SteamLogin;
use LRedis as Redis;

class SteamBotAuthenticatorContainerService implements CacheKeysConstantInterface
{
    protected $container;
    protected $index;
    protected $botConfigs = [];

    /**
     * @param array $botConfig
     */
    public function addBot(array $botConfig)
    {

        $this->container[] = new SteamLogin($botConfig);

        $this->botConfigs[] = $botConfig;

        ++$this->index;

        return $this;
    }

    /**
     * @param int $index
     * @return bool
     */
    public function getBotAtIndex($index = 0)
    {
        if (empty($this->container[$index])) {
            return false;
        }

        return $this->container[$index];
    }

    /**
     * @param int $index
     * @return $this
     */
    public function removeBotAtIndex($index = 0)
    {
        if (!empty($this->container[$index])) {
            unset($this->container[$index]);
            --$this->index;
        }

        return $this;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getOneReady() {
        $this->checkBotsAvailability(true);
        // @todo index would not be static.
        $index = 0;
        $this->container[$index]->config = $this->botConfigs[$index];

        if (!$this->container[$index]->isLoggedIn()) {
            try {
                $this->loginBot($this->container[$index], $index);

            } catch (\Exception $e) {
                Redis::hdel(static::KEY_ROULETTE_BOTS_CREDENTIALS, $index);
                throw new \Exception($e->getMessage());
            }

            if (!$this->container[$index]->isLoggedIn()) {
                throw new \Exception(sprintf('There is a configuration error exists for the bot at index %d', $index));
            }
        }
		dd($this->container[$index]);
        return $this->container[$index];
    }

    /**
     * @return \Generator
     * @throws \Exception
     */
    public function getAllReady() {
        $this->checkBotsAvailability();

        foreach ($this->container as $index => $bot) {
            $bot->config = $this->botConfigs[$index];
            if (!$bot->isLoggedIn()) {
                $this->loginBot($bot, $index);
                if ($bot->isLoggedIn()) {
                    yield $bot;
                }
            }
        }
    }

    protected function loginBot(SteamLogin $bot, $index)
    {
        $botCredentials = Redis::hget(static::KEY_ROULETTE_BOTS_CREDENTIALS, $index);

        $bot->login($botCredentials['authCode'], $botCredentials['twoFactorCode']);
        return $bot;
    }

    /**
     * @param bool $onlyOne
     * @throws \Exception
     */
    protected function checkBotsAvailability($onlyOne = false) {
        if (count($this->container) == 0) {
            if (count($this->botConfigs) == 0) {
                throw new \Exception('There is no bot configured in system no available bot!');
            }

            if ($onlyOne) {
                $this->addBot($this->botConfigs[0]);
                return;
            }

            foreach ($this->botConfigs as $config) {
                $this->addBot($config);
            }

        }
    }
}