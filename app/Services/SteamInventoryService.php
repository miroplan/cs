<?php

namespace App\Services;

use App\Traits\BotConfigsTrait;
use Auth;
use App\Skin;
use App\User;
use App\UserSkin;
use App\Model;
use App\SteamModels\RGDescriptionModel;
use App\SteamModels\RGInventoryModel;


/**
 * Class SteamInventoryService
 * @package App\Services
 */
class SteamInventoryService
{
    use BotConfigsTrait;
    /**
     * @var SteamAPIConnectorService
     */
    protected $steamAPI;

    /**
     * @var SteamTransactionService
     */
    protected $transactor;

    /**
     * SteamInventoryService constructor.
     * @param SteamAPIConnectorService $steamAPI
     */
    public function __construct(SteamAPIConnectorService $steamAPI, SteamTransactionService $transactor)
    {
        $this->steamAPI = $steamAPI;
        $this->transactor = $transactor;
        $this->bots = $this->getBotConfigs();
    }

    /**
     * @param array $rgDescriptions
     * @return \Generator
     */
    public static function convertRGDescriptionsFromSteam(array $rgDescriptions)
    {
        foreach ($rgDescriptions as $key => $description) {
            yield (new RGDescriptionModel($description))
                ->getEloquentModel()
                ->toArray();
        }

    }

    /**
     * @param User $user
     * @param null $uri
     * @return bool
     */
    public function getPlayerPermissions(User $user, $uri = null)
    {
        $permissions = $this->steamAPI->getPlayerPermissions($user->steamid, $uri);
        return $permissions;
    }

    /**
     * @return mixed
     */
    public function getBotsInventory($uri = null)
    {

        foreach ($this->bots as $bot) {
            $inventory = $this->steamAPI->getPlayerInventory($bot['STEAM_64_ID'], $uri);

//			$rgDescriptions = static::convertRGDescriptionsFromSteam($inventory['rgDescriptions']);

            $descriptions = $inventory['descriptions'];


            foreach ($descriptions as $description) {
                $description_model = Skin::where('class_id', $description['classid'])->first();
                if (is_null($description_model)) {
                    $price_request = $this->steamAPI->officialMarketSearch($description['market_hash_name'], 730, 1);
                    $arr = array();
                    foreach ($description['descriptions'] as $desc) {
                        $string = implode(',', $desc);
                        array_push($arr, $string);
                    }
                    $string = implode(' | ', $arr);

                    Skin::create(['app_id' => 730,
                        'class_id' => $description['classid'],
                        'instance_id' => $description['instanceid'],
                        'icon_url' => $description['icon_url'],
                        'name' => $description['name'],
                        'market_hash_name' => $description['market_hash_name'],
                        'market_name' => $description['market_name'],
                        'name_color' => $description['name_color'],
                        'background_color' => $description['background_color'],
                        'type' => $description['type'],
                        'tradable' => $description['tradable'],
                        'marketable' => $description['marketable'],
                        'commodity' => $description['commodity'],
                        'market_tradable_restriction' => $description['market_tradable_restriction'],
                        'descriptions' => $string,
                        'price' => floatval(str_replace("$", "", $price_request['median_price']))
                    ]);
                }
            }

            $rgInventory = static::convertRGInventoryFromSteam($inventory['assets'], $bot['STEAM_64_ID']);


            foreach ($rgInventory as $inv) {

                $skin = UserSkin::where('user_id', null)->where('class_id', $inv['steam_skin_id'])->first();

                if (is_null($skin)) {
                    $skin = new UserSkin();
                    $skin->skin_id = $inv['skin_id'];
                    $skin->class_id = $inv['steam_skin_id'];
                    $skin->steam_skin_id = $inv['steam_skin_id'];
                    $skin->amount = $inv['amount'];
                    $skin->user_id = null;
                    $skin->save();
                }

            }

//			// Pay attention it is recursive!
//			if (!empty($inventory['more'])) {
//				return $this->getBotsInventory($inventory['more_start']);
//			}

        }

        $bots_inventory = UserSkin::where('user_id', null)->get();
        $bot_skins = array();
        foreach ($bots_inventory as $key => $item) {
            $skin = Skin::find($item['skin_id']);
            $skin->amount = $item['amount'];
            array_push($bot_skins, $skin);

        }
        return $bot_skins;
    }

    /**
     * @param array $rgInventory
     * @param $userId
     * @return \Generator
     */
    public static function convertRGInventoryFromSteam(array $rgInventory, $userId)
    {

        foreach ($rgInventory as $key => $skin) {
            yield (new RGInventoryModel($skin, $userId))
                ->getEloquentModel()
                ->toArray();
        }
    }

    /**
     * @return mixed
     */
    public function getBotsInventoryByName($uri = null, $name)
    {


        foreach ($this->bots as $bot) {
            $inventory = $this->steamAPI->getPlayerInventory($bot['STEAM_64_ID'], $uri);

//			$rgDescriptions = static::convertRGDescriptionsFromSteam($inventory['rgDescriptions']);

            $descriptions = $inventory['descriptions'];


            foreach ($descriptions as $description) {
                $description_model = Skin::where('class_id', $description['classid'])->first();
                if (is_null($description_model)) {
                    $price_request = $this->steamAPI->officialMarketSearch($description['market_hash_name'], 730, 1);
                    $arr = array();
                    foreach ($description['descriptions'] as $desc) {
                        $string = implode(',', $desc);
                        array_push($arr, $string);
                    }
                    $string = implode(' | ', $arr);

                    Skin::create(['app_id' => 730,
                        'class_id' => $description['classid'],
                        'instance_id' => $description['instanceid'],
                        'icon_url' => $description['icon_url'],
                        'name' => $description['name'],
                        'market_hash_name' => $description['market_hash_name'],
                        'market_name' => $description['market_name'],
                        'name_color' => $description['name_color'],
                        'background_color' => $description['background_color'],
                        'type' => $description['type'],
                        'tradable' => $description['tradable'],
                        'marketable' => $description['marketable'],
                        'commodity' => $description['commodity'],
                        'market_tradable_restriction' => $description['market_tradable_restriction'],
                        'descriptions' => $string,
                        'price' => floatval(str_replace("$", "", $price_request['median_price']))
                    ]);
                }
            }

            $rgInventory = static::convertRGInventoryFromSteam($inventory['assets'], $bot['STEAM_64_ID']);


            foreach ($rgInventory as $inv) {

                $skin = UserSkin::where('user_id', null)->where('class_id', $inv['steam_skin_id'])->first();

                if (is_null($skin)) {
                    $skin = new UserSkin();
                    $skin->skin_id = $inv['skin_id'];
                    $skin->class_id = $inv['steam_skin_id'];
                    $skin->steam_skin_id = $inv['steam_skin_id'];
                    $skin->amount = $inv['amount'];
                    $skin->user_id = null;
                    $skin->save();
                }

            }

//			// Pay attention it is recursive!
//			if (!empty($inventory['more'])) {
//				return $this->getBotsInventory($inventory['more_start']);
//			}

        }


        $skin = Skin::where('market_hash_name', 'LIKE', '%' . $name . '%')->get();

        return $skin;
    }

    /**
     * @param $playerId
     * @param array $skinIds
     */
    public function depositSkins(User $user, array $skinIds)
    {

    }

    /**
     * @param $playerId
     */
    public function withdrawSkins($playerId)
    {

    }

    /**
     * @param $playerId
     * @param array $skinIds
     * @return array
     */
    public function checkDepositableSkins(User $user, array $skinIds)
    {
        $inventory = $this->getPlayerInventory($user);

        $nonDepositables = array_diff($skinIds, $inventory->pluck('skin_id'));

        return $inventory->reject(function ($skin) use ($nonDepositables) {
            return in_array($skin->skin_id, $nonDepositables);

        });
    }

    /**
     * @param $playerId
     * @return mixed
     */
    public function getPlayerInventory(User $user, $uri = null)
    {

        $inventory = $this->steamAPI->getPlayerInventory($user->steamid, $uri);

        if (!$inventory) {
            return false;
        }

//        $rgDescriptions = static::convertRGDescriptionsFromSteam($inventory['descriptions']);
        $descriptions = $inventory['descriptions'];

        foreach ($descriptions as $description) {
            $description_model = Skin::where('class_id', $description['classid'])->first();

            if (is_null($description_model)) {

                $price_request = $this->steamAPI->officialMarketSearch($description['market_hash_name'], 730, 1);
                $arr = array();
                foreach ($description['descriptions'] as $desc) {
                    $string = implode(',', $desc);
                    array_push($arr, $string);
                }
                $string = implode(' | ', $arr);

                Skin::create(['app_id' => 730,
                    'class_id' => $description['classid'],
                    'instance_id' => $description['instanceid'],
                    'icon_url' => $description['icon_url'],
                    'name' => $description['name'],
                    'market_hash_name' => $description['market_hash_name'],
                    'market_name' => $description['market_name'],
                    'name_color' => $description['name_color'],
                    'background_color' => $description['background_color'],
                    'type' => $description['type'],
                    'tradable' => $description['tradable'],
                    'marketable' => $description['marketable'],
                    'commodity' => $description['commodity'],
                    'market_tradable_restriction' => $description['market_tradable_restriction'],
                    'descriptions' => $string,
                    'price' => floatval(str_replace("$", "", $price_request['median_price']))
                ]);
            }
        }
        $rgInventory = static::convertRGInventoryFromSteam($inventory['assets'], $user->id);

        foreach ($rgInventory as $inv) {

            $skin = UserSkin::where('user_id', $user->id)->where('class_id', $inv['classid'])->first();
            if (is_null($skin)) {
                $skin = new UserSkin();
                $skin->skin_id = $inv['skin_id'];
                $skin->class_id = $inv['steam_skin_id'];
                $skin->steam_skin_id = $inv['steam_skin_id'];
                $skin->amount = $inv['amount'];
                $skin->user_id = $user->id;
                $skin->save();
            }

        }

//        // Pay attention it is recursive!
//        if (!empty($inventory['more'])) {
//            return $this->getPlayerInventory($user, $inventory['more_start']);
//        }

        return UserSkin::where('user_id', $user->id)->get();

    }

    /**
     * @param array $skinIds
     * @return array
     */
    protected function getSkins(array $skinIds)
    {
        return [];
    }
}