<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response as GZResponse;
use App\Providers\GuzzleProvider as GZ;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Response;


/**
 * Class OPSkinsAPIConnectorService
 * @package App\Services
 */
class OPSkinsAPIConnectorService
{

    const ENDPOINT_ITEMS_SEARCH = "https://api.opskins.com/ISales/Search/v1?key=%s&app=%s&page=%d&search_item=%s";
    const ENDPOINT_ITEMS_PRICES = "https://api.opskins.com/IPricing/GetAllLowestListPrices/v1/?key=%s&appid=%s";

    const APP_ID = "730_2"; // CSGO
    const PRICES_APP_ID = 730; // CSGO

    /**
     * @var
     */
    protected $opskinsAPIKey;

    /**
     * OPSkinsAPIConnectorService constructor.
     * @param Client $httpClient
     * @param $opskinsAPIKey
     * @internal param $steamAPIKey
     */
    public function __construct(Client $httpClient, $opskinsAPIKey)
    {
        $this->httpClient = $httpClient;
        $this->opskinsAPIKey = $opskinsAPIKey;
    }

    /**
 * further information about user.
 * @param null $page
 * @param null $search_item
 * @param string $appId
 * @return mixed
 * @internal param $playerId
 */
    public function getItems($page = null,$search_item = null, $appId = self::APP_ID)
    {
        // change to guzzle
        $url = sprintf(self::ENDPOINT_ITEMS_SEARCH, $this->opskinsAPIKey, $appId, $page, $search_item);
        $response = $this->httpClient->request(GZ::GET, $url);

        return $this->decodeResponse($response);
    }

    /**
     * further information about user.
     * @param null $page
     * @param null $search_item
     * @param string $appId
     * @return mixed
     * @internal param $playerId
     */
    public function getItemsPrices($appId = self::PRICES_APP_ID)
    {
        // change to guzzle
        $url = sprintf(self::ENDPOINT_ITEMS_PRICES, $this->opskinsAPIKey, $appId);
        $response = $this->httpClient->request(GZ::GET, $url);

        return $this->decodeResponse($response);
    }

    /**
     * way to decode PSR-7 json response to use response in system.
     * @param Response $response
     * @return mixed
     */
    protected function decodeResponse(GZResponse $response = null)
    {
        if (is_null($response)) {
            return null;
        }
        return json_decode($response->getBody()->getContents(), true);
    }


}