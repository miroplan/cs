<?php

namespace App\Services;

use App\GameRoom;
use App\User;

/**
 * Class SteamTradeService
 * @package App\Services
 */
class SteamTradeService
{
    const NEW_TRADE_OFFER_MESSAGE = <<<TRADEOFFERMESSAGE
Hi %s,
Please accept your recent %s items to continue your %s process.

Best Regards,
God of Skins Team.
TRADEOFFERMESSAGE;

    const CANCEL = 'cancel';
    const DECLINE = 'decline';
    const GET = 'get';

    const URL_TRADE_OFFER = "https://steamcommunity.com/tradeoffer/new/?partner=%d&token=%s";

    /**
     * @var SteamAPIConnectorService
     */
    protected $steamAPI;

    /**
     * @var SteamInventoryService
     */
    protected $inventory;

    /**
     * SteamTradeService constructor.
     * @param $steamAPI
     * @param $inventory
     * @param $botContainer
     */
    public function __construct(SteamAPIConnectorService $steamAPI, SteamBotAuthenticatorContainerService $botContainer, SteamInventoryService $inventory)
    {
        $this->steamAPI = $steamAPI;
        $this->inventory = $inventory;
        $this->botContainer = $botContainer;
    }

    /**
     * @param $tradeUrl
     * @return mixed
     */
    public function getTradeTokenFromUrl($tradeUrl)
    {
        $urlParts = parse_url($tradeUrl);
        parse_str($urlParts['query'], $query);

        return $query['token'];
    }

    /**
     * @param $tradeUrl
     * @return bool|mixed
     */
    public function checkTradeToken($tradeUrl)
    {
        $token = $this->getTradeTokenFromUrl($tradeUrl);

        if (strlen($token) > 0) {
            return $token;
        }

        return false;
    }

    /**
     * @Cache implement caching if needed.
     */
    public function getMostAvailableBot($gameRoomId = 0)
    {

	    $bot = $this->botContainer->getBotAtIndex(0);
        $availableBot = [
            'steam32Id'   => $bot->config['STEAM_32_ID'],
            'steam64Id'   => $bot->config['STEAM_64_ID'],
            'tradeOfferUrl' => sprintf(self::URL_TRADE_OFFER, $bot->config['STEAM_32_ID'], $bot->config['TRADE_TOKEN']),
        ];

        return $availableBot;
    }

    public function attachBotToGameRoom(GameRoom $gameRoom) {
        $bot = $this->botContainer->getOneReady();

        $gameRoom->steam_64_id = $bot->config['steam_64_id'];
        $gameRoom->save();

        return $bot;
    }


    /**
     *
     */
    public function getTradeOffers(User $user)
    {

    }

    /**
     *
     */
    public function getCompletedTradeItems()
    {

    }

    /**
     * @param User $user
     * @param string $type values : bet or deposit
     */
    public function newTradeOffer(User $user = null, $sessionID, $type)
    {

        $bot = $this->botContainer->getOneReady();

        if ($user == null || $bot->config['steam_id_64'] == $user->steam_id_64) {
            return false;
        }

        $botInventory = $this->steamAPI->getBotInventory($bot->config['steam_id_64']);

        $itemsFromMe = $botInventory;
        $itemsFromThem = [];

        $tradeOffer = [
            'newversion' => true,
            'version' => 2,
            'me' => ['assets' => $itemsFromMe, 'currency' => [], 'ready' => false],
            'them' => ['assets' => $itemsFromThem, 'currency' => [], 'ready' => false],
        ];

        $formFields = [
            'serverid' => 1,
            'sessionid' => $sessionID,
            'partner' => $user->steam_id_64,
            'tradeoffermessage' => sprintf(self::NEW_TRADE_OFFER_MESSAGE, $user->username, $type, $type . 'ing'),
            'json_tradeoffer' => json_encode($tradeOffer),
        ];

        $options = [
            'headers' => ['Cookie' => $bot->getCookie()],
            'form_params' => $formFields,
        ];

        $tradeOfferResult = $this->steamAPI->newTradeOfferByTradeUrl(
            $user->steam_id_64,
            $user->trade_url,
            $options
        );

        return $tradeOfferResult;
    }

    /**
     * @param $tradeOfferId
     * @param $sessionID
     * @return mixed
     * @throws \Exception
     */
    public function acceptTradeOffer($tradeOfferId, $sessionID)
    {

        $bot = $this->botContainer->getOneReady();

        $formFields = [
            'serverid' => 1,
            'sessionid' => $sessionID,
            'tradeofferid' => $tradeOfferId
        ];

        $options = [
            'headers' => ['Cookie' => $bot->getCookie()],
            'form_params' => $formFields,
        ];

        $tradeOfferResult = $this->steamAPI->acceptTradeOffer($tradeOfferId, $options);
        return $tradeOfferResult;
    }

    /**
     * @param $tradeOfferId
     * @param $action
     * @return mixed
     * @throws \Exception
     */
    protected function tradeOffer($tradeOfferId, $action)
    {
        if (!in_array([self::CANCEL, self::DECLINE, self::GET], $action)) {
            throw new \Exception('Unexpected action for trade offer');
        }

        $bot = $this->botContainer->getBotAtIndex();

        if (!$bot) {
            throw new \Exception('Bot could not accessible at the time.');
        }

        $options = [
            'params' => ['tradeofferid' => $tradeOfferId],
        ];

        // declineTradeOffer, cancelTradeOffer, getTradeOffer.
        $tradeOfferResult = $this->steamAPI->{$action . 'TradeOffer'}($options, $bot->config['STEAM_API_KEY']);

        return $tradeOfferResult;
    }
}