<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response as GZResponse;
use App\Providers\GuzzleProvider as GZ;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Response;


/**
 * Class SteamAPIConnectorService
 * @package App\Services
 */
class SteamAPIConnectorService
{

    const ENDPOINT_PLAYER_PERMISSIONS = "http://steamcommunity.com/profiles/%d/inventory/json/%d/%d?l=%s&count=%d";
    const ENDPOINT_PLAYER_SUMMARIES = "https://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=%s&steamids=%s";
    const ENDPOINT_PROFILE_INVENTORY = "http://steamcommunity.com/inventory/%d/%d/%d?l=%d&count=%d";
    const ENDPOINT_NEW_TRADE_OFFER = "https://steamcommunity.com/tradeoffer/new/?partner=%d&token=%s";
    const ENDPOINT_ACCEPT_TRADE_OFFER = "https://steamcommunity.com/tradeoffer/%d/accept";
//    const ENDPOINT_MARKET_ITEM_SEARCH = "https://steamcommunity.com/market/listings/%d/%s/render/?query=&start=0&count=10&country=US&language=english&currency=%s";
    const ENDPOINT_MARKET_ITEM_SEARCH = "https://steamcommunity.com/market/priceoverview/?appid=%d&market_hash_name=%s&currency=%s";
    const ENDPOINT_MARKET_COMMODITY_ITEM_SEARCH = "https://steamcommunity.com/market/itemordershistogram?country=US&language=english&currency=%s&item_nameid=%d";

    const ENDPOINT_NEW_TRADE_OFFER_REFERER = "https://steamcommunity.com/tradeoffer/new/%s";
    const ENDPOINT_ACCEPT_TRADE_OFFER_REFERER = "https://steamcommunity.com/tradeoffer/%d/";
    const ENDPOINT_IECONSERVICE = "https://api.steampowered.com/IEconService/%s/?key=%s";

    const METHOD_DECLINE_TRADE_OFFER = "DeclineTradeOffer/v1";
    const METHOD_CANCEL_TRADE_OFFER = "CancelTradeOffer/v1";
    const METHOD_GET_TRADE_OFFER = "GetTradeOffer/v1";

    const APP_ID = 730; // CSGO
    const CONTEXT_ID = 2;
    const CURRENCY = 1;

    /**
     * @var
     */
    protected $steamAPIKey;

    /**
     * SteamAPIConnectorService constructor.
     * @param $steamAPIKey
     */
    public function __construct(Client $httpClient, $steamAPIKey)
    {
        $this->httpClient = $httpClient;
        $this->steamAPIKey = $steamAPIKey;
    }

    /**
     * further information about user.
     * @param $playerId
     * @return mixed
     */
    public function getPlayerSummary($playerId)
    {
        // change to guzzle
        $playerSummariesUrl = sprintf(self::ENDPOINT_PLAYER_SUMMARIES, $this->steamAPIKey, $playerId);
        $response = $this->httpClient->request(GZ::GET, $playerSummariesUrl);

        return $this->decodeResponse($response);
    }

    /**
     * way to decode PSR-7 json response to use response in system.
     * @param GZResponse|Response $response
     * @return mixed
     */
    protected function decodeResponse(GZResponse $response = null)
    {
        if (is_null($response)) {
            return null;
        }
        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @param $playerId
     * @param null $url
     * @param int $appId
     * @param int $contextId
     * @param string $lang
     * @param int $count
     * @return mixed
     */
    public function getPlayerPermissions($playerId, $url = null, $appId = self::APP_ID, $contextId = self::CONTEXT_ID, $lang = 'english', $count = 1)
    {
        if (is_null($url)) {
            $url = sprintf(self::ENDPOINT_PLAYER_PERMISSIONS, $playerId, $appId, $contextId, $lang, $count);
        }

        $options = ['headers' => [
            'User-Agent' => 'Mozilla/5.0 (Linux; U; Android 4.1.1; en-us; Google Nexus 4 - 4.1.1 - API 16 - 768x1280 Build/JRO03S) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30',
            'Accept' => 'text/json'
        ]];
        try {
            $response = $this->httpClient->request(GZ::GET, $url, $options);
        } catch (\Exception $e) {
            print_r($e->getMessage());
        }

        return $this->decodeResponse($response);
    }

    /**
     * CSGONINJA account bot inventory.
     * @return mixed
     */
    public function getBotInventory($steam64Id)
    {
        return $this->getPlayerInventory($steam64Id);
    }

    /**
     * getting inventory of the user. To show which skins does he want to deposit.
     * @param $playerId
     * @return mixed
     */
    public function getPlayerInventory($playerId, $url = null, $appId = self::APP_ID, $contextId = self::CONTEXT_ID, $lang = 'english', $count = 5000)
    {
        if (is_null($url)) {
            $url = sprintf(self::ENDPOINT_PROFILE_INVENTORY, $playerId, $appId, $contextId, $lang, $count);
        }
        $options = ['headers' => [
            'User-Agent' => 'Mozilla/5.0 (Linux; U; Android 4.1.1; en-us; Google Nexus 4 - 4.1.1 - API 16 - 768x1280 Build/JRO03S) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30',
            'Accept' => 'text/javascript, text/html, application/xml, text/xml, */*'
        ]];
        try {
            $response = $this->httpClient->request(GZ::GET, $url, $options);
        } catch (\Exception $e) {
            //if ($e->getCode() == "403") {
                return false;
           // }
        }

        return $this->decodeResponse($response);
    }

    /**
     * @param $skinMarketHashName
     * @param int $appId
     * @param int $currency
     * @return mixed
     */
    public function officialMarketSearch($skinMarketHashName, $appId = self::APP_ID, $currency = self::CURRENCY)
    {
        $marketSearchUrl = sprintf(self::ENDPOINT_MARKET_ITEM_SEARCH, $appId, urlencode($skinMarketHashName), $currency);

        try {
            $response = $this->httpClient->request(GZ::GET, $marketSearchUrl);

        } catch (\Exception $e) {
            $response = null;

        }

        return $this->decodeResponse($response);
    }

    /**
     * @param $skinMarketHashName
     * @param int $appId
     * @param int $currency
     */
    public function nonOfficialMarketSearch($skinMarketHashName, $appId = self::APP_ID, $currency = self::CURRENCY)
    {
        $response = null;

        return $this->decodeResponse($response);
    }

    /**
     * @param $playerId integer
     * @param $options array
     * @return mixed
     */
    public function newTradeOfferByToken($playerId, $token, $options)
    {

        $options['referer'] = sprintf(self::ENDPOINT_NEW_TRADE_OFFER_REFERER, '?' . http_build_query(['partner' => $playerId]));

        return $this->decodeSteamTradeResponse(function () use ($playerId, $token, $options) {
            return $this->httpClient->request(
                GZ::POST,
                sprintf(self::ENDPOINT_NEW_TRADE_OFFER, $playerId, $token),
                $options

            );
        });

    }

    /**
     * @param \Closure $tradeRequest
     * @return array|mixed
     */
    protected function decodeSteamTradeResponse(\Closure $tradeRequest)
    {
        $response = null;

        try {
            $response = $tradeRequest();

            if ($response->getStatusCode() !== Response::HTTP_OK) {
                return ['body' => $response->getReasonPhrase(), 'status' => $response->getStatusCode()];
            }
        } catch (ClientException $e) {
            return ['body' => $e->getMessage(), 'status' => $e->getCode()];
        }

        return $this->decodeResponse($response);
    }

    /**
     * @param $playerId integer
     * @param $options array
     * @return mixed
     */
    public function newTradeOfferByTradeUrl($playerId, $tradeUrl, $options)
    {

        $options['referer'] = sprintf(self::ENDPOINT_NEW_TRADE_OFFER_REFERER, '?' . http_build_query(['partner' => $playerId]));

        return $this->decodeSteamTradeResponse(function () use ($tradeUrl, $options) {
            return $this->httpClient->request(GZ::POST, $tradeUrl, $options);
        });

    }

    /**
     * @param $tradeOfferId integer
     * @param $options array
     * @return mixed
     */
    public function acceptTradeOffer($tradeOfferId, $options)
    {
        $options['referer'] = sprintf(self::ENDPOINT_ACCEPT_TRADE_OFFER_REFERER, $tradeOfferId);

        return $this->decodeSteamTradeResponse(function () use ($tradeOfferId, $options) {
            return $this->httpClient->request(
                GZ::POST,
                sprintf(self::ENDPOINT_ACCEPT_TRADE_OFFER, $tradeOfferId),
                $options

            );
        });

    }

    public function declineTradeOffer($options, $apiKey)
    {
        $url = sprintf(self::ENDPOINT_IECONSERVICE, self::METHOD_DECLINE_TRADE_OFFER, $apiKey);
        return $this->decodeSteamTradeResponse(function () use ($options, $url) {
            return $this->httpClient->request(
                GZ::POST,
                $url,
                $options['params']

            );
        });
    }

    public function cancelTradeOffer($options, $apiKey)
    {
        $url = sprintf(self::ENDPOINT_IECONSERVICE, self::METHOD_CANCEL_TRADE_OFFER, $apiKey);
        return $this->decodeSteamTradeResponse(function () use ($options, $url) {
            return $this->httpClient->request(
                GZ::POST,
                $url,
                $options['params']

            );
        });
    }
}