<?php

namespace App\Services;

use App\ItemPrice;
use App\Traits\BotConfigsTrait;
use Auth;
use App\Skin;
use App\User;
use App\UserSkin;
use App\Model;
use App\SteamModels\RGDescriptionModel;
use App\SteamModels\RGInventoryModel;


/**
 * Class OPSkinsInventoryService
 * @package App\Services
 */
class OPSkinsInventoryService
{
    use BotConfigsTrait;
    /**
     * @var OPSkinsInventoryService
     */
    protected $opskinsAPI;


    /**
     * OPSkinsInventoryService constructor.
     * @param OPSkinsAPIConnectorService $opskinsAPI
     * @internal param SteamTransactionService $transactor
     * @internal param SteamAPIConnectorService $steamAPI
     */
    public function __construct(OPSkinsAPIConnectorService $opskinsAPI)
    {
        $this->opskinsAPI = $opskinsAPI;
        $this->bots = $this->getBotConfigs();
    }

    /**
     * @param array $rgDescriptions
     * @return \Generator
     */
    public static function convertRGDescriptionsFromSteam(array $rgDescriptions)
    {
        foreach ($rgDescriptions as $key => $description) {
            yield (new RGDescriptionModel($description))
                ->getEloquentModel()
                ->toArray();
        }

    }

    /**
     * @param null $page
     * @return mixed
     */
    public function getItems($page = null)
    {

        $items = $this->opskinsAPI->getItems($page);

        if ($items['status'] && !empty($items['response']['sales'])) {
            $items = $items['response']['sales'];

            foreach ($items as $id => $item) {

                if (strripos($item['market_name'], "Sealed Graffiti")) {
                    unset($items[$id]);
                    continue;
                }

                $item_price = ItemPrice::where('name', str_replace('/', '-', $item['market_name']))->first();
                $items[$id]['price'] = !empty($item_price->price) ? floatval($item_price->price / 100) : 0.00;

            }

            return $items;
        }

        return false;

    }

}