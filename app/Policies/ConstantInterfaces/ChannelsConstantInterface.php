<?php

namespace App\Policies\ConstantInterfaces;

interface ChannelsConstantInterface
{
    const CHANNEL_ROULETTE_WINNER = 'roulette.winner#%d';
    const CHANNEL_ROULETTE_ROOM = 'roulette.room#%d';
    const CHANNEL_ROULETTE_PLAY = 'roulette.play#%d';
    const CHANNEL_COUNTDOWN_ROOM = 'roulette.countdown#%d';
    const CHANNEL_NEW_BET = 'roulette.bet#%d';
}