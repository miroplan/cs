<?php

namespace App\Policies\ConstantInterfaces;

interface CacheKeysConstantInterface {
    const KEY_ROULETTE_ACTIVE_GAMES = 'roulette.activeGames';
    const KEY_ROULETTE_BOTS_CREDENTIALS = 'roulette.botCredentials';
    const KEY_ROULETTE_WINNERS_TO_BE_BROADCASTED = 'roulette.broadcastingWinners';
}