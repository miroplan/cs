<?php

namespace App\Policies\ConstantInterfaces;

interface CommandConstantInterface {
    const NINJA_FETCH_MARKET = 'ninja:fetch-market';
    const NINJA_ACCEPT_OFFERS = 'ninja:accept-offers';
    const NINJA_MANAGE_ROULETTE = 'ninja:manage-roulette';
    const NINJA_ADD_BALANCE = 'ninja:add-balance {user} {balance}';
    const NINJA_BROADCAST_WINNERS = 'ninja:broadcast-winners';
}