<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ItemPrice;
use App\Services\OPSkinsAPIConnectorService;

class UpdateItemPrices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UpdateItemPrice:updateprice';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Items Price from OPSkins';

    protected $opskinsAPI;


    /**
     * Create a new command instance.
     *
     * @param OPSkinsAPIConnectorService $opskinsAPI
     */
    public function __construct(OPSkinsAPIConnectorService $opskinsAPI)
    {
        $this->opskinsAPI = $opskinsAPI;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $prices = $this->opskinsAPI->getItemsPrices();

        if (!empty($prices['response'])) {
            ItemPrice::truncate();

            foreach ($prices['response'] as $name => $inf) {
                $model = new ItemPrice();
                $model->name = $name;
                $model->price = $inf['price'];
                $model->quantity = $inf['quantity'];

                $model->save();
            }
        }
    }
}
