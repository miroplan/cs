<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\OPSkinsModels\OPSkin;

class DeleteSealedGraffiti extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DeleteSealedGraffiti:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Delete Sealed Graffiti Skins from OPSkins";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        OPSkin::where('market_name', 'LIKE', '%Sealed Graffiti%')->delete();
    }
}
