<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\OPSkinsModels\OPSkin;
use App\OPSkinsModels\OPSkinUserSkin;
use App\Services\OPSkinsInventoryService;

class UpdateSkins extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UpdateSkins:update {page_start?} {page_end?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update skins from OpSkins API';

    protected $opskinsAPI;
    protected $skins_count = 0;
    protected $user_skins_count = 0;
    protected $time_start;
    protected $time_end;
    protected $cycle = 0;

    /**
     * Create a new command instance.
     *
     * @param OPSkinsAPIConnectorService $opskinsAPI
     */
    public function __construct(OPSkinsInventoryService $opskinsAPI)
    {
        $this->opskinsAPI = $opskinsAPI;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $page_start = $this->argument('page_start') ? $this->argument('page_start') : 0;
        $page_end = $this->argument('page_end') ? $this->argument('page_end') : 500;
        $this->time_start = microtime(true);

        $this->addSkins($page_start, $page_end);
    }

    /**
     * @param int $page_start
     * @param int $page_end
     * @return string
     * @internal param $page
     */
    private function addSkins($page_start = 0, $page_end = 500)
    {
        $page = $page_start;

        $this->checkTimer();

        if ($page_start == $page_end + 1) {
            echo "Finish on page " . $page . ". Total added " . $this->skins_count . " new skins and " . $this->user_skins_count . " new user skins\n";
            return;
        }

        $skins = $this->getSkins($page);

        if ($skins) {

            echo "Page: " . $page . " Items count: " . count($skins) . "\n";

            foreach ($skins as $item) {

                $model = OPSkin::where('class_id', $item['classid'])->first();

                if (!$model) {
                    $model = new OPSkin();
                    $this->skins_count++;
                }

                $model->app_id = 730;
                $model->class_id = $item['classid'];
                $model->instance_id = $item['instanceid'];
                $model->img = $item['img'];
                $model->market_name = $item['market_name'];
                $model->inspect = $item['inspect'];
                $model->type = $item['type'];
                $model->context_id = $item['contextid'];
                $model->price = $item['price'];

                if (!$model->save()) {
                    var_dump($model->errors());
                }

                $this->saveUserSkin($item, $model);


            }

            $this->addSkins(++$page, $page_end);

        } else {
            echo "Finish! Total added " . $this->skins_count . " new skins and " . $this->user_skins_count . " new user skins\n";
            return;
        }

    }

    /**
     * Check operation limit 20 requests per minute
     */
    private function checkTimer()
    {
        if ($this->cycle == 20) {
            $this->time_end = microtime(true);
            $diff = $this->time_end - $this->time_start;

            if ($diff < 60) {
                echo "Wait for " . (60 - $diff) . " seconds...\n";
                sleep(60 - $diff);
            }

            $this->cycle = 0;
            $this->time_start = microtime(true);
        }

        $this->cycle++;
    }

    /**
     * @param $page
     * @return mixed
     */
    private function getSkins($page)
    {
        return $this->opskinsAPI->getItems($page);
    }

    /**
     * @param $item
     * @param $model
     */
    private function saveUserSkin($item, $model)
    {
        $skin = OPSkinUserSkin::where('sale_id', $item['id'])->first();

        if (!$skin) {
            $this->user_skins_count++;
            $skin = new OPSkinUserSkin();
        }

        $skin->skin_id = $model->id;
        $skin->sale_id = $item['id'];
        $skin->opskins_skin_id = $item['item_id'];
        $skin->class_id = $item['classid'];
        $skin->amount = $item['amount'];
        $skin->stickers = $item['stickers'];
        $skin->wear = $item['wear'];
        $skin->bot_id = $item['bot_id'];

        if (!$skin->save()) {
            var_dump($skin->errors());
        }

    }
}
