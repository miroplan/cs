<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\RedisSubscribe',
        'App\Console\Commands\UpdateItemPrices',
        'App\Console\Commands\UpdateSkins',
        'App\Console\Commands\DeleteSealedGraffiti',
    ];


    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $file = '/home/forge/cron_gos.log';

        $schedule->command('UpdateItemPrice:updateprice')
            ->dailyAt('23:00')
            ->sendOutputTo($file);

        $dw = intval(date("w", time())); // Current day of week
        $hour = intval(date("G", time())); //Current hour

        $page_start = ($dw * 2000) + ($hour * 400);
        $page_end = $page_start + 400;

        $schedule->command('UpdateSkins:update ' . $page_start . ' ' . $page_end)
            ->hourly()
            ->between('00:00', '05:00')
            ->withoutOverlapping()
            ->sendOutputTo($file);

    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
