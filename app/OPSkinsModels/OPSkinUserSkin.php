<?php

namespace App\OPSkinsModels;

use Illuminate\Database\Eloquent\Model;


class OPSkinUserSkin extends Model
{
    protected $table = 'opskins_user_skins';
    protected $fillable = ['class_id','skin_id','sale_id','opskins_skin_id', 'amount', 'stickers',
        'wear','bot_id','user_id' ];
    /**
     * The user that belong to the skin.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * The skin that belong to the user.
     */
    public function skin()
    {
        return $this->belongsTo('App\OPSkinsModels\OPSkin');
    }
}
