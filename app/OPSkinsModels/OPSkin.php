<?php

namespace App\OPSkinsModels;

use Illuminate\Database\Eloquent\Model;

class OPSkin extends Model
{
    protected $table = 'opskins_skins';
    protected $fillable = ['skin_id', 'app_id', 'class_id', 'instance_id', 'img',
        'market_name', 'inspect',
        'type', 'context_id', 'price'];

    public function delete()
    {
        $this->user()->delete();
        parent::delete();
    }

    /**
     * The skin that belong to the user.
     */

    public function user()
    {
        return $this->hasMany('App\OPSkinsModels\OPSkinUserSkin', 'skin_id');
    }
}
