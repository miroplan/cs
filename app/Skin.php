<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skin extends Model
{

	protected $table = 'skins';
	protected $fillable = ['app_id','class_id','instance_id', 'icon_url', 'icon_drag_url',
		'name', 'market_hash_name', 'market_name', 'name_color', 'background_color',
		'type', 'tradable', 'marketable', 'commodity',
		'market_tradable_restriction', 'market_marketable_restriction', 'descriptions','price' ];
	/**
	 * The skin that belong to the user.
	 */

	public function user()
	{
		return $this->hasOne('App\Models\UserSkin','class_id');
	}
}
