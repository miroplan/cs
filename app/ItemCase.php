<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemCase extends Model
{

    public function cases(){

	    return  $this->belongsTo('App\CaseCart');

    }

    public function skin(){

	    return  $this->belongsTo('App\OPSkinsModels\OPSkin');
    }
	public function winnings(){

		return $this->hasMany('App\Winner');
	}

}
