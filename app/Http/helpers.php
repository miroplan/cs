<?php
use App\Skin;
use App\UserSkin;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;


	function transform_items($inventoty){
		$bot_inv = array();

		foreach ($inventoty as $inv){
			$bot= Skin::where('class_id','inv');
		}
		return 'transform';
	}

	function validate_trade_url($url){

		if (!filter_var($url, FILTER_VALIDATE_URL) === false) {
			$partner = strpos($url,'partner');

			if($partner >=0){

				$token = strpos($url,'token');

				if($token >=0){

					return true;
				}
			}

		}

		return "$url is not a valid URL";

	}


	function case_images($filename){
		$path = storage_path() . '/app/public/case_images/' . $filename;

		if(!File::exists($path)) abort(404);

		$file = File::get($path);
		$type = File::mimeType($path);

		return response($file)
			->header('Content-Type', $type);
	}
