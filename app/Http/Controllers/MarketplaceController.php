<?php

namespace App\Http\Controllers;

use App\Traits\BotConfigsTrait;
use App\User;
use App\UserProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;


class MarketplaceController extends Controller
{
	use BotConfigsTrait;




    public function index(Request $request)
    {


	    if($this->redis->hexists('bot_inv','bot'.date('y-m-d:H:m'))){

		    $bots_inventory =json_decode($this->redis->hGet('bot_inv','bot'.date('y-m-d:H:m')),true);
	    }
	    else{
		    $bots_inventory = $this->steamInventory->getBotsInventory();
		    $this->redis->hset('bot_inv','bot'.date('y-m-d:H:m'),json_encode($bots_inventory));

        }


        return view('marketplace',compact('bots_inventory'));
    }

	public function getBotTradeOfferUrl()
	{
		$tradeOfferUrl = $this->steamTrade->getMostAvailableBot();
		if (!$tradeOfferUrl) {
			abort('404');
		}

		return $tradeOfferUrl;
	}

    public function new_trade(Request $request){
	    $bot = $this->steamTrade->getMostAvailableBot();
	    $trade = $this->steamTrade;
	    $result= $trade->newTradeOffer(Auth::user(),$bot['sessionId'],'bet');
	    return $result;
    }

    public function cancel_offer(Request $request){

	    $api =$this->steamApi;
	    $result = $api->cancelTradeOffer($request);
	    return $result;

    }

	public function accept_offer(Request $request){
		$bot = $this->steamTrade->getMostAvailableBot();
		$api =$this->steamTrade;
		dd($bot);
		$result = $api->acceptTradeOffer($bot['sessionId']);

		return $result;

	}

	public function updateTradeUrl(Request $request)
	{
		$this->validate($request, [
			'trade_url'    => 'required|url',
		]);
		$tradeUrl = $request->get('trade_url');

		if (!$this->steamTrade->checkTradeToken($tradeUrl)) {
			abort('404');
		}
		$user = Auth::user();
		$user->trade_url = $tradeUrl;
		$user->save();

		return $user;
	}

	public function getAccountBalance()
	{
		$user = Auth::user();
		$profile = UserProfile::where('user_id',$user->id)->get();
		return [$user,$profile];
	}


}
