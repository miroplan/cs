<?php

namespace App\Http\Controllers;

use App\Services\CaseHelperService;
use App\Services\SteamAPIConnectorService;
use App\Services\OPSkinsAPIConnectorService;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Services\SteamBotAuthenticatorContainerService;
use App\Services\SteamLogin;
use App\Services\SteamTradeService;
use App\Services\SteamTransactionService;
use App\Services\SteamInventoryService;
use App\Services\OPSkinsInventoryService;
use App\Traits\BotConfigsTrait;
use Illuminate\Redis\RedisManager;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	protected $steamInventory;
	protected $opskinsInventory;
	protected $steamTrade;
	protected $transactor;


	public function __construct(
        SteamInventoryService $steamInventory,
        OPSkinsInventoryService $opskinsInventory,
        SteamTradeService $steamTrade,
        SteamTransactionService $transactor,
        RedisManager $redis,
        SteamAPIConnectorService $api,
        OPSkinsAPIConnectorService $opskins_api,
        CaseHelperService $caseHelper
	)
	{
		$this->steamInventory = $steamInventory;
		$this->opskinsInventory = $opskinsInventory;
		$this->steamTrade = $steamTrade;
		$this->transactor = $transactor;
		$this->redis = $redis;
		$this->steamApi = $api;
		$this->opskinsApi = $opskins_api;
		$this->caseHelper = $caseHelper;
	}
}
