<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GiftcardsController extends Controller
{
    public function index()
    {
        return view('giftcards');
    }

    public function giftcardDetails()
    {
        return view('giftcardDetails');
    }
}
