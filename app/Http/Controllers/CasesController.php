<?php

namespace App\Http\Controllers;

use App\CaseCart;
use App\ItemCase;
use App\OPSkinsModels\OPSkinUserSkin;
use App\Skin;
use App\UserSkin;
use App\OPSkinsModels\OPSkin;
use App\Winner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\CaseImage;
use App\Events\LatestWinners;
use Auth;

class CasesController extends Controller
{

    public function index($type = null)
    {
        $cases = CaseCart::type($type)->filter()->paginate(12);

        $filters = CaseCart::filters();

        return view('cases', [
            'cases' => $cases->appends(Input::except('page')),
            'type' => $type,
            'filters' => $filters
        ]);
    }

    public function winnerPost(Request $request)
    {
        if (!Auth::user()) {
            return redirect()->back();
        }

        $case = CaseCart::find($request->case);
        $case->status = CaseCart::STATUS_OPENED;
        $case->save();

        $winner_item = $case->items()->get()->random(1)->first();

        $winner = new Winner();
        $winner->user_id = Auth::user()->id;
        $winner->casecart_id = $case->id;
        $winner->caseitem_id = $winner_item->id;
        $winner->save();

        $skin = UserSkin::find($winner_item->user_skin_id);

        $new = new UserSkin();
        $new->class_id = $skin->steam_skin_id;
        $new->skin_id = $skin->skin_id;
        $new->steam_skin_id = $skin->steam_skin_id;
        $new->amount = $skin->amount;
        $new->position = $skin->position;
        $new->user_id = Auth::user()->id;
        $new->save();


        return redirect('/inventory');

    }


    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function openCases($id)
    {
        if (!Auth::user()) {
            $user_skins = [];
            return view('inventory', compact('user_skins'));
        }

        $case = CaseCart::find($id);
        $items = $case->items()->with('skin')->get();
        $items = $this->ItemsArray($items);

        $opened = CaseCart::opened();
        $cases = CaseCart::with('image')->get();
        $winners = Winner::with('item', 'cart', 'user')->where('casecart_id', $id)->get();
        if (is_null($winners)) {
            $winners = [];
        }

        return view('openCases', compact('items', 'case', 'opened', 'cases', 'winners'));
    }

    /**
     * @param $items
     * @return array
     * @internal param int $count
     */
    private function ItemsArray($items)
    {
        $items_array = [];

        foreach ($items as $item) {
            $array = array_fill(0, intval($item->odds), $item);
            $items_array = array_merge($items_array, $array);
        }

        shuffle($items_array);

        return $items_array;
    }

    /**
     * @param $id
     * @return int
     */
    public function getRandomInt($id = null)
    {

        $num = rand(0, 1000) % 100;
        $img = "";

        if (!empty($id)) {
            $case = CaseCart::find($id);
            $items = $case->items()->with('skin')->get();
            $items = $this->ItemsArray($items);
            $user_id = Auth::user()->id;
            $img = $items[$num]->skin->img;

            $this->setWinner($id, $items[$num], $user_id);
        }

        return json_encode(['num' => $num, "img" => $img]);
    }

    /**
     * @param $id
     * @param $item
     * @param $user_id
     */
    private function setWinner($id, $item, $user_id)
    {
        $case = CaseCart::find($id);
        $case->status = CaseCart::STATUS_OPENED;
        $case->open_count += 1;
        $case->save();

        $winner_item = $item;

        $winner = new Winner();
        $winner->user_id = $user_id;
        $winner->casecart_id = $case->id;
        $winner->caseitem_id = $winner_item->id;
        $winner->is_new = 1;
        $winner->save();

        $skin = OPSkinUserSkin::find($winner_item->user_skin_id);

        $new = new OPSkinUserSkin();
        $new->class_id = $skin->class_id;
        $new->skin_id = $skin->skin_id;
        $new->sale_id = $skin->sale_id;
        $new->opskins_skin_id = $skin->opskins_skin_id;
        $new->amount = $skin->amount;
        $new->stickers = $skin->stickers;
        $new->wear = $skin->wear;
        $new->user_id = Auth::user()->id;
        $new->save();

        // Latest Winner

        $latestWinner = Winner::with('item','cart','user')->where('id', $winner->id)->first();

        event(
            new LatestWinners(json_encode($latestWinner))
        );
    }

    /**
     * @param $id
     * @param int $count
     * @return array
     */
    public function getCaseItems($id, $count = 1)
    {
        if (!Auth::user()) {
            return redirect()->back();
        }

        $case = CaseCart::find($id);
        $items = $case->items()->with('skin')->get();
        $items = $this->ItemsArray($items, $count);
        $result = [];

        foreach ($items as $item) {
            $result[] = $item->skin->img;
        }

        return $result;
    }

    public function caseCreator(Request $request)
    {

        $case = new CaseCart();
        $data['creator_id'] = $request->user;
        $data['case_image_id'] = $request->image;
        $data['type'] = CaseCart::TYPE_COMMUNITY;
        $data['status'] = CaseCart::STATUS_CLOSED;
        $data['name'] = $request->case_name;
        $data['item_image'] = $request->skin_image;

        if (!$case->validate($data)) {
            return response(['error' => true, 'message' => $case->errors()]);
        }

        $case->fill($data);
        $case->save();

        $this->caseHelper->addCase($request->items, $case);

        $case->price = $request->price;

        $case->save();

        return response(['message' => 'Case created']);
    }

    public function deleteItemCase(Request $request, $id)
    {

        return ItemCase::delete($request->delete);

    }

    public function getEditItemCase($id)
    {

        $case = CaseCart::find($id);

        $result = $case->with('case_image', 'items')->get();

        return $result;

    }

    public function deleteCase(Request $request)
    {

        $case = CaseCart::find($request->id);

        $items = $case->items()->get();

        foreach ($items as $item) {

            ItemCase::delete($item->id);

        }

        $case->delete();

        return $case;

    }

    public function editCase(Request $request, $id)
    {

        $case = CaseCart::find($id);
        if ($request->image) {
            $case->case_image_id = $request->image;
        }
        $items = $case->items()->get();
        if ($request->item) {

            foreach ($items as $item) {
                if ($item->id == $request->item) {

                    $this->caseHelper->editItem($request, $item);
                }
            }
        }

        $case->save();

        return $items;

    }

    public function create()
    {

        $images = CaseImage::all();

        // if ($this->redis->hexists('bot_inv', 'bot' . date('y-m-d:H'))) {

        //    $bot_skins = json_decode($this->redis->hGet('bot_inv', 'bot' . date('y-m-d:H')), true);
        // } else {
        $bot_skins = OPSkin::limit(80)->has('user')->get();
        //     $this->redis->hset('bot_inv', 'bot' . date('y-m-d:H'), json_encode($bot_skins));

        //   }
        return view('create_case', compact('images', 'bot_skins'));
    }

    public function getSkinJson($id)
    {

        $item = OPSkin::find($id);

        return response($item);
    }


    public function getAllSkinsJson($name = null)
    {


        //$bot_skins = $this->steamInventory->getBotsInventoryByName(null, $name);

        $bot_skins = OPSkin::where('market_name', 'LIKE', '%' . addslashes($name) . '%')->limit(80)->has('user')->get();

        return response($bot_skins);

//		if ($this->redis->hexists('bot_inv', 'bot' . date('y-m-d:H'))) {
//
//			$bot_skins = json_decode($this->redis->hGet('bot_inv', 'bot' . date('y-m-d:H')), true);
//		} else {
//			$bot_skins = $this->steamInventory->getBotsInventory();
//			$this->redis->hset('bot_inv', 'bot' . date('y-m-d:H'), json_encode($bot_skins));
//
//		}
//		return response($bot_skins);

    }


}
