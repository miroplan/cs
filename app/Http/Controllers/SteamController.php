<?php

namespace App\Http\Controllers;

use App\UserProfile;
use Invisnik\LaravelSteamAuth\SteamAuth;
use App\User;
use Illuminate\Support\Facades\Auth;

class SteamController extends Controller
{
    /**
     * @var SteamAuth
     */
    private $steam;

    public function __construct(SteamAuth $steam)
    {
        $this->steam = $steam;
    }

    public function login()
    {
        if ($this->steam->validate()) {
            $info = $this->steam->getUserInfo();

            if (!is_null($info)) {
                /** @var User $user */
                $user = User::where('steamid', $info->steamID64)->first();

                if (is_null($user)) {
                    $user = new User();
                    $user->name = $info->personaname;
                    $user->password = bcrypt(uniqid());
                    $user->steamid = $info->steamID64;

                } else {
                    $user->updated_at = new \DateTime();
                }
                $user->save();
	            $userProfile = UserProfile::where('user_id',$user->id)->first();

	            if(is_null($userProfile)){
		            $userProfile = new UserProfile();
		            $userProfile->user_id = $user->id;
		            $userProfile->avatar =$info->avatar;
		            $userProfile->last_login =new \DateTime();
	            }else{
		            $userProfile->last_login =new \DateTime();
	            }
	            $userProfile->save();

                Auth::login($user, true);
                return redirect('/'); // redirect to site
            }
        }
        return $this->steam->redirect(); // redirect to Steam login page
    }
}
