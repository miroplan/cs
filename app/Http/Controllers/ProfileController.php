<?php

namespace App\Http\Controllers;

use App\UserProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{

    public function index(){
	    $user =Auth::user()->with('profile')->get();

	    return $user;
    }


    public function tradeUrl(Request $request){

		$validate = validate_trade_url($request->trade_url);
	    if($validate == true) {
		    $profile = Auth::user()->profile();
		    $profile->trade_url = $request->trade_url;
		    $profile->save();

		    return redirect()->back();

	    }

	    return $validate;

    }
}
