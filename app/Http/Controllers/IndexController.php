<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\CaseCart;
use App\FaqGroup;
use App\Faq;

class IndexController extends Controller
{
    public function index()
    {


	    $cases_official = CaseCart::with('image','items')
		    ->where('type',CaseCart::TYPE_OFFICIAL)
		    ->get();

	    $cases_community = CaseCart::with('image','items')
		    ->where('type',CaseCart::TYPE_COMMUNITY)
		    ->get();

	    return view('index',
		    compact('cases_official','cases_community'));
    }

    public function other()
    {
        return view('index', []);
    }

    public function terms()
    {
        return view('terms', []);
    }

    public function faq()
    {
    	  	
    	  			
    	  			
    	  			
        return view('faq', ['faqs' => FaqGroup::get()]);
    
    }


	public function profile()
	{
		return view('profile', []);
	}

	public function trade_url(Request $request)
	{
		return redirect('/profile');
	}
}
