<?php

namespace App\Http\Controllers;

use App\OPSkinsModels\OPSkin;
use App\OPSkinsModels\OPSkinUserSkin;
use App\Skin;
use App\Winner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InventoryController extends Controller
{
    public function index(Request $request)
    {
        if (!Auth::user()) {
            $user_skins = [];
            return view('inventory', compact('user_skins'));
        }


        if ($this->redis->hexists('user_inv', Auth::user()->steamid . date('y-m-d:H:m'))) {

            $user_inventory = json_decode($this->redis->hGet('user_inv', Auth::user()->steamid . date('y-m-d:H:m')), true);
        } else {
            $user_inventory = $this->steamInventory->getPlayerInventory(Auth::user());

            if (!$user_inventory) {
                $error = "This profile is private.";
                return view('inventory', compact('error'));
            }
            $this->redis->hset('user_inv', Auth::user()->steamid . date('y-m-d:H:m'), json_encode($user_inventory));

        }
        $user_skins = array();

        foreach ($user_inventory as $key => $item) {
            $skin = Skin::find($item['skin_id']);
            $skin->amount = $item['amount'];
            array_push($user_skins, $skin);
        }

        /*********User custom Inventory**************/
        $user_custom_skins = OPSkinUserSkin::where("user_id", Auth::user()->id)->get();


        $winners = Winner::where("user_id", Auth::user()->id)->where("is_new", 1)->count();

        return view('inventory', compact('user_skins', 'winners', 'user_custom_skins'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|null|string
     */
    public function saleItem($id)
    {
        if (!Auth::user()) {
            $user_skins = [];
            return view('inventory', compact('user_skins'));
        }

        $item = OPSkin::find($id);
        $skin = OPSkinUserSkin::where("user_id", Auth::user()->id)->where("skin_id", $id)->first();

        if ($item && $skin) {
            $skin->delete();
            $this->transactor->addUserBalance(Auth::user(), $item->price);

            return "success";
        }

        return null;


    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getWinnerSkins()
    {

        $winners = Winner::where("user_id", Auth::user()->id)->where("is_new", 1)->get();
        $items = [];

        if (!empty($winners)) {

            foreach ($winners as $winner) {
                $winner->is_new = 0;
                $winner->save();
                $items[] = $winner->item->skin;
            }
        }

        return response($items);
    }
}
