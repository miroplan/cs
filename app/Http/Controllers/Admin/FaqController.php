<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Faq;
use App\FaqGroup;
use Symfony\Component\HttpFoundation\Response;

class FaqController extends Controller
{

	public function group ()
	{

		return view('admin.pages.faq_group',['groups' => FaqGroup::all()]);

	}

	public function createGroup (Request $r)
	{

		$this->validate($r,[

				'title' => 'required|min:1',

			]);

		$group = new FaqGroup();
		$group->group_title = $r->get('title');
		$group->save();
		return redirect('/admin/faq')->with('group_id',$group->id);

	}

	public function updateGroup (Request $r)
	{

		return(FaqGroup::where('group_id',$r->get('id'))->update(['group_title' => $r->get('title')]) ? response(Response::$statusTexts[Response::HTTP_OK],Response::HTTP_OK) : response(Response::$statusTexts[Response::HTTP_BAD_REQUEST],Response::HTTP_BAD_REQUEST));

	}

	public function removeGroup (Request $r)
	{

		return FaqGroup::where('group_id',$r->get('id'))->delete() ? Response::$statusTexts[Response::HTTP_OK] : Response::$statusTexts[Response::HTTP_BAD_REQUEST];

	}

	public function faq (Request $r)
	{

		$groups = FaqGroup::all();

		return(count($groups) > 0 ? view('admin.pages.faq',compact('groups')) : redirect('admin/group'));

	}

	public function editFaq (Request $r)
	{

		return view('admin.pages.edit_faq',['groups' => FaqGroup::all()]);

	}

	public function getFaq (Request $r)
	{

		return view('admin.pages.faq_data',['faqs' => Faq::where('parent_id',$r->get('id'))->get()]);

	}

	public function createFaq (Request $r)
	{

		$faq = new Faq();
		$faq->parent_id = $r->get('group');
		$faq->question = $r->get('question');
		$faq->answer = $r->get('answer');
		return($faq->save() ? response(Response::$statusTexts[Response::HTTP_OK],Response::HTTP_OK) : response(Response::$statusTexts[Response::HTTP_BAD_REQUEST],Response::HTTP_BAD_REQUEST));

	}

	public function updateFaq (Request $r)
	{

		return(Faq::where('faq_id',$r->get('id'))->update($r->only(['question','answer'])) ? response(Response::$statusTexts[Response::HTTP_OK],Response::HTTP_OK) : response(Response::$statusTexts[Response::HTTP_BAD_REQUEST],Response::HTTP_BAD_REQUEST));

	}

	public function removeFaq (Request $r)
	{

		return Faq::where('faq_id',$r->get('id'))->delete() ? Response::$statusTexts[Response::HTTP_OK] : Response::$statusTexts[Response::HTTP_BAD_REQUEST];

	}

}
