<?php

namespace App\Http\Controllers\Admin;

use App\CaseImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CaseCart;
use App\ItemCase;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class CasesController extends Controller
{

    public function cases(){

	    $cases_official = CaseCart::with('items','image')->where('type',CaseCart::TYPE_OFFICIAL)->get();
	    $cases_community = CaseCart::with('items','image')->where('type',CaseCart::TYPE_COMMUNITY)
		    ->get();

	    return view('admin.pages.cases',
		    compact('cases_official','cases_community'));
    }

    public function getCaseCreate(){
	    $images = CaseImage::all();

	    if($this->redis->hexists('bot_inv','bot'.date('y-m-d:H'))){

		    $bot_skins =json_decode($this->redis->hGet('bot_inv','bot'.date('y-m-d:H')),true);
	    }
	    else{
		    $bot_skins = $this->steamInventory->getBotsInventory();
		    $this->redis->hset('bot_inv','bot'.date('y-m-d:H'),json_encode($bot_skins));

	    }
	    return view('admin.pages.create_case',compact('images','bot_skins'));
    }

    public function getCasesImages(){

	    $images = CaseImage::all();

	    return view('admin.pages.case_images',compact('images'));

    }

	public function createCasesImages(){

		return view('admin.pages.create_images');

	}

    public function createImage(Request $request){

	    $this->validate($request, [
		    'image_closed' => 'image|required',
		    'image_opened' => 'image|required',
	    ]);

	    if($request->file('image_opened')&&$request->file('image_closed')){
		    $image = new CaseImage();
		    $image_opened = time().$request->file('image_opened')
				    ->getClientOriginalName().'.'.$request->file('image_opened')
				    ->getClientOriginalExtension();
		    $image_closed = time().$request->file('image_closed')
				    ->getClientOriginalName().'.'.$request->file('image_opened')
				    ->getClientOriginalExtension();

		    $request->file('image_opened')
			    ->move(storage_path('app/public/case_images'), $image_opened);
		    $request->file('image_closed')
			    ->move(storage_path('app/public/case_images'), $image_closed);
		    $image->image_opened = $image_opened;
		    $image->image_closed = $image_closed;
		    if($request->background){
			    $image->background_color = $request->background;
		    }
		    $image->save();

		    return redirect('/admin/cases/images');

	    }

    }

	public function deleteImage(Request $request){

		return CaseImage::delete($request->id);
	}

	public function changeImage(Request $request){

		if($request->file('image_opened')){
			$this->validate($request, [
				'image_opened' => 'image|required',
			]);
		}

		if($request->file('image_closed')){
			$this->validate($request, [
				'image_closed' => 'image|required',
			]);
		}

		$image = CaseImage::find($request->id);

		if(!$image){
			return redirect()->back()->with('not_found','Requested image does not exist');
		}

		if($request->file('image_opened')){

			$image_opened = Hash::make($request->file('image_opened')
					->getClientOriginalName()).'.'.$request->file('image_opened')
					->getClientOriginalExtension();
			Storage::delete('/app/public/case_images'.$image->image_opened);

			$request->file('image_opened')
				->move(storage_path('app/public/case_images'), $image_opened);

			$image->image_opened = $image_opened;

		}

		if($request->file('image_closed')){

			$image_closed = Hash::make($request->file('image_closed')
					->getClientOriginalName()).'.'.$request->file('image_closed')
					->getClientOriginalExtension();

			Storage::delete('/app/public/case_images'.$image->image_closed);

			$request->file('image_opened')
				->move(storage_path('app/public/case_images'), $image_closed);

			$image->image_closed = $image_closed;

		}

		if($request->background){
			$image->background_color = $request->background;
		}

		$image->save();

		return redirect()->back();

	}

	public function caseCreator(Request $request){

		$case = new CaseCart();
		$case->creator_id = 0;
		$case->case_image_id = $request->image_id;
        $case->is_featured = $request->is_featured ? 1 : 0;
		$case->type =CaseCart::TYPE_OFFICIAL;
		$case->status =CaseCart::STATUS_CLOSED;
		$case->name = $request->name;
		$case->save();

        $func = $this->caseHelper->addCase($request->skin_id,$case);

        if(isset($func[1]))
		    $case->price = $func[1];

		$case->save();

		return redirect('/admin/cases');
	}

	public function deleteItemCase(Request $request,$id){

		return ItemCase::delete($request->delete);

	}

	public function getEditItemCase($id){

		$case = CaseCart::find($id);

		$result= $case->with('case_image','items')->get();

		return $result;

	}

	public function deleteCase(Request $request){

		$case = CaseCart::find($request->id);

		$items = $case->items()->get();

		foreach ($items as $item){

			ItemCase::delete($item->id);

		}

		$case->delete();

		return $case;

	}

	public function editItemCase(Request $request,$id){

		$case = CaseCart::find($id);

		$items = $case->items()->get();

		foreach ($items as $item){
			if($item->id == $request->item){

				$this->caseHelper->editItem($request,$item);
			}
		}

		return $items;

	}
}
