<?php

namespace App\Traits;

Trait BotConfigsTrait {

    private $configurationPrefix = 'BOT';
    private $configurationDelimiter = '_';
    private $dataSeparator = ',';
    private $keyValSeparator = ':';

    public function getBotConfigs($numberRequested = 0)
    {
        $numberOfBots = env('NUMBER_OF_BOTS');

        if ($numberRequested  > 0 && $numberRequested < $numberOfBots) {
            $numberOfBots = $numberRequested;
        }

        for ($i = 0; $i < $numberOfBots; ++$i) {

            $botConfig = env($this->configurationPrefix . $this->configurationDelimiter . $i);
            $botConfigArray  = explode($this->dataSeparator, $botConfig);

            foreach ($botConfigArray as $configArray) {
                $configArrayM = explode($this->keyValSeparator, $configArray);
                $botConfigMArray[$configArrayM[0]] = $configArrayM[1];
            }

            if (
                !empty($botConfigMArray['STEAM_API_KEY']) &&
                !empty($botConfigMArray['STEAM_64_ID']) &&
                !empty($botConfigMArray['ACCOUNT_USER']) &&
                !empty($botConfigMArray['ACCOUNT_PASS'])
            ) {
                yield $botConfigMArray;
            }
        }
    }
}