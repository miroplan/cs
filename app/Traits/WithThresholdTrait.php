<?php

namespace App\Traits;

Trait WithThresholdTrait {

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        return $this->executeWithThreshold();

    }

    /**
     * In *nix systems, minimum timing for the cron is 1 min.
     * but we need 30 secs here. So cron executes this command only 1 time
     * but we just put the logic to make this script work multiple times in 1 min.
     * so if our threshold is 2, then it will execute script then go to sleep for 60 / 2 and
     * come back to execute again. So practically it means our script worked 2 times in 1 min.
     * result = 30 secs cron.
     */
    protected function executeWithThreshold()
    {
        $dynamicThreshold = static::THRESHOLD;

        while ($dynamicThreshold) {

            $this->manage();
            $index = (static::THRESHOLD - $dynamicThreshold);
            $this->info(sprintf('Command Executed #%d', ++$index));
            if ($dynamicThreshold > 1) {
                sleep(60 / static::THRESHOLD);
            }

            --$dynamicThreshold;
        }
    }
}