<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FaqGroup extends Model
{
 
 
 
   protected $table = 'faq_group';
 
 
   public $timestamps = false;
 
}
