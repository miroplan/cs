<?php

namespace App\SteamModels;

use App\Skin;

/**
 * Class RGDescriptionModel
 * @package App\SteamModels
 */
class RGDescriptionModel implements Contracts\SteamInventoryModelInterface
{
    /**
     * @var
     */
    public $appId;
    /**
     * @var mixed
     */
    public $classId;
    /**
     * @var mixed
     */
    public $instanceId;
    /**
     * @var mixed
     */
    public $iconUrl;
    /**
     * @var mixed
     */
    public $iconDragUrl;
    /**
     * @var mixed
     */
    public $name;
    /**
     * @var mixed
     */
    public $marketHashName;
    /**
     * @var mixed
     */
    public $marketName;
    /**
     * @var mixed
     */
    public $nameColor;
    /**
     * @var mixed
     */
    public $backgroundColor;
    /**
     * @var mixed
     */
    public $type;
    /**
     * @var mixed
     */
    public $tradable;
    /**
     * @var mixed
     */
    public $marketable;
    /**
     * @var mixed
     */
    public $commodity;
    /**
     * @var mixed
     */
    public $marketTradableRestriction;
    /**
     * @var mixed
     */
    public $marketMarketableRestriction;
    /**
     * @var mixed
     */
    public $descriptions;
    /**
     * @var mixed
     */
    public $tags;

    public $skin;

    /**
     * RGDescriptionModel constructor.
     * @param array ...$description
     */
    public function __construct(array $description)
    {
        $this->appId = $description['appid'];
        $this->classId = $description['classid'];
        $this->instanceId = $description['instanceid'];
        $this->iconUrl = $description['icon_url'];
        $this->iconDragUrl = $description['icon_drag_url'];
        $this->name = $description['name'];
        $this->marketHashName = $description['market_hash_name'];
        $this->marketName = $description['market_name'];
        $this->nameColor = $description['name_color'];
        $this->backgroundColor = $description['background_color'];
        $this->type = $description['type'];
        $this->tradable = $description['tradable'];
        $this->marketable = $description['marketable'];
        $this->commodity = $description['commodity'];
        $this->marketTradableRestriction = $description['market_tradable_restriction'];

        if (isset($description['market_marketable_restriction'])) {
            $this->marketMarketableRestriction = $description['market_marketable_restriction'];
        }

        $this->descriptions = $description['descriptions'];
        $this->tags = $description['tags'];
    }

    /**
     *
     */
    public function getEloquentModel()
    {
        $skin = new Skin();
        $skin->app_id = $this->appId;
        $skin->class_id = $this->classId;
        $skin->instance_id = $this->instanceId;
        $skin->icon_url = $this->iconUrl;
        $skin->icon_drag_url = $this->iconDragUrl;
        $skin->name = $this->name;
        $skin->market_hash_name = $this->marketHashName;
        $skin->market_name = $this->marketName;
        $skin->name_color = $this->nameColor;
        $skin->background_color = $this->backgroundColor;
        $skin->type = $this->type;
        $skin->tradable = $this->tradable;
        $skin->marketable = $this->marketable;
        $skin->commodity = $this->commodity;
        $skin->market_tradable_restriction = $this->marketTradableRestriction;
        $skin->market_marketable_restriction = $this->marketMarketableRestriction;
        $skin->descriptions = $this->descriptions;
        $skin->tags = $this->tags;
        $skin->created_at = new \DateTime();
        $skin->updated_at = $skin->created_at;
        $this->skin = $skin;

        return $skin;
    }

    public function toArray()
    {
        if ($this->skin instanceof Skin) {
            return $this->skin->toArray();
        }

        return [];
    }
}