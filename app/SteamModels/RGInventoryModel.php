<?php

namespace App\SteamModels;

use App\UserSkin;
use App\Skin;

/**
 * Class RGInventoryModel
 * @package App\SteamModels
 */
class RGInventoryModel implements Contracts\SteamInventoryModelInterface
{
    /**
     * @var
     */
    protected $userId;

    /**
     * @var
     */
    protected $skinId;

    /**
     * @var
     */
    protected $steamSkinId;
    /**
     * @var
     */
    protected $classId;
    /**
     * @var
     */
    protected $instanceId;
    /**
     * @var
     */
    protected $position;
    /**
     * @var
     */
    protected $amount;

    /**
     * @var
     */
    protected $userSkin;

    /**
     * RGInventoryModel constructor.
     * @param $inventory
     */
    public function __construct($inventory, $userId)
    {

	    Skin::where('class_id', $inventory['classid'])->first()->id;
        $this->skinId = Skin::where('class_id', $inventory['classid'])->first()->id;
        $this->userId = $userId;
//		dd($this);
        $this->steamSkinId = $inventory['classid'];
        $this->classId = $inventory['classid'];
        $this->instanceId = $inventory['instanceid'];
        $this->amount = $inventory['amount'];
    }

    /**
     *
     */
    public function getEloquentModel()
    {
        if (!$this->skinId) {
            return [];
        }

        $userSkin = new UserSkin();
        $userSkin->skin_id = $this->skinId;
        $userSkin->steam_skin_id = $this->steamSkinId;
        $userSkin->amount = $this->amount;
		$userSkin->classid = $this->classId;
        $this->userSkin = $userSkin;

        return $userSkin;
    }

    public function toArray()
    {
        if ($this->userSkin instanceof UserSkin) {
            return $this->userSkin->toArray();
        }

        return [];
    }
}