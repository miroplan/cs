<?php

namespace App\SteamModels\Contracts;


interface SteamInventoryModelInterface
{
    public function getEloquentModel();
    public function toArray();
}