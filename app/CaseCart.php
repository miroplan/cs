<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\CaseImage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Validator;
use Auth;

class CaseCart extends Model
{
    const TYPE_OFFICIAL = 1;
    const TYPE_COMMUNITY = 0;

    const STATUS_CLOSED = 0;
    const STATUS_OPENED = 1;

    protected $fillable = array('creator_id', 'case_image_id','type','status','name','item_image','is_featured','open_count');

    private $rules = array(
        'creator_id' => 'required|max:11',
        'name' => 'required|max:191',
        'item_image' => 'max:512',
        'case_image_id' => 'required|exists:case_images,id|max:11',
        'status' => 'required|in:' . self::STATUS_CLOSED . ', ' . self::STATUS_OPENED,
        'type' => 'required|in:' . self::TYPE_COMMUNITY, ',' . self::TYPE_OFFICIAL,
    );

    private $errors;

    static function opened()
    {
        return count(self::where('status', self::STATUS_OPENED)->get());
    }

    /*
     * This Scope adds a custom condition to filter by type
     * @param string $type Selected type by query
     * @param boolean $customField Checking selected type if it's custom field or not
     *
     * @return Query object
    */
    public function scopeType($query, $type, $customField = false) {

        if(!$type)
            $customField = "featured";

        switch ($type) {
            case 'official':
                $type = CaseCart::TYPE_OFFICIAL;
                break;
            case 'custom':
                $type = CaseCart::TYPE_COMMUNITY;
                break;
            case 'new':
            case 'my':
            case 'top1':
            case 'top7':
            case 'top2w':
                $customField = $type;
                break;
            default: $type = CaseCart::TYPE_OFFICIAL;
        }

        if($customField == "featured")
            return $query->where('is_featured', 1);

        if($customField == "new")
            return $query->orderBy('created_at', 'DESC');

        if($customField == "top1") {
            return $query->selectRaw('case_carts.*, COUNT(w.id) as winners_count')->leftjoin('winners as w', 'w.casecart_id', '=', 'case_carts.id')->groupBy('w.casecart_id')->whereRaw('Date(w.created_at) = CURDATE()')->orderBy('winners_count', 'desc');
        }

        if($customField == "top7" || $customField == "top2w") {

            $days = 7;

            if($customField == "top2w")
                $days = 14;

            $fromDate = date('Y-m-d', strtotime("-$days days"));

            return $query->selectRaw('case_carts.*, COUNT(w.id) as winners_count')->leftjoin('winners as w', 'w.casecart_id', '=', 'case_carts.id')->groupBy('w.casecart_id')->whereRaw('w.created_at >= "'.$fromDate.'"')->orderBy('winners_count', 'desc');
        }

        if($customField == "my" && Auth::check()) {
            return $query->where('creator_id', Auth::id());
        }

        if($type == CaseCart::TYPE_OFFICIAL)
            return $query->where('type', 1)->orderBy('created_at', 'DESC');

        return $query->orderBy('created_at', 'DESC');
    }

    /*
     * This Scope filters data by given condition
     *
     * @return Query object
    */
    public function scopeFilter($query) {

        if($min_price = Input::get('min_price', false))
            $query->where('price', '>=', $min_price);

        if($max_price = Input::get('max_price', false))
            $query->where('price', '<=', $max_price);

        if($search = Input::get('search', false))
            $query->where('name', 'LIKE','%'.$search.'%');

        if($opened = Input::get('min_opened', false)) {
            $query->where('open_count', '>=', $opened);
        }

        return $query;
    }

    /*
     * Get the filtering options
     *
     * @return Array
    */
    static function filters() {
        $filters = [
            ''         => ['label' => 'Featured'],
            'official' => ['label' => 'Official'],
            'new'      => ['label' => 'New Cases'],
            'custom'   => ['label' => 'Custom'],
            'top1'     => ['label' => 'Top 1 Day'],
            'top7'     => ['label' => 'Top 7 Days'],
            'top2w'    => ['label' => 'Top 2 Weeks'],
            'my'       => ['label' => 'My Cases'],
        ];

        if(!Auth::check())
            unset($filters['my']);

        return $filters;
    }

    public function validate($data)
    {
        $v = Validator::make($data, $this->rules);

        if ($v->fails()) {
            $this->errors = $v->errors()->first();
            return false;
        }

        return true;
    }

    public function errors()
    {
        return $this->errors;
    }

    public function items()
    {

        return $this->hasMany('App\ItemCase', 'casecart_id');

    }

    public function image()
    {

        return $this->belongsTo('App\CaseImage', 'case_image_id');
    }

    public function winnings()
    {

        return $this->hasMany('App\Winner');
    }
}
