<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Winner extends Model
{
    public function user(){

	    return $this->belongsTo('App\User');

    }

	public function cart(){

		return $this->belongsTo('App\CaseCart','casecart_id');
	}
	public function item(){

		return $this->belongsTo('App\ItemCase','caseitem_id');

	}
}
