<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSkin extends Model
{

	protected $table = 'user_skins';
	protected $fillable = ['class_id','steam_skin_id', 'amount', 'position',
		'user_id' ];
	/**
	 * The user that belong to the skin.
	 */
	public function user()
	{
		return $this->belongsTo('App\User');
	}

	/**
	 * The skin that belong to the user.
	 */
	public function skin()
	{
		return $this->belongsTo('App\Skin');
	}
}
