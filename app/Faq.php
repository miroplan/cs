<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{

    protected $table = 'faq';
    
    public $timestamps = false;

}
