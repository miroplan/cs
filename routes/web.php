<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/inventory', 'InventoryController@index')->name('inventory');
Route::get('/cases/{type?}', 'CasesController@index')->name('cases');
Route::get('/inventory/sale/{id}', 'InventoryController@saleItem')->name('saleItem');
Route::get('/inventory/winners', 'InventoryController@getWinnerSkins')->name('getWinnerSkins');
Route::get('/opencases/{id}', 'CasesController@openCases')->name('openCases');
Route::get('/opencases/{id}/caseItems', 'CasesController@getCaseItems')->name('getCaseItems');
Route::get('/tradeup', 'TradeupController@index')->name('tradeup');
Route::get('/giftcards', 'GiftcardsController@index')->name('giftcards');
Route::get('/giftcardDetails', 'GiftcardsController@giftcardDetails')->name('giftcardDetails');
Route::get('/other', 'IndexController@other')->name('other');
Route::get('/marketplace', 'MarketplaceController@index')->name('marketplace');
Route::get('/terms', 'IndexController@terms')->name('terms');
Route::get('/faq', 'IndexController@faq')->name('faq');
Route::get('/support', 'SupportController@support')->name('support');
Route::get('/contact', 'ContactController@contact')->name('contact');
Route::get('/privacy_policy', 'SupportController@policy')->name('policy');
Route::get('/profile', 'IndexController@profile')->name('profile');
Route::post('/profile/trade_url', 'IndexController@trade_url')->name('trade_url');


Route::post('/new_offer','MarketplaceController@new_trade')->name('new_trade');
Route::post('/cancel_offer','MarketplaceController@cancel_offer')->name('cancel_offer');
Route::get('/getBotTradeOfferUrl','MarketplaceController@getBotTradeOfferUrl')->name('getBotTradeOfferUrl');
Route::get('/accept_offer','MarketplaceController@accept_offer')->name('accept_offer');
Route::post('/updateTradeUrl','MarketplaceController@updateTradeUrl')->name('updateTradeUrl');
Route::get('/getAccountBalance','MarketplaceController@getAccountBalance')->name('getAccountBalance');


// cases
Route::post('/storeCase','CasesController@caseCreator')->name('store_case');
Route::post('/winner/post','CasesController@winnerPost')->name('winner_case');
Route::get('/case_create','CasesController@create')->name('case_create');
Route::post('/api/storeCase','CasesController@caseCreator')->name('store_case');
Route::get('/getrandom/{id?}','CasesController@getRandomInt', function ($id = null) {
    //
})->where('id', '[0-9]+');

// Steam login
Route::get('/login', 'SteamController@login')->name('login');



Route::group(['middleware' => 'admin','prefix' => '/admin'], function () {

	Route::get('/','Admin\AdminController@index');
    Route::get('/cases','Admin\CasesController@cases');
    Route::get('/case/create','Admin\CasesController@getCaseCreate');
	Route::get('/cases/images','Admin\CasesController@getCasesImages');
	Route::get('/cases/images/create','Admin\CasesController@createCasesImages');
	Route::post('/store_image','Admin\CasesController@createImage');
	Route::post('/store_case','Admin\CasesController@caseCreator');

    Route::get('/group','Admin\FaqController@group');
    Route::post('/group/create','Admin\FaqController@createGroup');
    Route::post('/group/remove','Admin\FaqController@removeGroup');
    Route::post('/group/update','Admin\FaqController@updateGroup');
  
    Route::get('/faq','Admin\FaqController@faq');
    Route::get('/faq/edit','Admin\FaqController@editFaq');
    Route::post('/faq/create','Admin\FaqController@createFaq');
    Route::post('/faq/get','Admin\FaqController@getFaq');
    Route::post('/faq/remove','Admin\FaqController@removeFaq');
    Route::post('/faq/update','Admin\FaqController@updateFaq');
});


Route::group(['prefix' => '/admin/auth', 'as' => 'admin.auth.'], function () {

	Route::get('login', ['as' => 'login', 'uses' => 'Admin\Auth\LoginController@showLoginForm']);
	Route::post('login', ['as' => 'login.post', 'uses' => 'Admin\Auth\LoginController@login']);
	Route::post('logout', ['as' => 'logout', 'uses' => 'Admin\Auth\LoginController@logout']);

});


Route::get('/case_images/{filename}', function ($filename)
{
	$response = case_images($filename);
	return $response;
});

