<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpskinsUserSkinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opskins_user_skins', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->integer('skin_id')->unsigned();
            $table->integer('sale_id')->unsigned();
            $table->bigInteger('opskins_skin_id', false, true);
            $table->bigInteger('class_id');
            $table->integer('amount');
            $table->string('stickers')->nullable();
            $table->double('wear')->nullable();
            $table->integer('bot_id');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opskins_user_skins');
    }
}
