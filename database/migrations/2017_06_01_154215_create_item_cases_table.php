<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_cases', function (Blueprint $table) {
            $table->increments('id');
	        $table->integer('casecart_id');
	        $table->integer('skin_id');
	        $table->integer('user_skin_id');
	        $table->double('price');
	        $table->double('odds')->nullable();
            $table->timestamps();
        });
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_cases');
    }
}
