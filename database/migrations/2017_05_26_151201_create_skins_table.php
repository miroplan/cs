<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSkinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('skins', function (Blueprint $table) {
		    $table->increments('id');
		    $table->integer('app_id')->default(730);
		    $table->bigInteger('class_id');
		    $table->bigInteger('instance_id')->nullable();
		    $table->string('icon_url', 2083);
		    $table->string('icon_drag_url', 2083)->nullable();
		    $table->string('name');
		    $table->string('market_hash_name');
		    $table->string('market_name');
		    $table->char('name_color', 6)->nullable();
		    $table->char('background_color', 6)->nullable();
		    $table->string('type', 50);
		    $table->tinyInteger('tradable')->nullable();
		    $table->tinyInteger('marketable')->nullable();
		    $table->tinyInteger('commodity')->nullable();
		    $table->integer('market_tradable_restriction')->nullable();
		    $table->integer('market_marketable_restriction')->nullable();
		    $table->text('descriptions')->nullable();
		    $table->text('tags')->nullable();
		    $table->double('price')->nullable();
		    $table->timestamps();

	    });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('skins');
    }
}
