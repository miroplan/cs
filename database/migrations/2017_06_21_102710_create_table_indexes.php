<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('opskins_user_skins', function (Blueprint $table) {
            $table->foreign('skin_id','opskin_id_user_skin_id')
                ->references('id')->on('opskins_skins')
                ->onDelete('cascade');

            $table->unique('sale_id');
            $table->unique('opskins_skin_id');
        });

        Schema::table('opskins_skins', function (Blueprint $table) {
            $table->index('market_name', 'market_name_index');
            $table->unique('class_id');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('opskins_skins', function (Blueprint $table) {
            $table->dropIndex('market_name_index');
            $table->dropUnique('class_id');
        });

        Schema::table('opskins_user_skins', function (Blueprint $table) {
             $table->dropForeign('opskin_id_user_skin_id');
            $table->dropUnique('sale_id');
            $table->dropUnique('opskins_skin_id');
        });
    }
}
