<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSkinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('user_skins', function (Blueprint $table) {
		    $table->increments('id');
		    $table->integer('user_id')->nullable();
		    $table->integer('skin_id')->unsigned();
		    $table->bigInteger('steam_skin_id', false, true);
		    $table->integer('position');
		    $table->bigInteger('class_id');
		    $table->integer('amount');
		    $table->timestamps();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_skins');
    }
}
