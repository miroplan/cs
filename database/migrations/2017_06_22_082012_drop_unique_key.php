<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropUniqueKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('opskins_user_skins', function (Blueprint $table) {
            $table->dropUnique('opskins_user_skins_opskins_skin_id_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('opskins_user_skins', function (Blueprint $table) {
            $table->unique('opskins_skin_id');
        });
    }
}
