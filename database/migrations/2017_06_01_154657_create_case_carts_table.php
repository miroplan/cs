<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaseCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('case_carts', function (Blueprint $table) {
            $table->increments('id');
	        $table->integer('creator_id');
	        $table->integer('case_image_id');
	        $table->string('item_image')->nullable();
	        $table->string('name')->nullable();
	        $table->double('price')->nullable();
	        $table->boolean('status')->default(0);
	        $table->boolean('type')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('case_carts');
    }
}
