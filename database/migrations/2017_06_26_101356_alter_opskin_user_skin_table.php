<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOpskinUserSkinTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('opskins_user_skins', function (Blueprint $table) {
            $table->dropUnique('opskins_user_skins_sale_id_unique');
            $table->index('sale_id', 'sale_id_index');
            $table->integer('bot_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('opskins_user_skins', function (Blueprint $table) {
            $table->dropIndex('sale_id_index');
            $table->unique('sale_id');
        });
    }
}
