<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpskinsSkinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opskins_skins', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('app_id')->default(730);
            $table->bigInteger('class_id');
            $table->bigInteger('instance_id')->nullable();
            $table->string('img', 2083);
            $table->string('market_name');
            $table->string('inspect')->nullable();
            $table->string('type', 50);
            $table->integer('context_id');
            $table->double('price')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opskins_skins');
    }
}
