// Live Chat
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var Redis = require('ioredis');
var redis = new Redis();

redis.psubscribe('*', function(error, count){
    //
});

redis.on('pmessage', function(pattern, channel, message){
    console.log(message);
    //latest-winners-template
    var message = JSON.parse(message);

    if(message.event && message.data.message)
        io.emit(channel + ':' + message.event, message.data.message);
});

http.listen(3000, function(){
    console.log('listening on *:3000');
});
