// Scroll Bar 
// ================================================== 

	(function($){
		$(window).on("load",function(){
			
			$(".left-scroll, .chat-ul, .dropdown-cart-tbody").mCustomScrollbar({
				theme:"minimal",
				scrollButtons:{
					enable:true
				},
				callbacks:{
					onScrollStart:function(){ myCallback(this,"#onScrollStart") },
					onScroll:function(){ myCallback(this,"#onScroll") },
					onTotalScroll:function(){ myCallback(this,"#onTotalScroll") },
					onTotalScrollOffset:60,
					onTotalScrollBack:function(){ myCallback(this,"#onTotalScrollBack") },
					onTotalScrollBackOffset:50,
					whileScrolling:function(){ 
						myCallback(this,"#whileScrolling"); 
						$("#mcs-top").text(this.mcs.top);
						$("#mcs-dragger-top").text(this.mcs.draggerTop);
						$("#mcs-top-pct").text(this.mcs.topPct+"%");
						$("#mcs-direction").text(this.mcs.direction);
						$("#mcs-total-scroll-offset").text("60");
						$("#mcs-total-scroll-back-offset").text("50");
					},
					alwaysTriggerOffsets:false,
					autoHideScrollbar:true
				}
			});
			
			function myCallback(el,id){
				if($(id).css("opacity")<1){return;}
				var span=$(id).find("span");
				clearTimeout(timeout);
				span.addClass("on");
				var timeout=setTimeout(function(){span.removeClass("on")},350);
			}
			
			$(".callbacks a").click(function(e){
				e.preventDefault();
				$(this).parent().toggleClass("off");
				if($(e.target).parent().attr("id")==="alwaysTriggerOffsets"){
					var opts=$(".left-scroll, .chat-ul, .dropdown-cart-tbody").data("mCS").opt;
					if(opts.callbacks.alwaysTriggerOffsets){
						opts.callbacks.alwaysTriggerOffsets=false;
					}else{
						opts.callbacks.alwaysTriggerOffsets=true;
					}
				}
			});
			
		});
		})(jQuery);


$(document).ready(function(){


// header bottom Fix
// ==================================================

	$('.header-bottom').affix({offset: {top: 176} });


// Tooltipster-1
// ==================================================

	$('.tooltipster-1').tooltipster({
		interactive : true,
		contentAsHTML : true,
		side: ['bottom'],
		maxWidth : 270
	});


// Spin Slider
// ==================================================

	var owl = $(".spin-slider-1");
     
      owl.owlCarousel({
         
          itemsCustom : [
            [0, 1],
            [450, 1],
            [600, 3],
            [700, 3],
            [1000, 3],
            [1200, 5],
            [1400, 7],
            [1600, 7]
          ],
          navigation : false,
          pagination : false,
          scrollPerPage : true,
          transitionStyle : true,
          // autoPlay : true,
     	
      });



      var owl = $(".spin-slider-2");
     
      owl.owlCarousel({
         
          itemsCustom : [
            [0, 1],
            [450, 1],
            [600, 3],
            [700, 3],
            [1000, 3],
            [1200, 5],
            [1400, 5],
            [1600, 5]
          ],
          navigation : true,
          pagination : false,
          scrollPerPage : true,
          transitionStyle : true,
          navigationText : ["<img class='img-responsive' src='images/spin-slider-arrow-left.png' alt='product'>","<img class='img-responsive' src='images/spin-slider-arrow-right.png' alt='product'>"]
          // autoPlay : true,
     	
      });

        $('.spin-slider-2-link').on('click', function(event){
    		var $this = $(this);
    		if($this.hasClass('selected')){
      			$this.removeAttr('style').removeClass('selected');
    		} else{
      			$this.addClass('selected');
    		}
  		});


// Range Slider
// ==================================================

	$(".range-slider").ionRangeSlider({
		type: "double",
		grid: false,
		min: 0,
		max: 200,
		from: 1,
		to: 150,
		prefix: "$"
	});

	$(".range-slide-2").ionRangeSlider({
		 type: "single",
	});


// Smart Menu
// ==================================================

	$('#main-menu').smartmenus({
		subMenusSubOffsetX: 1,
		subMenusSubOffsetY: -8
	});



});