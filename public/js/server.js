var protocol = location.protocol;
var slashes = protocol.concat("//");
var port = "3000";
var host = slashes.concat(window.location.hostname) + ":" + port;
var socketio = io(host);

socketio.on('winners:message', function (data) {
    getLatestWinners(data);
});