// Scroll Bar 
// ================================================== 

(function ($) {
    $(window).on("load", function () {


        $(".left-scroll, .chat-ul, .dropdown-cart-tbody, #skins_tab").mCustomScrollbar({
            theme: "minimal",
            scrollButtons: {
                enable: true
            },
            callbacks: {
                onScrollStart: function () {
                    myCallback(this, "#onScrollStart")
                },
                onScroll: function () {
                    myCallback(this, "#onScroll")
                },
                onTotalScroll: function () {
                    myCallback(this, "#onTotalScroll")
                },
                onTotalScrollOffset: 60,
                onTotalScrollBack: function () {
                    myCallback(this, "#onTotalScrollBack")
                },
                onTotalScrollBackOffset: 50,
                whileScrolling: function () {
                    myCallback(this, "#whileScrolling");
                    $("#mcs-top").text(this.mcs.top);
                    $("#mcs-dragger-top").text(this.mcs.draggerTop);
                    $("#mcs-top-pct").text(this.mcs.topPct + "%");
                    $("#mcs-direction").text(this.mcs.direction);
                    $("#mcs-total-scroll-offset").text("60");
                    $("#mcs-total-scroll-back-offset").text("50");
                },
                alwaysTriggerOffsets: false,
                autoHideScrollbar: true
            }
        });

        function myCallback(el, id) {
            if ($(id).css("opacity") < 1) {
                return;
            }
            var span = $(id).find("span");
            clearTimeout(timeout);
            span.addClass("on");
            var timeout = setTimeout(function () {
                span.removeClass("on")
            }, 350);
        }

        $(".callbacks a").click(function (e) {
            e.preventDefault();
            $(this).parent().toggleClass("off");
            if ($(e.target).parent().attr("id") === "alwaysTriggerOffsets") {
                var opts = $(".left-scroll, .chat-ul, .dropdown-cart-tbody").data("mCS").opt;
                if (opts.callbacks.alwaysTriggerOffsets) {
                    opts.callbacks.alwaysTriggerOffsets = false;
                } else {
                    opts.callbacks.alwaysTriggerOffsets = true;
                }
            }
        });

    });
})(jQuery);


function getInfo(id) {

    var modal = $("#item_modal"),
        body = modal.find(".winning-body"),
        item_name = modal.find(".item_name"),
        item_img = modal.find(".item_img"),
        sale_button = $("#sale_item_button"),
        html = "",
        total_price = 0,
        item_ids = [];

    if (typeof id === "undefined" || id === null) {

        item_name.text("Your winnings");
        $.get('/inventory/winners', function (items) {

            if (items.length == 1) {
                var item = items[0];
                item_name.text(item["market_name"]);
                item_img.attr("src", "http://steamcommunity.com/economy/image/" + item['img']);

                sale_button.data("id", item["id"]).html("<i class='fa fa-shopping-cart' aria-hidden='true'></i>Sell for $" + item['price']);
                modal.modal('show');
            } else {
                body.empty();

                $.each(items, function (id, item) {

                    html = '<div class="winning-multi">' +
                        '<div class="winningimg">' +
                        '<span class="product-1-img">' +
                        '<img class="img-responsive center-block" src="http://steamcommunity.com/economy/image/' + item['img'] + '" alt="product">' +
                        '</span></div>' +
                        '<div class="winningtext">' +
                        '<span><small>' + item["market_name"] + '</small></span>' +
                        '<a href="#" onclick="saleItem(' + item['id'] + ')" class="model-marketplace-btn"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Sell for $' + item['price'] + '</a>' +
                        '<a href="#" class="model-trade-btn"><i class="fa fa-address-card-o"aria-hidden="true"></i></a>' +
                        '</div>' +
                        '<div class="clearfix"></div>' +
                        '</div>';

                    body.append(html);

                    total_price += item['price'];
                    item_ids.push(item['id']);
                    sale_button.data("id", item_ids).html("<i class='fa fa-shopping-cart' aria-hidden='true'></i>Sell all for $" + total_price);
                })
            }
        });

        modal.modal('show');

    } else {

        $.get('/api/skin/' + id, function (item) {

            item_name.text(item["market_name"]);
            item_img.attr("src", "http://steamcommunity.com/economy/image/" + item['img']);

            sale_button.data("id", item["id"]).html("<i class='fa fa-shopping-cart' aria-hidden='true'></i>Sell for $" + item['price']);
            modal.modal('show');

        });
    }
}


function saleItem(id) {
    $.get('/inventory/sale/' + id, function (status) {

        if (status == "success") {

            $('div[class="modal-body"]').empty();
            var text = '<h3>Item successfully sold</h3>';
            $('div[class="modal-body"]').append(text);
            $('#status_modal').modal('show');
        } else {
            $('div[class="modal-body"]').empty();
            var text = '<h3>Item not found</h3>';
            $('div[class="modal-body"]').append(text);
            $('#status_modal').modal('show');
        }

    });
}

$(document).ready(function () {


// header bottom Fix
// ==================================================

    $('.header-bottom').affix({offset: {top: 176}});


// Tooltipster-1
// ==================================================

    $('.tooltipster-1').tooltipster({
        interactive: true,
        contentAsHTML: true,
        side: ['bottom'],
        maxWidth: 270
    });


// Roulette
// =================================================

    var roulette = $(".spin-slider-1"),
        spin_button = $('#test_spin_button'),
        open_case_button = $('#open_case_button'),
        price = $('div.spin-currency span'),
        prev_val = 1,
        case_id = parseInt(location.href.substr(location.href.lastIndexOf('/') + 1)),
        roulettes_count = 1,
        test_spin = 1;


    roulette.roulette({
        startCallback: function () {
            spin_button.removeClass('start');
            open_case_button.removeClass('start');
        },
        slowDownCallback: function () {
        },
        stopCallback: function () {
            roulette_stop();
        }
    });

    //Test Spin roulette

    spin_button.click(function () {
        if ($(this).hasClass('start')) {
            $(".spin-slider-1").roulette('start');
            test_spin = 1;
        }
    });

    //Open Case

    open_case_button.click(function () {
        if ($(this).hasClass('start')) {
            $(".spin-slider-1").roulette('start', case_id);
            test_spin = 0;
        }
    });

    $('#roulette_counter').on('focus', function () {
        prev_val = $(this).val();
    }).change(function () {

        var val = $(this).val(),
            current_roulette_count = $('.owl-carousel.spin-slider-1').length,
            div_el = '';

        price.text(parseFloat(price.text()) * val / prev_val);
        prev_val = val;
        roulettes_count = val;

        if (val > current_roulette_count) {

            for (var it = 0; it < (val - current_roulette_count); it++) {

                $.get('/opencases/' + case_id + '/caseItems', {count: roulettes_count}, function (items) {
                    div_el = $('<div/>', {
                        class: 'owl-carousel owl-theme spin-slider-1',
                    }).insertAfter(roulette);

                    for (var i = 0; i < items.length; i++) {
                        div_el.append('<img width="219" class="owl-item img-responsive center-block" src="https://steamcommunity-a.akamaihd.net/economy/image/' + items[i] + '">');
                    }

                    div_el.roulette({
                        startCallback: function () {
                            spin_button.removeClass('start');
                            open_case_button.removeClass('start');
                        },
                        slowDownCallback: function () {
                        },
                        stopCallback: function () {
                            roulette_stop();
                        }
                    });

                });

            }

        } else {
            for (var it = 0; it < (current_roulette_count - val); it++) {
                $('.owl-carousel.spin-slider-1').last().remove();
            }
        }
    });

    function roulette_stop() {
        spin_button.addClass('start');
        open_case_button.addClass('start');

        if (test_spin == 0) {
            location.href = "/inventory";
        }
    }

// Inventory Item
// ==================================================

    $("a.inventory_info").click(function () {
        getInfo($(this).data("id"));
    });


// Sale Item
// ==================================================

    $("#sale_item_button").click(function () {
        var item_id = $(this).data("id");

        if ($.isArray(item_id)) {
            for (var i = 0; i < item_id.length; i++) {
                saleItem(item_id[i]);
            }
        } else {
            saleItem(item_id);
        }
    });

// Spin Slider
// ==================================================


    /*  owl.owlCarousel({

     itemsCustom: [
     [0, 1],
     [450, 1],
     [600, 3],
     [700, 3],
     [1000, 3],
     [1200, 5],
     [1400, 7],
     [1600, 7]
     ],
     navigation: false,
     pagination: false,
     scrollPerPage: true,
     transitionStyle: true,
     // autoPlay : true,


     });*/


    var owl1 = $(".spin-slider-2");
    owl1.hide();
    owl1.owlCarousel({

        itemsCustom: [
            [0, 1],
            [450, 1],
            [600, 3],
            [700, 3],
            [1000, 3],
            [1200, 5],
            [1400, 5],
            [1600, 5]
        ],
        navigation: true,
        pagination: false,
        scrollPerPage: true,
        transitionStyle: true,
        navigationText: ["<img class='img-responsive' src='images/spin-slider-arrow-left.png' alt='product'>", "<img class='img-responsive' src='images/spin-slider-arrow-right.png' alt='product'>"]
        // autoPlay : true,

    });
    $('#ellipsis').hide();
    owl1.show();

    $('.spin-slider-2-link').on('click', function (event) {
        var $this = $(this);

        if ($this.hasClass('selected')) {
            $this.removeAttr('style').removeClass('selected');
        } else {
            $('.spin-slider-2-link').removeAttr('style').removeClass('selected');
            $this.addClass('selected');
        }
    });


// Range Slider
// ==================================================

    $(".range-slider").ionRangeSlider({
        type: "double",
        grid: false,
        min: 0,
        max: 200,
        from: 1,
        to: 150,
        prefix: "$"
    });

    $(".range-slide-2").ionRangeSlider({
        type: "single",
        from: 0,
        step: 0.01,
        to: 4,
        min: 0,
        max: 4,
    });
    $(".range-slide-2").on("change", function () {
        var $this = $(this),
            value = $this.prop("value");
        var input = $('input[name="affilate"]');
        input.val(value + '%');
    });


// Smart Menu
// ==================================================

    $('#main-menu').smartmenus({
        subMenusSubOffsetX: 1,
        subMenusSubOffsetY: -8
    });

// Select Skin Creator and ODDS Adding
// ==================================================

    $('#skins_tab').on('click', '.product-1-select-hover', function () {
        var $this = $(this);

        if ($this.hasClass('selected')) {
            $this.removeAttr('style').removeClass('selected');
            var id = $this.attr("id");
            $('tr[id=' + id + ']').remove();
        } else {
            var id = $this.attr("id");
            $this.addClass('selected');
            $.get('/api/skin/' + id, function (item) {

                var html = $('<tr id="' + item['id'] + '"><td>' +
                    '<input id="radio-' + item['id'] + '" class="magic-radio" value="' + item['img']
                    + '" name="radio" type="radio"><label class="checkbox-radio-label" for="radio-' + item['id']
                    + '">&nbsp;</label></td>' +
                    '<td><a class="product-img" href="javascript:void(0);"><img src="https://steamcommunity-a.akamaihd.net/economy/image/' + item['img'] +
                    '" alt="product"></a></td>' +
                    '<td><a class="product-name" href="javascript:void(0);">' + item['market_name']
                    + '</a></td>' +
                    '<td>$<span id="price">' + item['price'] + '</span></td>' +
                    '<td><input class="form-control affiliate-input" name="odds[]" id="input_' + item['id'] + '" value="0" min="0" type="number" step="0.01"></td>' +
                    '<td><button type="button"  class="btn btn-delete"><i id="' + item['id']
                    + '" class="fa fa-trash" ></i></button></td></tr>');

                $('#creator_odds').append(html);

            });


        }
    });


    $(document).on('input', '.form-control.affiliate-input', function () {

        onClick();

    });

    $('#creator_odds').on('click', 'i', function () {

        var $this = $(this);
        var id = $this.attr("id");
        $('.product-1-select-hover[id=' + id + ']').removeClass('selected');
        $('tr[id=' + id + ']').remove();

        onClick();

    });

    function onClick(event) {
        var odds = $('input[name^=odds]').map(function (idx, elem) {
            return $(elem);
        }).get();

        var total = 0;
        for (var i = 0; i < odds.length; i++) {

            total += parseFloat(odds[i].val());


        }
        $('#odds_span').text(total.toFixed(2));


        if (total === 100.00) {
            var price = $('span[id="price"]').map(function (idx, elem) {
                return $(elem).text();
            }).get();

            var p = 0;
            for (var i = 0; i < price.length; i++) {

                p += parseFloat(price[i]);


            }

            var input = $('input[name="affilate"]').val();
            var percent = ((parseFloat(input) / 100) * p) + p;
            $('#price_span').text('$ ' + percent.toFixed(2));
            $('#submit_btn').removeClass('disabled');

        }
        if (total !== 100.00) {
            $('#price_span').text('N/A');
            $('#submit_btn').addClass('disabled');
        }

    }

    $('.range-slide-2').on('change', function () {
        var $this = $(this),
            value = $this.prop("value");
        var odd = $('#odds_span').text();
        if (odd >= 100) {

            var price = $('span[id="price"]').map(function (idx, elem) {
                return $(elem).text();
            }).get();

            var p = 0;
            for (var i = 0; i < price.length; i++) {

                p += parseFloat(price[i]);

            }

            var percent = ((parseFloat(value) / 100) * p) + p;
            $('#price_span').text('$ ' + percent.toFixed(2));
            $('#submit_btn').removeClass('disabled');
        }

    });

    $('#submit_btn').on('click', function () {
        var $this = $(this),
            odds = $('input[name^=odds]').map(function (idx, elem) {
                return $(elem);
            }).get();

        var total = 0;
        for (var i = 0; i < odds.length; i++) {

            total += parseFloat(odds[i].val());


        }
        if ($this.hasClass('disabled') || total !== 100.00) {

            $('div[class="modal-body"]').empty();
            var text = '<h3>Total odds must be 100%</h3>';
            $('div[class="modal-body"]').append(text);
            $('#case_modal').modal('show');

        } else {

            var image = $('.spin-slider-2-link.selected :input').val(),
                case_name = $('textarea[id="case_name"]').val(),
                token = $('input[name="_token"]').val(),
                price = $('#price_span').text().replace("$", ""),
                user = $('input[name="user"]').val(),
                skin_image = $('input[name=radio]:checked', '#creator_odds').val(),
                items = $('input[name^=odds]').map(function (idx, elem) {
                    return {odd: $(elem).val(), id: $(elem).attr('id').replace('input_', '')};
                }).get();

            if (price < 0.02) {
                $('div[class="modal-body"]').empty();
                var text = '<h3>Community case has a minimum open cost of $0.02.</h3>';
                $('div[class="modal-body"]').append(text);
                $('#case_modal').modal('show');
            } else {

                var data = {
                    image: image,
                    case_name: case_name,
                    price: price,
                    items: items,
                    skin_image: skin_image,
                    _token: token,
                    user: user
                };

                $.ajax({
                    type: "POST",
                    url: '/api/storeCase',
                    data: data,
                    success: function (res) {
                        var text = '<h3>' + res['message'] + '</h3>';
                        $('div[class="modal-body"]').empty();
                        $('div[class="modal-body"]').append(text);
                        $('#case_modal').modal('show');
                        if (!res['error']) {
                            $('button[data-dismiss="modal"]').on('click', function () {
                                window.location = '/cases';
                            })
                        }

                    }

                });
            }
        }
    });


    $('#search').on('input', function () {
        var $this = $(this);
        $('#skins_tab').hide();
        $.get('/api/skins/' + $this.val(), function (items) {
            $('#skins_tab .mCSB_container').empty();
            $(items).each(function (index, item) {
                console.log(item['id']);
                var html = '<div class="product-1-grid-2 col-xs-4 col-sm-4 col-md-6 col-lg-3">' +
                    '<div class="product-1">' +
                    '<a class="product-1-select-hover" id="' + item['id'] + '" href="javascript:void(0);">' +
                    '<span class="product-1-img">' +
                    '<img class="img-responsive center-block" src="https://steamcommunity-a.akamaihd.net/economy/image/' + item['img'] + '" alt="product">' +
                    '<span class="money-4">' + item['price'] + '</span>' +
                    '</span>' +
                    '<span class="product-1-detail">' +
                    '<span class="product-1-name">' + item['market_name'] + '</span>' +
                    '</span>' +
                    '</a>' +
                    '</div>' +
                    '</div>';

                $('#skins_tab .mCSB_container').append(html);
            });
        });

        $('#skins_tab').show();
        //   $('#skins_tab').mCustomScrollbar("update");
    });

    $(document).on('input', 'input[name^=odds]', function () {
        var $this = $(this),
            total = $('#odds_span').text();
        if (total > 100.00) {

            var val = $this.val();
            $this.val(parseFloat(val - (total - 100.00)).toFixed(2));
            $('#odds_span').text(100);
            var value = $('.range-slide-2').prop('value');
            var price = $('span[id="price"]').map(function (idx, elem) {
                return $(elem).text();
            }).get();

            var p = 0;
            for (var i = 0; i < price.length; i++) {

                p += parseFloat(price[i]);

            }
            var percent = ((parseFloat(value) / 100) * p) + p;
            $('#price_span').text('$ ' + percent.toFixed(2));
            $('#submit_btn').removeClass('disabled');


        }

    })

});
