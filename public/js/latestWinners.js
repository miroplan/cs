function getLatestWinners(data) {

    var source   = $("#latest-winners-template").html();
    var template = Handlebars.compile(source);
    var data = JSON.parse(data);

    var context = {
        skin_image  : data.item.skin ? data.item.skin.img : 0,
        cart_price  : data.cart ? data.cart.price : 0,
        item_price  : data.item ? data.item.price : 0,
        market_name : data.item.skin ? data.item.skin.market_hash_name : 0
    };

    var html    = template(context);

    $(".latestWinners").prepend(html);
}