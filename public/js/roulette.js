if (!String.prototype.format) {
    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined' ?
                args[number] :
                match;
        });
    };
}

(function ($) {
    var Roulette = function (options) {
        var defaultSettings = {
            maxPlayCount: 5, // x >= 0 or null
            speed: 10, // x > 0
            stopImageNumber: null, // x >= 0 or null or -1
            rollCount: 3, // x >= 0
            duration: 3, //(x second)
            case_id: null, //Case num
            $images: null,
            stopCallback: function () {
            },
            startCallback: function () {
            },
            slowDownCallback: function () {
            }
        }
        var defaultProperty = {
            playCount: 0,
            $rouletteTarget: null,
            imageCount: null,
            //        $images: null,
            originalStopImageNumber: null,
            totalWidth: null,
            topPosition: 0,
            winImg: null,

            maxDistance: null,
            slowDownStartDistance: null,

            isRunUp: true,
            isSlowdown: false,
            isStop: false,

            distance: 0,
            runUpDistance: null,
            isIE: navigator.userAgent.toLowerCase().indexOf('msie') > -1 // TODO IE
        };
        var p = $.extend({}, defaultSettings, options, defaultProperty);

        var reset = function () {
            p.maxDistance = defaultProperty.maxDistance;
            p.slowDownStartDistance = defaultProperty.slowDownStartDistance;
            p.distance = defaultProperty.distance;
            p.isRunUp = defaultProperty.isRunUp;
            p.isSlowdown = defaultProperty.isSlowdown;
            p.isStop = defaultProperty.isStop;
            p.topPosition = defaultProperty.topPosition;
        }


        function getRandomInt(min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }

        function getPositionOfWinner(winner) {
            var widthOfImg = p.imageWidth;
            var minDistanceToEdgeAllowed = 295;

            var desiredImg = $(p.$images[winner - 3]);

            var minPos = desiredImg.position().left + minDistanceToEdgeAllowed;
            var maxPos = desiredImg.position().left + widthOfImg - 40;

            return getRandomInt(minPos, maxPos);
        }


        function timelineFinished(destImg) {
            // this is where you highlight your winner
            $(p.$images[destImg]).addClass('winner_img');
            p.stopCallback({elem: destImg});
        }

        function rouletteSpin(destImg) {

            tl = new TimelineMax({
                onComplete: timelineFinished,
                onCompleteParams: [destImg]
            });

            var rouletteImages = p.$rouletteTarget,
                startLeft = rouletteImages.position().left;

            tl
                .to(rouletteImages, 3, {
                    x: getPositionOfWinner(destImg) * -1,
                    ease: Power4.easeOut
                })

            // console.log(getPositionOfWinner(destImg));
        }

        var init = function ($roulette) {
            $roulette.css({'overflow': 'hidden'});
            defaultProperty.originalStopImageNumber = p.stopImageNumber;
            if (!p.$images) {
                p.$images = $roulette.find('img').remove();
                p.imageCount = p.$images.length;
                p.$images.eq(0).bind('load', function () {
                    p.imageWidth = $(this).outerWidth();
                    p.imageHeight = $(this).height();
                    p.totalWidth = p.imageCount * p.imageWidth;
                    $roulette.css({'height': p.imageHeight, 'width': (p.totalWidth + 13)});
                    p.runUpDistance = 2 * p.imageWidth;
                }).each(function () {
                    //this.attr('id', 'roulette-img' + this.index());
                    if (this.complete || this.complete === undefined) {
                        var src = this.src;
                        // set BLANK image
                        this.src = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";
                        this.src = src;
                    }
                });

            }
            $roulette.find('div').remove();
            p.$images.css({
                'display': 'block'
            });
            p.$rouletteTarget = $('<div>').css({
                'position': 'relative',
                'left': '0'
            }).attr('class', "roulette-inner");
            $roulette.append(p.$rouletteTarget);
            p.$rouletteTarget.append(p.$images);
            p.$rouletteTarget.append(p.$images.eq(0).clone());
            $roulette.show();
        }

        var start = function (case_id) {
            $('.winner_img').removeClass('winner_img');

            var url = '/getrandom';

            if (case_id != undefined) {
                url = '/getrandom/' + case_id;
            }

            p.startCallback();

            $.getJSON(url, function (item) {

                if(item.img)
                {
                    p.$images.eq(item.num).attr("src", "http://steamcommunity.com/economy/image/" + item.img);
                }
                rouletteSpin(item.num);
            });



        }

        var stop = function (option) {
            if (!p.isSlowdown) {
                if (option) {
                    var stopImageNumber = Number(option.stopImageNumber);
                    if (0 <= stopImageNumber && stopImageNumber <= (p.imageCount - 1)) {
                        p.stopImageNumber = option.stopImageNumber;
                    }
                }
                slowDownSetup();
            }
        }
        var option = function (options) {
            p = $.extend(p, options);
            p.speed = Number(p.speed);
            p.duration = Number(p.duration);
            p.duration = p.duration > 1 ? p.duration - 1 : 1;
            defaultProperty.originalStopImageNumber = options.stopImageNumber;
        }

        var ret = {
            start: start,
            stop: stop,
            init: init,
            option: option
        }
        return ret;
    }

    var pluginName = 'roulette';
    $.fn[pluginName] = function (method, options) {
        return this.each(function () {
            var self = $(this);
            var roulette = self.data('plugin_' + pluginName);

            if (roulette) {
                if (roulette[method]) {
                    roulette[method](options);
                } else {
                    console && console.error('Method ' + method + ' does not exist on jQuery.roulette');
                }
            } else {
                roulette = new Roulette(method);
                roulette.init(self, method);
                $(this).data('plugin_' + pluginName, roulette);
            }
        });
    }
})(jQuery);
