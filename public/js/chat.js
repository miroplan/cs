// Live Chat
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

io.on('connection', function(socket){
    console.log('connection');
    socket.on('chat.user.connect', function (data) {
        socket.username = data.username;
        io.emit('chat.user.connect', data.username);
    });

    socket.on('disconnect', function(){
        console.log('disconnect');
        if(!socket.username) return;
        io.emit('chat.user.disconnect', socket.username);
    });

    socket.on('chat.message', function (data) {
        console.log('New message: ' + data.message);
        io.emit('chat.message', data);
    });
});

http.listen(3000, function(){
    console.log('listening on *:3000');
});
